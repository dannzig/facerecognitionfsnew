package com.megvii.demoface;

import com.megvii.demoface.models.faceModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Global {
    public static ArrayList<faceModel> faceTemplatesModel = new ArrayList<>();

    public static ArrayList<String> faceString = new ArrayList<>();
    public static Map<String, byte[]> faceTemplatMap = new HashMap<>();
}
