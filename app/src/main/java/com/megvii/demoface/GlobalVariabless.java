package com.megvii.demoface;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONArray;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class GlobalVariabless {

    public static String selectedvalue;
    //public static String printerType = "inner"; //kentegra printer
    public static String printerType = "bluetooth printer"; // normal bluetooth
    public static String collected_weight;
    public static String captured_route;
    public static String captured_col_center;

    //public static String printerType = "inner";
    public static String printing_id;
    public static Boolean is_encrypted = false;
    public static String service_start_mode;

    //FC
    public static List<String> farmers_selected = new ArrayList<String>();
    public static List<String> topics_selected = new ArrayList<String>();
    public static List<String> reason_selected = new ArrayList<String>();
    public static List<String> plotNo_selected_already_mapped_ones = new ArrayList<String>();
    public static List<String> plotted_farm_lrNumber_selected = new ArrayList<String>();
    public static List<String> action_selected = new ArrayList<String>();
    public static List<String> child_selected = new ArrayList<String>();
    public static List<String> alp_main_qn_selected = new ArrayList<String>();
    public static List<String> biodiversity_main_qn_selected = new ArrayList<String>();

    public static String coachingSessionId = "";
    public static String topic_selected_id = "";
    public static String reason_selected_id = "";
    public static String plotted_plot_name = "";
    public static String plotted_plot_farm_lr_no = "";
    public static String plotted_plot_plot_no = "";
    public static String plotted_farm_name = "";
    public static String plotted_farm_lr_no = "";
    public static String action_selected_id = "";
    public static String main_transaction_no = "";
    public static String alp_main_qn_selected_id = "";
    public static String alp_main_qn_selected_code = "";
    public static String bio_main_qn_selected_id = "";
    public static String bio_main_qn_selected_code = "";
    public static boolean allow_edit_farmers = true;
    public static boolean allow_edit_kin = true;
    public static boolean allow_save_kin = true;
    public static boolean allow_update_kin = true;
    public static String methodTo;
    public static String mapfunction;
    public static String Idno;
    public static Double total_mapped_land;
    public static Double total_sales_value;
    public static double farm_center_latitude;
    public static double farm_center_longitude;
    public static String movement="";
    public static String updating_member_id;
    public static String visit_no;
    public static String updating_member_no;
    public static String kin_member_no;
    public static String number_number;
    public static String fullname="";
    public static String first_name="";
    public static String farm_manager_photostring="";
    public static String photostring="";
    public static String supporting_document_photostring="";
    public static String voters_document_photostring="";
    public static String drivers_document_photostring="";
    public static String pass_document_photostring="";
    public static String photostring_indiv_training="";
    public static String photostring_indiv_training2="";
    public static String photostring_indiv_training3="";
    public static String infrastructure_selected="";
    public static String infrastructure_selected_id="";

    public static String photostring_training="";
    public static String photostring_training2="";
    public static String photostring_training3="";

    public static String middle_name="";
    public static String last_name="";
    public static String selected_farm_lrnumber;
    public static String selected_farm_main_lrnumber;

    public static String selected_plot_no;
    public static String selected_plot_sid;
    public static String other_name="";
    public static String acc_no="";
    public static String mode_of_payment="";
    public static String payee="";
    public static String surname="";
    public static String firstname="";
    public static String other_names;
    public static String coopregDateTime;
    public static String voting_id;
    public static String memberNo;
    public static String total_person_in_house;
    public static String female_persons_in_house;
    public static String male_person_in_house;
    public static String coop_entry_date;
    public static String lrno="";
    public static String landowned;
    public static String farmersize;
    public static String landtenuer;
    public static String avgcost;
    public static String harvestperacre;
    public static String dob;
    public static String date_of_birth;
    public static String dob_kin;
    public static String kin_relationship_id;
    public static String kin_marital_status_id;
    public static String kin_educ_level;
    public static String kin_gender_id;
    public static String kin_is_orphan;
    public static String kin_is_schooled_id;
    public static String kin_phone;
    public static String kin_birth_cert;
    public static String kin_age;
    public static String kin_name;
    public static int vote;
    public static String phone;
    public static String individualVisitId;
    public static String surveyVisitId;
    public static String selected_answer;
    public static String email;
    public static String other_programs;
    public static int nationality;
    public static int bank;
    public static int bankbranches;
    public static int marital;
    public static String address;
    public static String Gender;
    public static String County;
    public static String FOrg;
    public static String DIST;
    public static String WARD;
    public static String VILLAGE;
    public static int gender;
    public static int county;
    public static int district;
    public static int ward;
    public static int village;
    public static int org;
    //FC

    // TC
    public static final String tc_file_path = Environment.getExternalStorageDirectory().toString() + "/.WC/TimeCapture/";
    public static final String fc_file_path = Environment.getExternalStorageDirectory().toString() + "/.WC/FarmCapture/";
    public static final String hrcapture_file_path = Environment.getExternalStorageDirectory().toString() + "/.WC/HRCaptureSignature/";

    public static final String tc_employee_file_path = tc_file_path + "Employee/";
    public static final String tc_engineering_file_path = tc_file_path + "Engineering/";

    public static final String fc_farmer_file_path = fc_file_path + "FarmersPics/";
    public static final String fc_farmer_signatures_file_path = fc_file_path + "FarmersSignatures/";
    public static final String fc_training_file_path = fc_file_path + "TrainingPics/";

    public static final String fc_ipm_file_path = fc_file_path + "IPM/";
    public static final String fc_others_file_path = fc_file_path + "OTHERS/";
    public static final String file_name_face_tracker ="FRMemory.dat";

    //public static int matching_acuracy = 60;
    //public static int matching_acuracy = 90;
    public static int matching_acuracy = 80;


    public static int tc_sync_interval_mins = 60;
    public static int fc_sync_interval_mins = 60;
    public static int tc_camera_sync_interval_mins = 5;
    public static int tc_random_call_mins = 2;


    public static final String sharedprefsname ="com.capturesolutions.weightcapture";

    public static String tc_sync_service = "all";
    public static String fc_sync_service = "all";

    public static String getTc_sync_service() {
        return tc_sync_service;
    }

    public static void setTc_sync_service(String tc_sync_service) {
        GlobalVariabless.tc_sync_service = tc_sync_service;
    }

    public static String getFc_sync_service() {
        return fc_sync_service;
    }

    public static void setFc_sync_service(String fc_sync_service) {
        GlobalVariabless.fc_sync_service = fc_sync_service;
    }




    public static String face_rec;

    public static String the_customer = "FIRESTONE";


    public static String licence_code;
    public  static  final String format = "###,###.##";

    public static HashMap<String,Integer> geek=new HashMap<String,Integer>();
    public static String prinitng_supplier = "";
    public static String tran_id;
    public static String is_pps = "no";
    public static String ls;

    public static String Log_file_name = "logs.cvtio";
    public static String OSRP_Rubber_Source_Code = "1269";

    //use this to set product
    public static boolean is_osrp = true;
    public static int collection_product = 2; // 1-Tea,  2-Rubber,   3-Flower; //This changes Collection activity interface



//    public static String surl = "https://digifiqa.bfusa.com:9090/"; //FIRESTONE LOCALHOST PILOT
	public static String surl = "https://digifipd.bfusa.com/"; //FIRESTONE LOCALHOST LIVE


    //public static String surl = "https://digifipd.bfusa.com/"; //FIRESTONE LOCALHOST LIVE
    //public static String surl_fc = "https://test1.cs4africa.com/"; //FIRESTONE LOCALHOST LIVE

    public static String use_fingerprint_scanner = "no";   //should be yes or no

    public static String rec_pp = "xxz1";

    public static boolean qrscanner = false;
    public static boolean select_transporter_on_startday = false;
    public static boolean is_new_system = true;
    public static String cummulative_type = "local";
    //public static String cummulative_type = "server";
    public static boolean is_passwordhashing = true;
    public static boolean limit_maximum_bag_weight = false;
    public static boolean checkscalenegativelimit = false;
    public static boolean password_on_route = false;
    public static boolean show_farmers_per_route = false;
    public static boolean isSMS = true;
    public static boolean show_fsn = false;
    public static String attendance_type;
    public static String mapactivity;
    public static boolean Map_Estates_Not_Farmers;


    public static String member_name,plucker_name,Transaction_Id,plucker_type,TID;
    public static int member_id1,plucker_id,Plucker_Cost_Id,SID;
    public static String tag_no;
    public static String scanner_activity;
    public static String photophoto;
    public static Boolean is_print_weight;
    public static String separator;
    public static String PIC;
    public static int mode_of_identification_id;
    public static String userid;
    public static String userrid_fc;
    public static int version_code;
    public static String selected_farm_id;
    public static String qr_code_json;
    public static String task_session_number;

    public static String DB_PATH = Environment.getExternalStorageDirectory().toString()+ "/.WeighCAPTURE_Files" + "/";

    //public static String DB_PATH2 = getExternalFilesDir(null).getAbsolutePath() + "/";

    public static String the_service_start;
    public static boolean isStickerOnVehicle;

    public static boolean is_back_up_running_already;
    public static Double total_gross = 0.0;
    public static Double total_net = 0.0;
    public static Double total_cuplump = 0.0;
    public static Double total_latex = 0.0;
    public static int count;

    public static String forwadingorder;
    public static String selectedSession;
    public static String transferingsession;
    public static String isclockinout;
    public static String motor="";

    public static String assigning_nfc;
    public static int UTK;
    public static int UKL;
    public static String visitor_id;
    public static String record_id;
    public static  String vehicle_owner;
    public static  String nfc_card;
    public static String qr_code;
    public static Boolean view_visitors;
    public static String clockin_out_register;
    public static String t_type;
    public static String vehicle_mode;
    public static String textcontent_nfc;
    public static String all_previous_nfc_data;
    public static String drivername;
    public static String driverAccount;
    public static String vehicle_reg;
    public static String dispatchingProduct;
    public static String cuplump_date_;
    public static String cuplump_date_ses;
    public static String cuplump_printdate;
    public static String coag_latex_date_;
    public static String osrp_date_;
    public static String collection_datetime;
    public static String transaction_no;
    public static String reasons;
    public static String destination;
    public static String host;
    public static String from;
    public static String type_;
    public static String vehicle_regi_type;
    public static String vehicle_cate_type;
    public static String ppd_no="";
    public static String vehicle_type;
    public boolean is_carters=false;
    public static String date_for_rating="";
    public static String date_for_headman_report="";
    public static String date_for_report="";

    public static String date_for_visit_fc="";
    public static String end_date_for_visit_fc="";
    public static String date_for_report_attendance="";
    public static String date_for_report_task="";
    public static String date_for_report_end="";
    public static String date_for_report_task_end="";
    public static String act_from="";
    public static List<String> addvalues_;

    public static String adult_answer_sid = "";



    //	public static String system_name = "secucapture";
    public static String system_name = "weightcapture";
//	public static String system_name = "qccapture";

    // TC
    public static void set_system_name(Context ctx, String systemName) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("system_name", systemName);

        editor_.apply();
    }

    public static String system_name(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("system_name", null);
    }

    public static void set_system_device(Context ctx, String systemDevice) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("system_device", systemDevice);

        editor_.apply();
    }

    public static String system_device(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("system_device", "general");
    }

    public static void set_tc_module_name(Context ctx, String tcModuleName) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_module_name", tcModuleName);

        editor_.apply();
    }

    public static String get_tc_module(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);
        return sharedPref.getString("tc_module_name", "new");
    }

    public static void set_tc_Submodule_name(Context ctx, String tcSubModuleName) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_sub_module_name", tcSubModuleName);

        editor_.apply();
    }

    public static String get_tc_Submodule(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_sub_module_name", "new");
    }


    public static void set_work_order_type(Context ctx, String workOrderType) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("work_order_type", workOrderType);

        editor_.apply();
    }

    public static String get_work_order_type(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("work_order_type", null);
    }



    public static void set_tc_session_no(Context ctx, String sessionNo) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_session_no", sessionNo);

        editor_.apply();
    }


    public static String get_tc_session_no(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_session_no", null);
    }

    public static void set_fc_visit_no(Context ctx, String visitID) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("fc_visit_no", visitID);

        editor_.apply();
    }


    public static String get_fc_visit_no(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("fc_visit_no", null);
    }


    public static void set_tc_new_dvpt_block(Context ctx, String tcDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_new_dvpt_block", tcDivision);

        editor_.apply();
    }

    public static String get_tc_new_dvpt_block(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_new_dvpt_block", "new");
    }

    public static void set_tc_new_dvpt_aggregate_block(Context ctx, String tcBlock) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_new_dvpt_aggregate_block", tcBlock);

        editor_.apply();
    }

    public static String get_tc_new_dvpt_aggregate_block(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_new_dvpt_aggregate_block", "new");
    }


    public static void set_tc_new_dvpt_Division(Context ctx, String tcDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_new_dvpt_division", tcDivision);

        editor_.apply();
    }

    public static String get_tc_new_dvpt_Division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_new_dvpt_division", "new");
    }

    public static void set_tc_headman_report_Division(Context ctx, String timeDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_headman_report_Division", timeDivision);

        editor_.apply();
    }

    public static String get_tc_headman_report_Division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_headman_report_Division", "new");
    }

    public static void set_tc_new_dvpt_aggregate_Division(Context ctx, String tcDivisionAgg) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();
        editor_.putString("tc_new_dvpt_aggregate_division", tcDivisionAgg);

        editor_.apply();
    }

    public static String get_tc_new_dvpt_aggregate_Division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_new_dvpt_aggregate_division", "new");
    }


    public static void set_fc_estates_division(Context ctx, String fcDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("fc_estates_division", fcDivision);

        editor_.apply();
    }

    public static String get_fc_estates_division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("fc_estates_division", "new");
    }

    public static void set_fc_estates_block(Context ctx, String fcBlock) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("fc_estates_block", fcBlock);

        editor_.apply();
    }

    public static String get_fc_estates_block(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("fc_estates_block", "new");
    }


    public static void set_fc_estates_block_category_seperator(Context ctx, String fcBlockCatSep) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("fc_estates_block_category_seperator", fcBlockCatSep);

        editor_.apply();
    }

    public static String get_fc_estates_block_category_seperator(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("fc_estates_block_category_seperator", "new");
    }

    public static void set_tc_Division(Context ctx, String tcDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_division", tcDivision);

        editor_.apply();
    }

    public static String get_tc_Division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_division", "new");
    }

    public static void set_fc_selected_Division(Context ctx, String fcDivision) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("fc_selected_division", fcDivision);

        editor_.apply();
    }

    public static String get_fc_selected_Division(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("fc_selected_division", "new");
    }

    public static void set_tc_SubDept(Context ctx, String tcSubDept) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(sharedprefsname, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = sharedPref.edit();
        editor_ = sharedPref.edit();

        editor_.putString("tc_sub_department", tcSubDept);

        editor_.apply();
    }

    public static String get_tc_SubDept(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("tc_sub_department", "new");
    }



    public static String previously_selected_activity_sid(Context act) {

        SharedPreferences prefs = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);
        return prefs.getString("previously_selected_activity_sid", null);

    }

    public static void set_previously_selected_activity_sid(Context act, String previously_selected_activity_sid) {

        SharedPreferences.Editor saver = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("previously_selected_activity_sid", previously_selected_activity_sid);
        saver.commit();

    }

    public static String previously_selected_block_sid(Context act) {

        SharedPreferences prefs = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);
        return prefs.getString("previously_selected_activity_area_sid", null);

    }

    public static void set_previously_selected_block_sid(Context act, String previously_selected_activity_area_sid) {

        SharedPreferences.Editor saver = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("previously_selected_activity_area_sid", previously_selected_activity_area_sid);
        saver.commit();

    }

    public static void set_randomCall_time(Context act, String randomCall_time_var) {
        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("randomCall_time_var", randomCall_time_var);
        saver.commit();
    }

    public static String randomCall_time(Context act) {
        SharedPreferences prefs = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE);
        return prefs.getString("randomCall_time_var", null);
    }

    public static void set_sync_time(Context act, String sync_time_var) {
        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("sync_time_var", sync_time_var);
        saver.commit();
    }

    public static String sync_time(Context act) {
        SharedPreferences prefs = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE);
        return prefs.getString("sync_time_var", null);
    }

    public static void set_face_camera(Context act, int cam_type) {
        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putInt("camera_type", cam_type);
        saver.commit();
    }

    public static int face_camera(Context act) {
        SharedPreferences prefs = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE);

        return prefs.getInt("camera_type", 0);
    }

    public static void set_face_registration(Context act, int reg_type) {
        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putInt("registration_type", reg_type);
        saver.commit();
    }

    public static int face_registration(Context act) {
        SharedPreferences prefs = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE);

        return prefs.getInt("registration_type", 0);
    }

    public static void set_gate_name(Context act,String name) {

        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();

        saver.putString("gatename", name);
        saver.commit();
    }
    public static void set_gate_id(Context act,String id) {

        SharedPreferences.Editor saver = act.getSharedPreferences(GlobalVariabless.sharedprefsname, act.MODE_PRIVATE).edit();
        saver.putString("gate_id", id);
        saver.commit();
    }

    public static String get_gate_name(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("gatename", "Gate Not Set");
    }
    public static String get_gate_id(Context act) {
        SharedPreferences sharedPref = act.getSharedPreferences(sharedprefsname, act.MODE_PRIVATE);

        return sharedPref.getString("gate_id", "1");
    }
    // TC

    public static String getSerialNumber() {
        String serialNumber;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");
//			if (serialNumber.equals(""))
//				serialNumber = (String) get.invoke(c, "ril.serialnumber");
//			if (serialNumber.equals(""))
//				serialNumber = (String) get.invoke(c, "ro.serialno");
//			if (serialNumber.equals(""))
            serialNumber = (String) get.invoke(c, "sys.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = Build.SERIAL;

            // If none of the methods above worked
            if (serialNumber.equals(""))
                serialNumber = null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber = null;
        }

        return serialNumber;
    }

    public static String getDeviceId(Context context) {

        String deviceId;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            deviceId = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephony.getDeviceId() != null) {
                deviceId = mTelephony.getDeviceId();
            } else {
                deviceId = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }
        }

        return deviceId;
    }
    public static String getCameraDeviceId(Context context) {

        String deviceId;

        Log.e("*****************", "getIMEIDeviceId: "+Build.VERSION.SDK_INT + " %%%%%%%%%%%%%%%%%%%%%%%%" +  Build.VERSION_CODES.Q);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)

        {
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    deviceId = mTelephony.getImei();
                }else {
                    deviceId = mTelephony.getDeviceId();
                }
            } else {
                deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        Log.e("DEVICE ID =>", deviceId +  "|" + Settings.Secure.getString(context.getContentResolver(), "android_id"));

        return deviceId +  "|" + Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String getIMEIDeviceId(Context context) {

        String deviceId;

        Log.e("*****************", "getIMEIDeviceId: "+Build.VERSION.SDK_INT + " %%%%%%%%%%%%%%%%%%%%%%%%" +  Build.VERSION_CODES.Q);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)

        {
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);


        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    deviceId = mTelephony.getImei();
                }else {
                    deviceId = mTelephony.getDeviceId();
                }
            } else {
                deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        Log.d("deviceId", deviceId);
        return deviceId;
    }


    public static String final_forwading_order;
    public static boolean writeCard;
    public static boolean IS_ADMIN_TEST = false;

    public static int dispatch_weight;

    public static boolean is_scale_check = false;


    //public static String inspection_code = "FactoryInspectionQc";
    public static String inspection_code = "OntappingCartersQc";
    public static String fc_code = "FARMCAPTURE";

    public static String pack_qc_identifier = "QCFINISHEDPRODUCT";

    public static String forwadingorder_no = "DEFAULT";
    public static String qc_transaction_no = "DEFAULT";
    public static String qc_transaction_date = "DEFAULT";

    public static String crate_no = "0";
    public static boolean is_proceed = true;
    public static boolean is_revision = false;
    public static String car_reg_no = "DEFAULT";
    public static String driver_name = "DEFAULT";
    public static boolean is_pre_weight = true;
    public static boolean is_lab = false;
    public static boolean is_edit = false;
    public static boolean is_reprocess = false;

    public static String identifier_staff = "EMPLOYEES";
    public static String identifier_driers = "DRYERS";
    public static String identifier_destinations = "FINALDEST";
    public static String identifier_grades = "GRADES";
    public static String identifier_detectors = "DETECTORS";
    public static JSONArray divisionWeightsArray;


    public static String timeNow() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }

    public static String timeNowNow() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date());
    }

    public static String dateNow() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(new Date());
    }

    public static String dateNowLB() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        return dateFormat.format(new Date());
    }

    public static String dateNowTrans() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(new Date());
    }

    public static String timeNowLB() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        return dateFormat.format(new Date());
    }

    public static String gatepass_time() {
        //2022-12-02T16:30:38.777
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return dateFormat.format(new Date());
    }
    public static String goodspass_time() {
        //2022-12-02T00:00:00
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00");
        return dateFormat.format(new Date());
    }

    public static String logDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        return dateFormat.format(new Date());
    }

    public static String yesterdayDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
        //return dateFormat.format(new Date());
    }

    public static String getRandomNumberString() {
        Random rnd = new Random();
        int number = rnd.nextInt(9999);
        String final_num = String.format("%03d", number);
        String final_val = "TXN_"+final_num+System.currentTimeMillis()/100+final_num;
        return final_val;
    }
    public static String get_transno(){
        Random rnd = new Random();
        int number = rnd.nextInt(8459);
        String final_val = System.currentTimeMillis()/1000+"_"+number;
        return final_val;
    }


    public static String qr_code_date;
    public static boolean isContractorLatex;
    public static boolean isFactoryExit;
    public static boolean isCuplumpSession;
    public static String  FO_TO_VIEW;
    public static String type_of_forwardingOrder;

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    //hrcapture global variable
    public static String Posting_No="none";

}

