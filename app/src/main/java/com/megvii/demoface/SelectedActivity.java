package com.megvii.demoface;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.megvii.demoface.denselmk.FaceDenseLmkFrameActivity;
import com.megvii.demoface.denselmk.FaceDenseLmkImageActivity;
import com.megvii.demoface.facecompare.FaceCompareActivity;
import com.megvii.demoface.facedetect.FaceDetectFrameActivity;
import com.megvii.demoface.facedetect.FaceDetectImageActivity;
import com.megvii.demoface.handdetect.HandDetectFrameActivity;
import com.megvii.demoface.handdetect.HandDetectImageActivity;
import com.megvii.demoface.scene.SceneFrameDetectActivity;
import com.megvii.demoface.scene.SceneImageDetectActivity;
import com.megvii.demoface.segment.SegmentFrameActivity;
import com.megvii.demoface.segment.SegmentImageActivity;
import com.megvii.demoface.segment.SegmentVideoActivity;
import com.megvii.demoface.skeleton.SkeletonFrameActivity;
import com.megvii.demoface.skeleton.SkeletonImageActivity;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.facepp.multi.sdk.BodySegmentApi;
import com.megvii.facepp.multi.sdk.DLmkDetectApi;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FaceppApi;
import com.megvii.facepp.multi.sdk.HandDetectApi;
import com.megvii.facepp.multi.sdk.SceneDetectApi;
import com.megvii.facepp.multi.sdk.SkeletonDetectApi;

import butterknife.BindView;
import butterknife.OnClick;

public class SelectedActivity extends BaseActivity {
    public static final int CAMERA_CODE = 100;
    public static final int REQ_GALLERY_CODE = 101;
    public static final int GALLERY_CODE = 101;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.ll_image_detect)
    LinearLayout llImageDetect;
    @BindView(R.id.ll_frame_detect)
    LinearLayout llFrameDetect;
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_video_detect)
    LinearLayout llVideoDetect;
    @BindView(R.id.rl_loading_view)
    LinearLayout rlLoadingView;

    private int sdkType;
    private boolean isClickable = false;
    private long lastClickTime = 0;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_skeleton_home;
    }

    @Override
    protected void initView() {
        tvVersion.setText(FaceppApi.getInstance().getApiVersion());
    }

    @Override
    protected void initData() {
        setResult(100);
        sdkType = getIntent().getIntExtra("sdkType", 0);
        //llVideoDetect.setVisibility(View.GONE);
        if (sdkType == SKELETON_DETECT) {
            tvTitleBar.setText("key points of human skeleton");
        } else if (sdkType == SEGMENT) {
            tvTitleBar.setText("human body cutout");
            llVideoDetect.setVisibility(View.VISIBLE);
        } else if (sdkType == FACE_DETECT) {
            tvTitleBar.setText("Face Detection");
        } else if (sdkType == FACEPLUS_DETECT) {
            tvTitleBar.setText("Face detection advanced version");
        } else if (sdkType == FACE_COMPARE) {
            tvTitleBar.setText("Face comparison");
            //llFrameDetect.setVisibility(View.GONE);
        } else if (sdkType == DENSE_LMK) {
            tvTitleBar.setText("Face dense key points");
        } else if (sdkType == HAND_DETECT) {
            tvTitleBar.setText("hand detection");
        } else if (sdkType == SCENE) {
            tvTitleBar.setText("scene detection");
        }
        loadModel();
    }

    private void loadModel() {
        rlLoadingView.setVisibility(View.VISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                int retCode;
                if (sdkType == SKELETON_DETECT) {
                    retCode = SkeletonDetectApi.getInstance().initSkeletonDetect();
                } else if (sdkType == SEGMENT) {
                    int segMode = getIntent().getIntExtra("segMode", BodySegmentApi.SEGMENT_MODE_ROBUST);
                    retCode = BodySegmentApi.getInstance().initBodySegment(ConUtil.SEGMENT_THREAD_COUNT, segMode);
                } else if (sdkType == FACE_DETECT) {
                    retCode = FaceDetectApi.getInstance().initFaceDetect();
                } else if (sdkType == FACEPLUS_DETECT) {
                    retCode = FaceDetectApi.getInstance().initFaceDetect();
                } else if (sdkType == FACE_COMPARE) {
                    retCode = FaceDetectApi.getInstance().initFaceDetect();
                } else if (sdkType == DENSE_LMK) {
                    retCode = FaceDetectApi.getInstance().initFaceDetect();
                    if (retCode == FaceppApi.MG_RETCODE_OK) {
                        retCode = DLmkDetectApi.getInstance().initDLmkDetect();
                    }
                } else if (sdkType == HAND_DETECT) {
                    retCode = HandDetectApi.getInstance().initHandDetect();
                } else if (sdkType == SCENE) {
                    retCode = SceneDetectApi.getInstance().initSceneDetect();
                } else {
                    retCode = -1;
                }

                final int finalRetCode = retCode;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rlLoadingView.setVisibility(View.GONE);
                        if (finalRetCode != FaceppApi.MG_RETCODE_OK) {
                            setResult(101);
                            finish();
                        } else {
                            isClickable = true;
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sdkType == SKELETON_DETECT) {
            SkeletonDetectApi.getInstance().releaseSkeletonDetect();
        } else if (sdkType == SEGMENT) {
            BodySegmentApi.getInstance().releaseBodySegment();
        } else if (sdkType == FACE_DETECT || sdkType == FACEPLUS_DETECT || sdkType == FACE_COMPARE) {
            FaceDetectApi.getInstance().releaseFaceDetect();
        } else if (sdkType == DENSE_LMK) {
            FaceDetectApi.getInstance().releaseFaceDetect();
            DLmkDetectApi.getInstance().releaseDlmDetect();
        } else if (sdkType == HAND_DETECT) {
            HandDetectApi.getInstance().releaseHandDetect();
        } else if (sdkType == SCENE) {
            SceneDetectApi.getInstance().releaseSceneDetect();
        }
    }

    private void requestGalleryPerm() {
        if (sdkType == FACE_COMPARE) {
            startActivity(new Intent(this, FaceCompareActivity.class));
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //进行权限请求
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_CODE);
        } else {
            openGalleryActivity();
        }

    }

    private void requestCameraPerm() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //进行权限请求
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_CODE);
        } else {
            goToFrameDetect();
        }

    }

    private void openGalleryActivity() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(type == 1 ? "image/*" : "video/*");
        startActivityForResult(intent, REQ_GALLERY_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == GALLERY_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {// Permission Granted
                showSettingDialog("Read memory card");
            } else {
                openGalleryActivity();
            }
        }
        if (requestCode == CAMERA_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {// Permission Granted
                showSettingDialog("camera");
            } else {
                goToFrameDetect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_GALLERY_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    if (type == 1) {
                        goToImageDetect(uri);
                    } else if (type == 2) {
                        getToVideoDetect(uri);
                    }
                }
                break;
        }
    }

    public void showSettingDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("无" + msg + "Permissions, go to the settings to open");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ConUtil.getAppDetailSettingIntent(SelectedActivity.this);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private int type = 1; // 1 image 2 video

    @OnClick({R.id.ll_go_back, R.id.ll_image_detect, R.id.ll_frame_detect, R.id.ll_video_detect})
    public void onViewClicked(View view) {
        if (System.currentTimeMillis() - lastClickTime < 500) {
            return;
        }
        lastClickTime = System.currentTimeMillis();
        switch (view.getId()) {
            case R.id.ll_go_back:
                if (isClickable) {
                    finish();
                }
                break;
            case R.id.ll_image_detect:
                if (isClickable) {
                    type = 1;
                    requestGalleryPerm();
                }
                break;
            case R.id.ll_frame_detect:
                if (isClickable) {
                    requestCameraPerm();
                }
                break;
            case R.id.ll_video_detect:
                if (isClickable) {
                    type = 2;
                    requestGalleryPerm();
                }
                break;
        }
    }

    private void goToFrameDetect() {
        Intent intent = null;
        if (sdkType == SKELETON_DETECT) {
            intent = new Intent(this, SkeletonFrameActivity.class);
        } else if (sdkType == SEGMENT) {
            intent = new Intent(this, SegmentFrameActivity.class);
        } else if (sdkType == FACE_DETECT) {
            intent = new Intent(this, FaceDetectFrameActivity.class);
            intent.putExtra("type", "face");
        } else if (sdkType == FACEPLUS_DETECT) {
            intent = new Intent(this, FaceDetectFrameActivity.class);
            intent.putExtra("type", "faceplus");
        } else if (sdkType == DENSE_LMK) {
            intent = new Intent(this, FaceDenseLmkFrameActivity.class);
        } else if (sdkType == HAND_DETECT) {
            intent = new Intent(this, HandDetectFrameActivity.class);
        } else if (sdkType == SCENE) {
            intent = new Intent(this, SceneFrameDetectActivity.class);
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    private void goToImageDetect(Uri uri) {
        Intent intent = null;
        if (sdkType == SKELETON_DETECT) {
            intent = new Intent(this, SkeletonImageActivity.class);
        } else if (sdkType == SEGMENT) {
            intent = new Intent(this, SegmentImageActivity.class);
        } else if (sdkType == FACE_DETECT) {
            intent = new Intent(this, FaceDetectImageActivity.class);
            intent.putExtra("type", "face");
        } else if (sdkType == FACEPLUS_DETECT) {
            intent = new Intent(this, FaceDetectImageActivity.class);
            intent.putExtra("type", "faceplus");
        } else if (sdkType == DENSE_LMK) {
            intent = new Intent(this, FaceDenseLmkImageActivity.class);
        } else if (sdkType == HAND_DETECT) {
            intent = new Intent(this, HandDetectImageActivity.class);
        } else if (sdkType == SCENE) {
            intent = new Intent(this, SceneImageDetectActivity.class);
        }
        if (intent != null) {
            intent.putExtra("imgpath", uri);
            startActivity(intent);
        }
    }

    private void getToVideoDetect(Uri uri) {
        if (sdkType == SEGMENT) {
            Intent intent = new Intent(this, SegmentVideoActivity.class);
            intent.putExtra("videopath", uri);
            startActivity(intent);
        }
    }
}
