package com.megvii.demoface;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.utils.StatusBarUtil;

import java.io.IOException;

import butterknife.ButterKnife;

public abstract class BaseActivity extends Activity {
    protected static final int FACE_DETECT = 1;
    protected static final int FACEPLUS_DETECT = 2;
    protected static final int FACE_COMPARE = 3;
    protected static final int DENSE_LMK = 4;
    protected static final int SKELETON_DETECT = 5;
    protected static final int SEGMENT = 6;
    protected static final int HAND_DETECT = 7;
    protected static final int SCENE = 8;

    public static dbhelper sd;
    public static Activity act;
    public static Context cntx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.initialize(this);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        StatusBarUtil.setStatusBarForGrayBg(this);
        initView();
        act = this;
        cntx = this;
        sd = new dbhelper(cntx,act);
        try {
            initData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //子类Activity实现
    protected abstract int getLayoutResId();
    //子类Activity实现
    protected abstract void initView();
    //子类Activity实现
    protected abstract void initData() throws IOException;
}
