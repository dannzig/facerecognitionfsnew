package com.megvii.demoface;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.megvii.demoface.models.dataretriever;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

//import android.database.sqlite.SQLiteDatabase;


public class MyIntentService extends Service {

    Looper mServiceLooper;
    ServiceHandler mServiceHandler;
    String service_url;
    String WeightCAPTURE = "WeightCAPTURE/";
    int startidserv;
    long TComparer;
    long TComparer1, TComparer3, TComparerAcc;
    long TComparerf;
    int take = 1000;
    int take_photo = 1;

    //    int take_photo_fc = 3;
    int take_photo_fc = 1;
    int skip = 0;
    String fo_id_no = "0";
    HandlerThread thread;
    String url;
    String HTTPjob;
    String Params;
    JSONObject json1 = new JSONObject();
    JSONObject jsonurl = new JSONObject();
    JSONObject jsonjob = new JSONObject();
    String serverAuth;
    AESHelper aesHelper;
    dbhelper doa;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();
    private static final String surl = GlobalVariabless.surl;
    String sending_osrp_receipt_no = "";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        //startForeground(1,new Notification());
        Log.v("BUILD VERSION ", Build.VERSION.SDK_INT + " SDK INT -- AND CODE " + Build.VERSION_CODES.O);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.v("JJJJJ", "JERE JERE");
            startMyOwnForeground();
        } else {
            Log.v("JJJJJ", "OTHER START JERE JERE");
            startForeground(1, new Notification());
        }
        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    private void startMyOwnForeground() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String NOTIFICATION_CHANNEL_ID = "com.capsole.weightcapture";
            String channelName = "My Background Service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    //.setSmallIcon(AppSpecific.SMALL_ICON)
                    .setContentTitle("App is running in background")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            startForeground(2, notification);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        if (GlobalVariabless.is_new_system) {
            WeightCAPTURE = "WeightCAPTURE/";
        } else {
            WeightCAPTURE = "";
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }


    // Handler that receives messages from the thread                                                       //2
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            super.handleMessage(msg);
            Log.e("KKKKKKK", "KKKKKKKKKKKKKK HERE ServiceHandler");
            startidserv = msg.arg1;
            aesHelper = new AESHelper(getApplicationContext());

            String DB_PATH_NOT = GlobalVariabless.DB_PATH;

            String DB_PATH = getExternalFilesDir(null).getAbsolutePath() + "/";

            String path_encrypted = DB_PATH + "xclgdjwc11hen.db";

            SQLiteDatabase.loadLibs(getApplicationContext());
            //databaseservice = SQLiteDatabase.openOrCreateDatabase(path_encrypted, aesHelper.getkey2(), null);
            //databaseservice = SQLiteDatabase.openDatabase(path_encrypted, aesHelper.getkey2(), null, 0);
            doa = new dbhelper(getApplicationContext(), getApplicationContext());



           /* HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 300000);
            JSONobjMAIN = Login();
            HttpJobProcess();*/
            String url = surl + "SystemAccounts/Authentication/Login/Submit";
            try {
                postData(url, Login().toString(), "JobLogin");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public JSONObject Login() {

        String url = surl + "SystemAccounts/Authentication/Login/Submit";
        JSONObject json = new JSONObject();
        JSONObject json1 = new JSONObject();
        JSONObject jsonurl = new JSONObject();
//        Log.v("MMMMMMMMM", "RRRRRRRRR " + url);

        try {
            dataretriever data = new dataretriever();

            data.password = getApplicationContext().getSharedPreferences("com.capturesolutions.weightcapture", Context.MODE_PRIVATE).getString("pass", "Tomcat.1926And26");
            data.username = getApplicationContext().getSharedPreferences("com.capturesolutions.weightcapture", Context.MODE_PRIVATE).getString("user_model", "admin");



            if (GlobalVariabless.is_pps.equalsIgnoreCase("Yes")) {
                json.put("PassWord", data.password);
                json.put("UserName", data.username);
                //json.put("AccountName", "WETCU");
                //json.put("Branch", "DEMO");
                json.put("Language", "English");
                jsonurl.put("url", url);
                jsonjob.put("httpjob", "JobLogin");
                json1.put("CurrentUser", json);
                json1.put("Language", "English");
                json1.put("IsRenewalPasswordRequest", false);
            } else {
                json.put("PassWord", data.password);
                json.put("UserName", data.username);
                json.put("AccountName", data.account_name);
                json.put("Branch", data.branch);
                json.put("Language", "English");
                jsonurl.put("url", url);
                jsonjob.put("httpjob", "JobLogin");
                json1.put("CurrentUser", json);
                json1.put("Language", "English");
                json1.put("IsRenewalPasswordRequest", false);
            }

            //----Log.v("QQQQQQQQQQQQQ","QQQQQQQQQQQQQQQQQQQQQQQQqq "+json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject[] job = {json1, jsonurl, jsonjob};

        return json1;

    }

    public void postData(String url, String json, String http_job) throws IOException {
        Log.v(http_job + "====", "MyIntentService POST OBJECT DATA " + json);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        client = builder.build();

        RequestBody body = RequestBody.create(json, JSON);
        Request request = null;
        request = new Request.Builder()
                .url(url)
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Content-Type", "application/json;")
                .post(body)
                .build();
        if (!http_job.matches("JobLogin")) {
            request = new Request.Builder()
                    .url(url)
                    .header("User-Agent", "OkHttp Headers.java")
                    .addHeader("Content-Type", "application/json;")
                    .addHeader("Authorization", serverAuth)
                    .post(body)
                    .build();
        }
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                try {
                    jobDone("error", "error");
                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                e.printStackTrace();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //try (ResponseBody responseBody = response.body()) {
                try {
                    if (!response.isSuccessful()) {

                        ResponseBody error_responce = response.peekBody(Long.MAX_VALUE);

                        jobDone(error_responce.string(), http_job);
                        throw new IOException("Unexpected code " + response);
                    }


                    serverAuth = response.header("Authorization");

                    ResponseBody responseBodyCopy = response.peekBody(Long.MAX_VALUE);

                    jobDone(responseBodyCopy.string(), http_job);
                } catch (Exception e) {
                    Log.e("FFFFFFFFF", "kkkkkkkkkkkkkkk" + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("NewApi")
    public void jobDone(String httpjobresponse, String httpjob) throws JSONException, IOException {

        String SW = httpjob;
        try {
            Log.v(SW, new JSONObject(httpjobresponse).toString());
        } catch (Exception e) {
            endService("VPN NOT CONNECTED");
            Log.v(SW, httpjobresponse);
        }

        JSONObject jObj = new JSONObject(httpjobresponse);

        Log.v(SW, jObj.toString());

        switch (SW) {
            case "JobLogin":

                String device_serial = GlobalVariabless.getSerialNumber();
                String device_imei = GlobalVariabless.getIMEIDeviceId(getApplicationContext());

                Log.e("deddedddede", "jobDone: " + device_serial + "-----" + device_imei + "=");

                sendIMEA();
                fetchMemberPhotos();


                break;
            //photos
            case "JobFetchMemberPhotos":

                try {

                    if (jObj.getString("statusCode").equals("603")) {
                        //do nothing
                        Log.e("TAG", "jobDone: " + jObj.getString("statusCode"));
                    } else {

                        doa.insertMemberPhotos(jObj.getJSONObject("Result").getJSONArray("Result"));

                    }

//                    doa.insertMemberPhotos(jObj.getJSONObject("Result").getJSONArray("Result"));

                    try {
                        if (jObj.getJSONObject("Result").getJSONArray("Result").length() >= take_photo) {
                            jObj = null;


                            System.gc();
                            fetchMemberPhotos();

                        } else {
                            endService("Successfull");
                        }

                    } catch (Exception ex) {
                        Log.v("------", ex.getMessage());
                        ex.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                endService("Successfull");
                break;


            default:

                break;
        }

    }

    public void endServiceShow(final String message) {
        if (GlobalVariabless.service_start_mode.equalsIgnoreCase("manual secu")) {

            Log.v("HERE", "E====== ");
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    HomeActivity.stopPD(message);
                    GlobalVariabless.the_service_start = "stop";
                }
            });
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    //SyncActivity.stopCount(message);
                    GlobalVariabless.the_service_start = "stop";


                }
            });
        }

    }

    public void endService(final String message) {
        deleteCache(getApplicationContext());
        Log.e("hehehehehehe", "jjjjjjjjjjjjjjj" + GlobalVariabless.service_start_mode);
        if (GlobalVariabless.service_start_mode == null) {

        } else if (GlobalVariabless.service_start_mode.equalsIgnoreCase("manual")) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    GlobalVariabless.the_service_start = "stop";

                }
            });
        } else if (GlobalVariabless.service_start_mode.equalsIgnoreCase("manual_report")) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    if (GlobalVariabless.act_from.equalsIgnoreCase("overseer")) {

                    } else if (GlobalVariabless.act_from.equalsIgnoreCase("superintendent")) {

                    } else {
                        Toast.makeText(MyIntentService.this, "ERROR:: VPN NOT CONNECTED", Toast.LENGTH_SHORT).show();
//                        TaskRatingReport.stopPD(message);
                    }

                    GlobalVariabless.the_service_start = "stop";
                }
            });

        } else if (GlobalVariabless.service_start_mode.equalsIgnoreCase("manual_fc")) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    GlobalVariabless.the_service_start = "stop";
                }
            });
        }
        stopSelf();
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    private void fetchMemberPhotos() {
        url = surl + "Digifi/CameraApi/GetImagesToCamera";
//        TComparerAcc = doa.getMaxIdQCTBL("member_data_table");
        TComparerAcc = doa.getMaxIdTCTBL("member_data_table");
        try {
            JSONObject jfiltersdtls = new JSONObject();
            jfiltersdtls.put("field", "datecomparer");
            jfiltersdtls.put("operator", "gt");
            jfiltersdtls.put("value", "" + TComparerAcc);
//            jfiltersdtls.put("value", 0);
            JSONObject jfiltersdtls2 = new JSONObject();

            jfiltersdtls2.put("field", "sub_group");
            jfiltersdtls2.put("operator", "eq");
            jfiltersdtls2.put("value", "" + "Garage");

            JSONArray filters = new JSONArray();
            filters.put(jfiltersdtls);
            filters.put(jfiltersdtls2);

            JSONObject filter = new JSONObject();
            filter.put("filters", filters);
            filter.put("logic", "AND");
            new JSONObject();
            json1.put("filter", filter);
            json1.put("take", take_photo);
            json1.put("skip", skip);

            postData(url, json1.toString(), "JobFetchMemberPhotos");
        } catch (JSONException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Camera Images Per Department (New-Development)

    public void sendIMEA() throws IOException {
        String TAG = "SendSynch";
        JSONObject mainObject = new JSONObject();
        try {

            mainObject.put("user_id", "" + doa.getUSERID());
            mainObject.put("deviceId", GlobalVariabless.getIMEIDeviceId(getApplicationContext()));

            Log.e(TAG, "sendIMEA: " + GlobalVariabless.getIMEIDeviceId(getApplicationContext()));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        url = surl + "Administration/NewSystemLogs/AddsyncHistory";
        service_url = url;
        HTTPjob = "jobSendSynch";
        Params = mainObject.toString();

        postData(url, mainObject.toString(), "jobSendSynch");


    }


}