package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.megvii.demoface.camera.CameraFactory;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceDetail;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceLmks;
import com.megvii.facepp.multi.sdk.denselmk.DlmkPoint;

import java.util.ArrayList;

public class FaceFrameDlmkView extends View {
    private int width;
    private int height;

    private int mCameraHeight;
    private int mCameraWidth;
    private Paint paint;
    private Paint rectPaint;
    private boolean isBackCamera;

    public FaceFrameDlmkView(Context context) {
        super(context);
        init();
    }

    public FaceFrameDlmkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FaceFrameDlmkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mCameraHeight = CameraFactory.mHeight;
        mCameraWidth = CameraFactory.mWidth;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(5);
        paint.setColor(Color.parseColor("#4DF3FD"));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private ArrayList<DlmkFaceDetail> faceDetails = new ArrayList<>();

    public void setResult(ArrayList<DlmkFaceDetail> faceDetails, boolean isBackCamera) {
        this.faceDetails.clear();
        if (faceDetails!=null){
            this.faceDetails.addAll(faceDetails);
        }
        this.isBackCamera = isBackCamera;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!faceDetails.isEmpty()) {
            long startTime = System.currentTimeMillis();
            for (DlmkFaceDetail faceDetail : faceDetails) {
                int detailNum = faceDetail.detailNum;
                for (int i = 0; i < detailNum; i++) {
                    DlmkFaceLmks lmk = faceDetail.lmk[i];
                    int pointNum = lmk.pointNum;
                    for (int j = 0; j < pointNum; j++) {
                        DlmkPoint point = lmk.data[j];
                        float x = point.x;
                        float y = point.y;
                        float relativeX = (y * 1.0f / mCameraHeight) * width;
                        float relativeY = ((mCameraWidth - x) / mCameraWidth) * height;
                        relativeX = width - relativeX;
                        if (isBackCamera) {
                            relativeY = height - relativeY;
                        }
                        canvas.drawCircle(relativeX, relativeY, 1.5f, paint);
                    }
                }
            }
            Log.e("onDraw","timecost:"+(System.currentTimeMillis()-startTime));
        }
    }
}


