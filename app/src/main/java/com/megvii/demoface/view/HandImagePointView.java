package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.megvii.demoface.R;
import com.megvii.facepp.multi.sdk.handskeleton.HandInfo;
import com.megvii.facepp.multi.sdk.handskeleton.HandPoint;
import com.megvii.facepp.multi.sdk.handskeleton.HandRect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HandImagePointView extends View {
    private Bitmap bitmap;
    private int width;
    private int height;

    public HandImagePointView(Context context) {
        super(context);
    }

    public HandImagePointView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HandImagePointView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private HandInfo[] handInfos = null;
    private ArrayList<Map> skeletonInfoList = new ArrayList<>();

    public void setBitmapAndResult(Bitmap bitmap, HandInfo[] handInfos) {
        this.handInfos = handInfos;
        skeletonInfoList.clear();
        if (handInfos != null) {
            for (HandInfo handInfo : handInfos) {
                Map<Integer, HandPoint> triangleVBMaps = new HashMap<>();
                int detailNum = handInfo.getDetailNum();
                for (int i = 0; i < detailNum; i++) {
                    HandPoint point = handInfo.getPointDetails()[i];
                    triangleVBMaps.put(point.getPointType(), point);
                }
                skeletonInfoList.add(triangleVBMaps);
            }
        }
        this.bitmap = bitmap;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int rectStroke = getResources().getDimensionPixelSize(R.dimen.dp1);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(rectStroke);
        paint.setColor(Color.parseColor("#7EB9FF"));

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.parseColor("#4DF3FD"));
        rectPaint.setStrokeWidth(rectStroke);
        rectPaint.setStyle(Paint.Style.STROKE);
        rectPaint.setAntiAlias(true);
        if (bitmap != null) {
            Bitmap resultBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas bitmapCanvas = new Canvas(resultBitmap);
            bitmapCanvas.drawBitmap(bitmap, new Matrix(), new Paint());

            for (int i = 0; i < skeletonInfoList.size(); i++) {
                Map<Integer, HandPoint> pointMap = skeletonInfoList.get(i);
                Path path1 = new Path();
                Path path2 = new Path();
                Path path3 = new Path();
                Path path4 = new Path();
                Path path5 = new Path();
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_WRIST.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_WRIST.ordinal());
                    path1.moveTo(point.getX(), point.getY());
                    path2.moveTo(point.getX(), point.getY());
                    path3.moveTo(point.getX(), point.getY());
                    path4.moveTo(point.getX(), point.getY());
                    path5.moveTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_TMCP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_TMCP.ordinal());
                    path1.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_TPIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_TPIP.ordinal());
                    path1.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_TDIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_TDIP.ordinal());
                    path1.lineTo(point.getX(), point.getY());
                }

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_TTIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_TTIP.ordinal());
                    path1.lineTo(point.getX(), point.getY());
                }
                rectPaint.setColor(Color.parseColor("#4D90FD"));
                bitmapCanvas.drawPath(path1, rectPaint);

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_IMCP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_IMCP.ordinal());
                    path2.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_IPIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_IPIP.ordinal());
                    path2.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_IDIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_IDIP.ordinal());
                    path2.lineTo(point.getX(), point.getY());
                }

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_ITIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_ITIP.ordinal());
                    path2.lineTo(point.getX(), point.getY());
                }
                rectPaint.setColor(Color.parseColor("#F74F9E"));
                bitmapCanvas.drawPath(path2, rectPaint);

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_MMCP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_MMCP.ordinal());
                    path3.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_MPIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_MPIP.ordinal());
                    path3.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_MDIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_MDIP.ordinal());
                    path3.lineTo(point.getX(), point.getY());
                }

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_MTIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_MTIP.ordinal());
                    path3.lineTo(point.getX(), point.getY());
                }
                rectPaint.setColor(Color.parseColor("#4DF3FD"));
                bitmapCanvas.drawPath(path3, rectPaint);

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_RMCP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_RMCP.ordinal());
                    path4.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_RPIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_RPIP.ordinal());
                    path4.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_RDIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_RDIP.ordinal());
                    path4.lineTo(point.getX(), point.getY());
                }

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_RTIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_RTIP.ordinal());
                    path4.lineTo(point.getX(), point.getY());
                }
                rectPaint.setColor(Color.parseColor("#7B76FF"));
                bitmapCanvas.drawPath(path4, rectPaint);

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_PMCP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_PMCP.ordinal());
                    path5.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_PPIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_PPIP.ordinal());
                    path5.lineTo(point.getX(), point.getY());
                }
                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_PDIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_PDIP.ordinal());
                    path5.lineTo(point.getX(), point.getY());
                }

                if (pointMap.containsKey(HandInfo.HandPointTag.MGH_PTIP.ordinal())) {
                    HandPoint point = pointMap.get(HandInfo.HandPointTag.MGH_PTIP.ordinal());
                    path5.lineTo(point.getX(), point.getY());
                }
                rectPaint.setColor(Color.parseColor("#53FD4D"));
                bitmapCanvas.drawPath(path5, rectPaint);

                for (Map.Entry<Integer, HandPoint> entry : pointMap.entrySet()) {
                    HandPoint point = entry.getValue();
                    paint.setColor(Color.BLACK);
                    bitmapCanvas.drawCircle(point.getX(), point.getY(), 8, paint);
                    paint.setColor(Color.WHITE);
                    bitmapCanvas.drawCircle(point.getX(), point.getY(), 4, paint);
                }

            }

            if (handInfos != null) {
                for (HandInfo handInfo : handInfos) {
                    HandRect handRect = handInfo.getHandRect();
                    rectPaint.setColor(Color.parseColor("#4DF3FD"));
                    bitmapCanvas.drawRect(new Rect(handRect.left, handRect.top, handRect.right, handRect.bottom), rectPaint);
                    Log.w("confidence",handInfo.getConfidence()+"");
                    Bitmap bitmap;
                    HandInfo.MG_GESTURE_TYPE gesture_type = handInfo.getGesture();
                    switch (gesture_type) {
                        case MG_GESTURE_TYPE_HEART_A:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_a);
                            break;
                        case MG_GESTURE_TYPE_HEART_B:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_b);
                            break;
                        case MG_GESTURE_TYPE_HEART_C:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_c);
                            break;
                        case MG_GESTURE_TYPE_HEART_D:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_d);
                            break;
                        case MG_GESTURE_TYPE_OK:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_hand_ok);
                            break;
                        case MG_GESTURE_TYPE_HAND_OPEN:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_hand_open);
                            break;
                        case MG_GESTURE_TYPE_THUMB_UP:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thumb_up);
                            break;
                        case MG_GESTURE_TYPE_THUMB_DOWN:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thumb_down);
                            break;
                        case MG_GESTURE_TYPE_ROCK:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_rock);
                            break;
                        case MG_GESTURE_TYPE_NAMASTE:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_namaste);
                            break;
                        case MG_GESTURE_TYPE_PALM_UP:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_palm_up);
                            break;
                        case MG_GESTURE_TYPE_FIST:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_fist);
                            break;
                        case MG_GESTURE_TYPE_INDEX_FINGER_UP:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_index_finger_up);
                            break;
                        case MG_GESTURE_TYPE_DOUBLE_FINGER_UP:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_double_finger_up);
                            break;
                        case MG_GESTURE_TYPE_VICTORY:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_victory);
                            break;
                        case MG_GESTURE_TYPE_BIG_V:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_big_v);
                            break;
                        case MG_GESTURE_TYPE_PHONECALL:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_phonecall);
                            break;
                        case MG_GESTURE_TYPE_BEG:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_beg);
                            break;
                        case MG_GESTURE_TYPE_THANKS:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thanks);
                            break;
                        default:
                            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_unknow);
                            break;
                    }

                    Rect srcRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                    int dstLeft = (int) (handRect.right - (handRect.right - handRect.left) * 0.33f);
                    int dstTop = handRect.top;
                    int dstRight = handRect.right;
                    int dstBottom = (int) (handRect.top + (handRect.right - handRect.left) * 0.33f);
                    bitmapCanvas.drawBitmap(bitmap, srcRect, new Rect(dstLeft, dstTop, dstRight, dstBottom), rectPaint);
                }
            }
            canvas.drawBitmap(resultBitmap, new Rect(0, 0, resultBitmap.getWidth(), resultBitmap.getHeight()), new Rect(0, 0, width, height), new Paint());
        }
    }
}


