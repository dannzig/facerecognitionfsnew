package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceDetail;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceLmks;
import com.megvii.facepp.multi.sdk.denselmk.DlmkPoint;

import java.util.ArrayList;

public class FaceImageDlmkView extends View {
    private Bitmap bitmap;
    private int width;
    private int height;

    public FaceImageDlmkView(Context context) {
        super(context);
    }

    public FaceImageDlmkView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FaceImageDlmkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private ArrayList<DlmkFaceDetail> faceDetails = new ArrayList<>();
    private int detailType;

    public void updateDetailType(int detailType){
        this.detailType = detailType;
        invalidate();
    }

    public void setBitmapAndResult(Bitmap bitmap, int detailType, ArrayList<DlmkFaceDetail> faceDetails) {
        this.bitmap = bitmap;
        this.detailType = detailType;
        this.faceDetails.clear();
        this.faceDetails.addAll(faceDetails);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        long startTimes = System.currentTimeMillis();
        int rectStroke = 5;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(rectStroke);
        paint.setColor(Color.parseColor("#4DF3FD"));

        if (bitmap != null) {
            Bitmap resultBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas bitmapCanvas = new Canvas(resultBitmap);
            bitmapCanvas.drawBitmap(bitmap, new Matrix(), new Paint());

            if (!faceDetails.isEmpty()) {
                for (DlmkFaceDetail faceDetail : faceDetails) {
                    int detailNum = faceDetail.detailNum;
                    for (int i = 0; i < detailNum; i++) {
                        DlmkFaceLmks lmk = faceDetail.lmk[i];
                        if ((lmk.detailType & detailType) != 0) {
                            int pointNum = lmk.pointNum;
                            for (int j = 0; j < pointNum; j++) {
                                DlmkPoint point = lmk.data[j];
                                bitmapCanvas.drawCircle(point.x, point.y, 1f, paint);
                            }
                        }
                    }
                }
            }
            canvas.drawBitmap(resultBitmap, new Rect(0, 0, resultBitmap.getWidth(), resultBitmap.getHeight()), new Rect(0, 0, width, height), new Paint());
            Log.w("onDraw", "timeCost:" + (System.currentTimeMillis() - startTimes));
        }
    }
}


