package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.facepp.multi.sdk.handskeleton.HandInfo;
import com.megvii.facepp.multi.sdk.handskeleton.HandRect;

import java.util.ArrayList;
import java.util.Map;

public class HandFramePointView extends View {
    private int width;
    private int height;
    private Paint paint;
    private Paint rectPaint;

    private Bitmap bitmapHeartA;
    private Bitmap bitmapHeartB;
    private Bitmap bitmapHeartC;
    private Bitmap bitmapHeartD;
    private Bitmap bitmapOK;
    private Bitmap bitmapHandOpen;
    private Bitmap bitmapThumbUp;
    private Bitmap bitmapThumbDown;
    private Bitmap bitmapRock;
    private Bitmap bitmapNamaste;
    private Bitmap bitmapPalmUp;
    private Bitmap bitmapFist;
    private Bitmap bitmapIndexFingerUp;
    private Bitmap bitmapDoubleFingerUp;
    private Bitmap bitmapVictory;
    private Bitmap bitmapBigV;
    private Bitmap bitmapPhoneCall;
    private Bitmap bitmapBeg;
    private Bitmap bitmapThanks;
    private Bitmap bitmapUnknow;

    private int mCameraHeight;
    private int mCameraWidth;
    private boolean isBackCamera;

    public HandFramePointView(Context context) {
        super(context);
        init();
    }

    public HandFramePointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HandFramePointView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mCameraHeight = CameraFactory.mHeight;
        mCameraWidth = CameraFactory.mWidth;

        int rectStroke = getResources().getDimensionPixelSize(R.dimen.dp1);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(rectStroke);
        paint.setColor(Color.parseColor("#7EB9FF"));

        rectPaint = new Paint();
        rectPaint.setColor(Color.parseColor("#4DF3FD"));
        rectPaint.setStrokeWidth(rectStroke);
        rectPaint.setStyle(Paint.Style.STROKE);
        rectPaint.setAntiAlias(true);

        bitmapHeartA = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_a);
        bitmapHeartB = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_b);
        bitmapHeartC = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_c);
        bitmapHeartD = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_heart_d);
        bitmapOK = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_hand_ok);
        bitmapHandOpen = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_hand_open);
        bitmapThumbUp = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thumb_up);
        bitmapThumbDown = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thumb_down);
        bitmapRock = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_rock);
        bitmapNamaste = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_namaste);
        bitmapPalmUp = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_palm_up);
        bitmapFist = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_fist);
        bitmapIndexFingerUp = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_index_finger_up);
        bitmapDoubleFingerUp = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_double_finger_up);
        bitmapVictory = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_victory);
        bitmapBigV = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_big_v);
        bitmapPhoneCall = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_phonecall);
        bitmapBeg = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_beg);
        bitmapThanks = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_thanks);
        bitmapUnknow = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_gesture_unknow);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private HandInfo[] handInfos = null;
    private ArrayList<Map> skeletonInfoList = new ArrayList<>();

    public void setBitmapAndResult(HandInfo[] handInfos, boolean isBackCamera) {
        this.handInfos = handInfos;
        this.isBackCamera = isBackCamera;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (handInfos != null) {
            for (HandInfo handInfo : handInfos) {
                 HandRect handRect = handInfo.getHandRect();
                rectPaint.setColor(Color.parseColor("#4DF3FD"));

                int left = (int) ((handRect.top * 1.0f / mCameraHeight) * width);
                int right = (int) ((handRect.bottom * 1.0f / mCameraHeight) * width);
                int top = (int) (((mCameraWidth - handRect.right) * 1.0f / mCameraWidth) * height);
                int bottom = (int) (((mCameraWidth - handRect.left) * 1.0f / mCameraWidth) * height);
                if (isBackCamera) {
                    top = (int) ((handRect.left * 1.0f / mCameraWidth) * height);
                    bottom = (int) ((handRect.right * 1.0f / mCameraWidth) * height);
                }
                Rect hRect = new Rect(width - right, top, width - left, bottom);
                canvas.drawRect(hRect, rectPaint);

//                canvas.drawRect(new Rect(handRect.left,handRect.top,handRect.right,handRect.bottom),rectPaint);
                Log.w("confidence", handInfo.getConfidence() + "");
                Bitmap bitmap;
                HandInfo.MG_GESTURE_TYPE gesture_type = handInfo.getGesture();
                Log.w("gesture_type","gesture_type:"+gesture_type);
                switch (gesture_type) {
                    case MG_GESTURE_TYPE_HEART_A:
                        bitmap = bitmapHeartA;
                        break;
                    case MG_GESTURE_TYPE_HEART_B:
                        bitmap = bitmapHeartB;
                        break;
                    case MG_GESTURE_TYPE_HEART_C:
                        bitmap = bitmapHeartC;
                        break;
                    case MG_GESTURE_TYPE_HEART_D:
                        bitmap = bitmapHeartD;
                        break;
                    case MG_GESTURE_TYPE_OK:
                        bitmap = bitmapOK;
                        break;
                    case MG_GESTURE_TYPE_HAND_OPEN:
                        bitmap = bitmapHandOpen;
                        break;
                    case MG_GESTURE_TYPE_THUMB_UP:
                        bitmap = bitmapThumbUp;
                        break;
                    case MG_GESTURE_TYPE_THUMB_DOWN:
                        bitmap = bitmapThumbDown;
                        break;
                    case MG_GESTURE_TYPE_ROCK:
                        bitmap = bitmapRock;
                        break;
                    case MG_GESTURE_TYPE_NAMASTE:
                        bitmap = bitmapNamaste;
                        break;
                    case MG_GESTURE_TYPE_PALM_UP:
                        bitmap = bitmapPalmUp;
                        break;
                    case MG_GESTURE_TYPE_FIST:
                        bitmap = bitmapFist;
                        break;
                    case MG_GESTURE_TYPE_INDEX_FINGER_UP:
                        bitmap = bitmapIndexFingerUp;
                        break;
                    case MG_GESTURE_TYPE_DOUBLE_FINGER_UP:
                        bitmap = bitmapDoubleFingerUp;
                        break;
                    case MG_GESTURE_TYPE_VICTORY:
                        bitmap = bitmapVictory;
                        break;
                    case MG_GESTURE_TYPE_BIG_V:
                        bitmap = bitmapBigV;
                        break;
                    case MG_GESTURE_TYPE_PHONECALL:
                        bitmap = bitmapPhoneCall;
                        break;
                    case MG_GESTURE_TYPE_BEG:
                        bitmap = bitmapBeg;
                        break;
                    case MG_GESTURE_TYPE_THANKS:
                        bitmap = bitmapThanks;
                        break;
                    default:
                        bitmap = bitmapUnknow;
                        break;
                }

                Rect srcRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                int dstLeft = (int) (( width - left) - hRect.width() * 0.33f);
                int dstTop = top;
                int dstRight =  width - left;
                int dstBottom = (int) (top +hRect.width() * 0.33f);
                canvas.drawBitmap(bitmap, srcRect, new Rect(dstLeft, dstTop, dstRight, dstBottom), rectPaint);
            }
        }
    }
}


