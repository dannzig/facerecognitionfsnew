package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.megvii.facepp.multi.sdk.skeleton.SkeletonInfo;
import com.megvii.facepp.multi.sdk.skeleton.SkeletonPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SkeletonImagePoint extends View {
    private Bitmap bitmap;
    private int width;
    private int height;
    private static final int head = SkeletonInfo.PointTag.MGSkeleton_HEAD.ordinal();
    private static final int neck = SkeletonInfo.PointTag.MGSkeleton_NECK.ordinal();
    private static final int leftHand = SkeletonInfo.PointTag.MGSkeleton_LEFTHAND.ordinal();
    private static final int leftElbow = SkeletonInfo.PointTag.MGSkeleton_LEFTELBOW.ordinal();
    private static final int leftShoulder = SkeletonInfo.PointTag.MGSkeleton_LEFTSHOULDER.ordinal();
    private static final int rightHand = SkeletonInfo.PointTag.MGSkeleton_RIGHTHAND.ordinal();
    private static final int rightElbow = SkeletonInfo.PointTag.MGSkeleton_RIGHTELBOW.ordinal();
    private static final int rightShoulder = SkeletonInfo.PointTag.MGSkeleton_RIGHTSHOULDER.ordinal();
    private static final int leftButtocks = SkeletonInfo.PointTag.MGSkeleton_LEFTBUTTOCKS.ordinal();
    private static final int leftKnee = SkeletonInfo.PointTag.MGSkeleton_LEFTKNEE.ordinal();
    private static final int leftFoot = SkeletonInfo.PointTag.MGSkeleton_LEFTFOOT.ordinal();
    private static final int rightButtocks = SkeletonInfo.PointTag.MGSkeleton_RIGHTBUTTOCKS.ordinal();
    private static final int rightKnee = SkeletonInfo.PointTag.MGSkeleton_RIGHTKNEE.ordinal();
    private static final int rightFoot = SkeletonInfo.PointTag.MGSkeleton_RIGHTFOOT.ordinal();

    public SkeletonImagePoint(Context context) {
        super(context);
    }

    public SkeletonImagePoint(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SkeletonImagePoint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private ArrayList<Map> skeletonInfoList = new ArrayList<>();

    public void setBitmapAndResult(Bitmap bitmap, SkeletonInfo[] skeletonInfos) {
        this.bitmap = bitmap;
        skeletonInfoList.clear();
        if (skeletonInfos != null) {
            for (int i = 0; i < skeletonInfos.length; i++) {
                Map<Integer, PointF> triangleVBMaps = new HashMap<>();
                int[] tags = skeletonInfos[i].getPointTags();
                float[] scores = skeletonInfos[i].getScore();
                SkeletonPoint[] points = skeletonInfos[i].getPoints2d();
                for (int j = 0; j < points.length; j++) {
                    if (points[j].getX() == 0 && points[j].getY() == 0) {
                        continue;
                    }
                    if (scores[j] < SkeletonInfo.MIN_NEED_SCORE) {
                        continue;
                    }
                    float relativeX = points[j].getX();
                    float relativeY = points[j].getY();
                    PointF pointF = new PointF(relativeX, relativeY);
                    triangleVBMaps.put(tags[j], pointF);
                }
                skeletonInfoList.add(triangleVBMaps);
            }
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int rectStroke = 5;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(rectStroke);
        if (bitmap != null) {
            Bitmap resultBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas bitmapCanvas = new Canvas(resultBitmap);
            bitmapCanvas.drawBitmap(bitmap, new Matrix(), new Paint());

            for (int i = 0; i < skeletonInfoList.size(); i++) {
                Map<Integer, PointF> pointMap = skeletonInfoList.get(i);
                if (pointMap.containsKey(head) && pointMap.containsKey(neck)) {
                    paint.setColor(Color.parseColor("#ECFD4D"));
                    PointF startPoint = pointMap.get(head);
                    PointF endPoint = pointMap.get(neck);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(rightShoulder) && pointMap.containsKey(neck)) {
                    paint.setColor(Color.parseColor("#F74F9E"));
                    PointF startPoint = pointMap.get(rightShoulder);
                    PointF endPoint = pointMap.get(neck);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(rightShoulder) && pointMap.containsKey(rightElbow)) {
                    paint.setColor(Color.parseColor("#53FD4D"));
                    PointF startPoint = pointMap.get(rightShoulder);
                    PointF endPoint = pointMap.get(rightElbow);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(rightHand) && pointMap.containsKey(rightElbow)) {
                    paint.setColor(Color.parseColor("#FD4D4D"));
                    PointF startPoint = pointMap.get(rightHand);
                    PointF endPoint = pointMap.get(rightElbow);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(leftShoulder) && pointMap.containsKey(neck)) {
                    paint.setColor(Color.parseColor("#F74F9E"));
                    PointF startPoint = pointMap.get(leftShoulder);
                    PointF endPoint = pointMap.get(neck);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(leftShoulder) && pointMap.containsKey(leftElbow)) {
                    paint.setColor(Color.parseColor("#4DF3FD"));
                    PointF startPoint = pointMap.get(leftShoulder);
                    PointF endPoint = pointMap.get(leftElbow);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(leftHand) && pointMap.containsKey(leftElbow)) {
                    paint.setColor(Color.parseColor("#4D90FD"));
                    PointF startPoint = pointMap.get(leftHand);
                    PointF endPoint = pointMap.get(leftElbow);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(rightButtocks) && pointMap.containsKey(rightKnee)) {
                    paint.setColor(Color.parseColor("#4DFDD2"));
                    PointF startPoint = pointMap.get(rightButtocks);
                    PointF endPoint = pointMap.get(rightKnee);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(rightFoot) && pointMap.containsKey(rightKnee)) {
                    paint.setColor(Color.parseColor("#7B76FF"));
                    PointF startPoint = pointMap.get(rightFoot);
                    PointF endPoint = pointMap.get(rightKnee);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(leftButtocks) && pointMap.containsKey(leftKnee)) {
                    paint.setColor(Color.parseColor("#C94DFD"));
                    PointF startPoint = pointMap.get(leftButtocks);
                    PointF endPoint = pointMap.get(leftKnee);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                if (pointMap.containsKey(leftFoot) && pointMap.containsKey(leftKnee)) {
                    paint.setColor(Color.parseColor("#FD914D"));
                    PointF startPoint = pointMap.get(leftFoot);
                    PointF endPoint = pointMap.get(leftKnee);
                    bitmapCanvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
                }

                for (Map.Entry<Integer, PointF> entry : pointMap.entrySet()) {
                    PointF pointF = entry.getValue();
                    paint.setColor(Color.BLACK);
                    bitmapCanvas.drawCircle(pointF.x, pointF.y, 12, paint);
                    paint.setColor(Color.WHITE);
                    bitmapCanvas.drawCircle(pointF.x, pointF.y, 8, paint);
                }
            }
            canvas.drawBitmap(resultBitmap, new Rect(0, 0, resultBitmap.getWidth(), resultBitmap.getHeight()), new Rect(0, 0, width, height), new Paint());
        }

    }
}
