package com.megvii.demoface.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.R;

public class SegmentOptionView extends RelativeLayout {
    private Drawable mDrawable;
    private String mText;
    private boolean isChecked;
    private ImageView mImageView;
    private ImageView mCheckedImageView;
    private TextView mTextView;
    private String checkedColor = "#2E2932";
    private String unCheckedColor = "#7C7780";

    public SegmentOptionView(Context context) {
        super(context);
    }

    public SegmentOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //加载视图的布局
        LayoutInflater.from(context).inflate(R.layout.activity_segment_option, this, true);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.segment_options);
        mDrawable = a.getDrawable(R.styleable.segment_options_drawable);
        mText = a.getString(R.styleable.segment_options_text);
        isChecked = a.getBoolean(R.styleable.segment_options_checked, false);
        a.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mImageView = findViewById(R.id.iv_seg_option);
        mTextView = findViewById(R.id.tv_seg_option);
        mCheckedImageView = findViewById(R.id.iv_seg_option_checked);
        mTextView.setText(mText);
        mImageView.setImageDrawable(mDrawable);
        if (!isChecked) {
            mCheckedImageView.setVisibility(GONE);
            mTextView.setTextColor(Color.parseColor(unCheckedColor));
        } else {
            mCheckedImageView.setVisibility(VISIBLE);
            mTextView.setTextColor(Color.parseColor(checkedColor));
        }
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        if (!isChecked) {
            mCheckedImageView.setVisibility(GONE);
            mTextView.setTextColor(Color.parseColor(unCheckedColor));
        } else {
            mCheckedImageView.setVisibility(VISIBLE);
            mTextView.setTextColor(Color.parseColor(checkedColor));
        }
    }

    public boolean isChecked() {
        return isChecked;
    }
}
