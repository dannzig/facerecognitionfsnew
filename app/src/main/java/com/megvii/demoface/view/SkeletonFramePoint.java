package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.megvii.demoface.camera.CameraFactory;
import com.megvii.facepp.multi.sdk.skeleton.SkeletonInfo;
import com.megvii.facepp.multi.sdk.skeleton.SkeletonPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SkeletonFramePoint extends View {
    private int width;
    private int height;
    private int mCameraHeight;
    private int mCameraWidth;
    private Paint paint;

    private static final int head = SkeletonInfo.PointTag.MGSkeleton_HEAD.ordinal();
    private static final int neck = SkeletonInfo.PointTag.MGSkeleton_NECK.ordinal();
    private static final int leftHand = SkeletonInfo.PointTag.MGSkeleton_LEFTHAND.ordinal();
    private static final int leftElbow = SkeletonInfo.PointTag.MGSkeleton_LEFTELBOW.ordinal();
    private static final int leftShoulder = SkeletonInfo.PointTag.MGSkeleton_LEFTSHOULDER.ordinal();
    private static final int rightHand = SkeletonInfo.PointTag.MGSkeleton_RIGHTHAND.ordinal();
    private static final int rightElbow = SkeletonInfo.PointTag.MGSkeleton_RIGHTELBOW.ordinal();
    private static final int rightShoulder = SkeletonInfo.PointTag.MGSkeleton_RIGHTSHOULDER.ordinal();
    private static final int leftButtocks = SkeletonInfo.PointTag.MGSkeleton_LEFTBUTTOCKS.ordinal();
    private static final int leftKnee = SkeletonInfo.PointTag.MGSkeleton_LEFTKNEE.ordinal();
    private static final int leftFoot = SkeletonInfo.PointTag.MGSkeleton_LEFTFOOT.ordinal();
    private static final int rightButtocks = SkeletonInfo.PointTag.MGSkeleton_RIGHTBUTTOCKS.ordinal();
    private static final int rightKnee = SkeletonInfo.PointTag.MGSkeleton_RIGHTKNEE.ordinal();
    private static final int rightFoot = SkeletonInfo.PointTag.MGSkeleton_RIGHTFOOT.ordinal();


    public SkeletonFramePoint(Context context) {
        super(context);
        init();
    }

    public SkeletonFramePoint(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SkeletonFramePoint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mCameraHeight = CameraFactory.mHeight;
        mCameraWidth = CameraFactory.mWidth;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(5);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    //    private SkeletonInfo[] skeletonInfos;
    private ArrayList<Map> skeletonInfoList = new ArrayList<>();

    public void setPointResult(SkeletonInfo[] skeletonInfos, boolean isBackCamera) {
        skeletonInfoList.clear();
        if (skeletonInfos != null) {
            for (int i = 0; i < skeletonInfos.length; i++) {
                Map<Integer, PointF> triangleVBMaps = new HashMap<>();
                int[] tags = skeletonInfos[i].getPointTags();
                float[] scores = skeletonInfos[i].getScore();
                SkeletonPoint[] points = skeletonInfos[i].getPoints2d();
                for (int j = 0; j < points.length; j++) {
                    if (points[j].getX() == 0 && points[j].getY() == 0) {
                        continue;
                    }
                    if (scores[j] <= SkeletonInfo.MIN_NEED_SCORE) {
                        continue;
                    }

                    Log.w("PointResult", "human index = " + i + ",score index= " + j + "," + scores[j]);

                    float x = points[j].getX();
                    float y = points[j].getY();
                    float relativeX = (y * 1.0f / mCameraHeight) * width;
                    float relativeY = ((mCameraWidth - x) / mCameraWidth) * height;
                    relativeX = width - relativeX;
                    if (isBackCamera) {
                        relativeY = height - relativeY;
                    }
                    PointF pointF = new PointF(relativeX, relativeY);
                    triangleVBMaps.put(tags[j], pointF);
                }
                skeletonInfoList.add(triangleVBMaps);
            }
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < skeletonInfoList.size(); i++) {
            Map<Integer, PointF> pointMap = skeletonInfoList.get(i);
            if (pointMap.containsKey(head) && pointMap.containsKey(neck)) {
                paint.setColor(Color.parseColor("#ECFD4D"));
                PointF startPoint = pointMap.get(head);
                PointF endPoint = pointMap.get(neck);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(rightShoulder) && pointMap.containsKey(neck)) {
                paint.setColor(Color.parseColor("#F74F9E"));
                PointF startPoint = pointMap.get(rightShoulder);
                PointF endPoint = pointMap.get(neck);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(rightShoulder) && pointMap.containsKey(rightElbow)) {
                paint.setColor(Color.parseColor("#53FD4D"));
                PointF startPoint = pointMap.get(rightShoulder);
                PointF endPoint = pointMap.get(rightElbow);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(rightHand) && pointMap.containsKey(rightElbow)) {
                paint.setColor(Color.parseColor("#FD4D4D"));
                PointF startPoint = pointMap.get(rightHand);
                PointF endPoint = pointMap.get(rightElbow);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(leftShoulder) && pointMap.containsKey(neck)) {
                paint.setColor(Color.parseColor("#F74F9E"));
                PointF startPoint = pointMap.get(leftShoulder);
                PointF endPoint = pointMap.get(neck);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(leftShoulder) && pointMap.containsKey(leftElbow)) {
                paint.setColor(Color.parseColor("#4DF3FD"));
                PointF startPoint = pointMap.get(leftShoulder);
                PointF endPoint = pointMap.get(leftElbow);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(leftHand) && pointMap.containsKey(leftElbow)) {
                paint.setColor(Color.parseColor("#4D90FD"));
                PointF startPoint = pointMap.get(leftHand);
                PointF endPoint = pointMap.get(leftElbow);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(rightButtocks) && pointMap.containsKey(rightKnee)) {
                paint.setColor(Color.parseColor("#4DFDD2"));
                PointF startPoint = pointMap.get(rightButtocks);
                PointF endPoint = pointMap.get(rightKnee);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(rightFoot) && pointMap.containsKey(rightKnee)) {
                paint.setColor(Color.parseColor("#7B76FF"));
                PointF startPoint = pointMap.get(rightFoot);
                PointF endPoint = pointMap.get(rightKnee);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(leftButtocks) && pointMap.containsKey(leftKnee)) {
                paint.setColor(Color.parseColor("#C94DFD"));
                PointF startPoint = pointMap.get(leftButtocks);
                PointF endPoint = pointMap.get(leftKnee);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            if (pointMap.containsKey(leftFoot) && pointMap.containsKey(leftKnee)) {
                paint.setColor(Color.parseColor("#FD914D"));
                PointF startPoint = pointMap.get(leftFoot);
                PointF endPoint = pointMap.get(leftKnee);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, paint);
            }

            for (Map.Entry<Integer, PointF> entry : pointMap.entrySet()) {
                PointF pointF = entry.getValue();
                paint.setColor(Color.BLACK);
                canvas.drawCircle(pointF.x, pointF.y, 12, paint);
                paint.setColor(Color.WHITE);
                canvas.drawCircle(pointF.x, pointF.y, 8, paint);
            }

//            SkeletonPoint[] points = skeletonInfos[i].getPoints2d();
//            for (int j = 0; j < points.length; j++) {
//                if (j > 13) {
//                    continue;
//                }
//                float x = points[j].getX();
//                float y = points[j].getY();
//                float xInt = (y * 1.0f / mCameraHeight) * width;
//                float yInt = ((mCameraWidth - x) / mCameraWidth) * height;
//                if (true) {
//                    xInt = width - xInt;
//                    yInt = height - yInt;
//                }
//                rectPaint.setColor(Color.BLACK);
//                canvas.drawCircle(xInt, yInt, 12, rectPaint);
//                rectPaint.setColor(Color.WHITE);
//                canvas.drawCircle(xInt, yInt, 8, rectPaint);
//            }


//                Rect rect = skeletonInfos[i].getRect();
//                int left = (int) ((rect.top * 1.0f / mCameraHeight) * width);
//                int right = (int) ((rect.bottom * 1.0f / mCameraHeight) * width);
//                int top = (int) (((mCameraWidth - rect.right) * 1.0f / mCameraWidth) * height);
//                int bottom = (int) (((mCameraWidth - rect.left) * 1.0f / mCameraWidth) * height);
//                if (true) {
//                    top = (int) ((rect.left * 1.0f / mCameraWidth) * height);
//                    bottom = (int) ((rect.right * 1.0f / mCameraWidth) * height);
//                }
//                //镜像
//                canvas.drawRect(new Rect(width - right, top, width - left, bottom), rectPaint);
        }
    }
}
