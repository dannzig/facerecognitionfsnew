package com.megvii.demoface.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.R;

public class DenseLmkDetailTypeView extends RelativeLayout {
    private Drawable mDrawable;
    private String mText;
    private boolean isChecked;
    private CheckBox mCheckBox;
    private ImageView mCheckedImageView;
    private TextView mTextView;
    private String checkedColor = "#2E2932";
    private String unCheckedColor = "#7C7780";

    public DenseLmkDetailTypeView(Context context) {
        super(context);
    }

    public DenseLmkDetailTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //加载视图的布局
        LayoutInflater.from(context).inflate(R.layout.activity_denselmk_detailtype, this, true);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.denselmk_type);
        mDrawable = a.getDrawable(R.styleable.denselmk_type_lmkdrawable);
        mText = a.getString(R.styleable.denselmk_type_lmktext);
        isChecked = a.getBoolean(R.styleable.denselmk_type_lmkchecked, false);
        a.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mCheckBox = findViewById(R.id.cb_denselmk_type);
        mTextView = findViewById(R.id.tv_denselmk_type);
        mCheckedImageView = findViewById(R.id.iv_denselmk_checked);
        mTextView.setText(mText);
        mCheckBox.setBackground(mDrawable);
        mCheckBox.setChecked(isChecked);
        if (!isChecked) {
            mCheckedImageView.setVisibility(GONE);
            mTextView.setTextColor(Color.parseColor(unCheckedColor));
        } else {
            mCheckedImageView.setVisibility(VISIBLE);
            mTextView.setTextColor(Color.parseColor(checkedColor));
        }
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        mCheckBox.setChecked(isChecked);
        if (!isChecked) {
            mCheckedImageView.setVisibility(GONE);
            mTextView.setTextColor(Color.parseColor(unCheckedColor));
        } else {
            mCheckedImageView.setVisibility(VISIBLE);
            mTextView.setTextColor(Color.parseColor(checkedColor));
        }
    }

    public boolean isChecked() {
        return isChecked;
    }
}
