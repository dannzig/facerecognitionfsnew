package com.megvii.demoface.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.megvii.facepp.multi.sdk.FaceDetectApi;

public class FaceImagePointView extends View {
    private Bitmap bitmap;
    private int width;
    private int height;

    public FaceImagePointView(Context context) {
        super(context);
    }

    public FaceImagePointView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FaceImagePointView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (width != 0 && height != 0) {
            setMeasuredDimension(width, height);
        }
    }

    public void refreshView(int width, int height) {
        this.width = width;
        this.height = height;
        requestLayout();
    }

    private FaceDetectApi.Face[] faces = null;

    public void setBitmapAndResult(Bitmap bitmap, FaceDetectApi.Face[] faces) {
        this.faces = faces;
        this.bitmap = bitmap;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int rectStroke = 5;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(rectStroke);
        paint.setColor(Color.parseColor("#7EB9FF"));

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.parseColor("#7EB9FF"));
        rectPaint.setStrokeWidth(rectStroke);
        rectPaint.setStyle(Paint.Style.STROKE);
        rectPaint.setAntiAlias(true);
        if (bitmap != null) {
            Bitmap resultBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas bitmapCanvas = new Canvas(resultBitmap);
            bitmapCanvas.drawBitmap(bitmap, new Matrix(), new Paint());

            if (faces != null) {
                for (FaceDetectApi.Face face : faces) {
                    PointF[] points = face.points;
                    for (PointF point : points) {
                        bitmapCanvas.drawCircle(point.x, point.y, 2.5f, paint);
                    }

                    Rect rect = face.rect;
                    bitmapCanvas.drawRect(rect,rectPaint);
                }
            }
            canvas.drawBitmap(resultBitmap, new Rect(0, 0, resultBitmap.getWidth(), resultBitmap.getHeight()), new Rect(0, 0, width, height), new Paint());
        }
    }
}


