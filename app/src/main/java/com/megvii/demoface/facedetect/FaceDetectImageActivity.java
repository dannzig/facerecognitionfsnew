package com.megvii.demoface.facedetect;

import android.graphics.Bitmap;
import android.net.Uri;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.FaceImagePointView;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.OnClick;

import static com.megvii.facepp.multi.sdk.FaceDetectApi.FaceppConfig.DETECTION_MODE_DETECT;

public class FaceDetectImageActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.mFaceImagePointView)
    FaceImagePointView mFaceImagePointView;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.rg_facepp_landmark_switch)
    RadioGroup rgFaceppLandmarkSwitch;
    private Bitmap bitmap;

    private FaceDetectApi.Face[] faces = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_face_image;
    }

    @Override
    protected void initView() {
        rgFaceppLandmarkSwitch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int landmarkCount = FaceDetectApi.LMK_84;
                if (checkedId == R.id.rb_facepp_landmark_84) {
                    landmarkCount = FaceDetectApi.LMK_84;
                } else {
                    landmarkCount = FaceDetectApi.LMK_106;
                }
                if (faces != null) {
                    for (FaceDetectApi.Face face : faces) {
                        FaceDetectApi.getInstance().getLandmark(face, landmarkCount, false);
                    }
                    mFaceImagePointView.setBitmapAndResult(bitmap, faces);
                }

            }
        });
    }

    @Override
    protected void initData() {
        DecimalFormat df = new DecimalFormat("0.00");
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String type = getIntent().getStringExtra("type");
        if ("face".equals(type)){
            tvTitleBar.setText("Face Detection");
        }else if ("faceplus".equals(type)){
            tvTitleBar.setText("Face Detection Advanced Edition");
        }
        String path = ConUtil.getRealPathFromURI(this, uri);
        bitmap = ConUtil.getImage(path);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        mFaceImagePointView.refreshView((int) (imageWidth * scale), (int) (imageHeight * scale));
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        long startTime = System.currentTimeMillis();
        FaceDetectApi.FaceppConfig config = FaceDetectApi.getInstance().getFaceppConfig();
        config.face_confidence_filter = 0.6f;
        config.detectionMode = DETECTION_MODE_DETECT;
        FaceDetectApi.getInstance().setFaceppConfig(config);
        faces = FaceDetectApi.getInstance().detectFace(facePPImage);
        StringBuilder sb = new StringBuilder();
        if (faces != null) {
            for (FaceDetectApi.Face face : faces) {
                FaceDetectApi.getInstance().getRect(face, false);
                FaceDetectApi.getInstance().getLandmark(face, FaceDetectApi.LMK_84, false);
                if ("faceplus".equals(type)) {
                    FaceDetectApi.getInstance().getAttribute(face);//获取所有属性
                    sb.append("· ").append("性别：").append(face.female > face.male ? "女" : "男").append("\n");
                    sb.append("· ").append("年龄：").append(face.age).append("\n");
                    sb.append("· ").append("模糊度：").append(df.format(face.blurness)).append("\n");
                    sb.append("· ").append("置信度：").append(df.format(face.confidence)).append("\n");
                    String expression = "";
                    switch (face.expressionType) {
                        case MG_EXPRESSION_TYPE_ANGRY:
                            expression = "angry";
                            break;
                        case MG_EXPRESSION_TYPE_DISGUST:
                            expression = "disgust";
                            break;
                        case MG_EXPRESSION_TYPE_FEAR:
                            expression = "fear";
                            break;
                        case MG_EXPRESSION_TYPE_HAPPY:
                            expression = "happy";
                            break;
                        case MG_EXPRESSION_TYPE_SAD:
                            expression = "sad";
                            break;
                        case MG_EXPRESSION_TYPE_SURPRISE:
                            expression = "surprise";
                            break;
                        case MG_EXPRESSION_TYPE_NEUTRAL:
                            expression = "neutral";
                            break;
                    }
                    sb.append("· ").append("表情：").append(expression).append("\n");
                    sb.append("· ").append("视线(左眼)：").append(face.gazeResult.getLeftEyeGaze().toString()).append("\n");
                    sb.append("· ").append("视线(右眼)：").append(face.gazeResult.getRightEyeGaze().toString()).append("\n");
                    sb.append("· ").append("眼部状态(左眼)：").append(getEyeStatus(face.leftEyeType)).append("\n");
                    sb.append("· ").append("眼部状态(右眼)：").append(getEyeStatus(face.rightEyeType)).append("\n");
                    sb.append("· ").append("嘴部状态：").append(getMouthStatus(face.mouthType)).append("\n");
                    sb.append("· ").append("3DPose：{pitch=").append(face.pitch).append(",roll=").append(face.roll).append(",yaw=").append(face.yaw).append("}\n");
                    sb.append("\n");
                }
            }
        }

        long timeConst = System.currentTimeMillis() - startTime;

        sb.append("单帧耗时(ms):");
        sb.append(timeConst);
        tvDetectTips.setText(sb.toString());
        tvDetectTips.setMovementMethod(ScrollingMovementMethod.getInstance());

        mFaceImagePointView.setBitmapAndResult(bitmap, faces);
    }

    private String getEyeStatus(int eyeType) {
        String reuslt = "";
        switch (eyeType) {
            case 0:
                reuslt = "不戴眼镜，并且睁着眼";
                break;
            case 1:
                reuslt = "不戴眼镜，并且闭着眼";
                break;
            case 2:
                reuslt = "戴着普通眼镜，并且睁着眼";
                break;
            case 3:
                reuslt = "戴着普通眼镜，并且闭着眼";
                break;
            case 4:
                reuslt = "戴着墨镜";
                break;
            case 5:
                reuslt = "眼镜被遮挡";
                break;
        }
        return reuslt;
    }

    private String getMouthStatus(int mouthStatus) {
        String result = "";
        switch (mouthStatus) {
            case 0:
                result = "带着面具或者带着口罩";
                break;
            case 1:
                result = "被其他东西遮挡着嘴巴";
                break;
            case 2:
                result = "闭嘴状态";
                break;
            case 3:
                result = "张嘴状态";
                break;
        }
        return result;
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }
    }
}
