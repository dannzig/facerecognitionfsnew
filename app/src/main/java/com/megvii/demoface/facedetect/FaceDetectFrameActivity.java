package com.megvii.demoface.facedetect;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Trace;
import android.text.SpannableString;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.Global;
import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.dbhelper;
import com.megvii.demoface.models.CompressionAlgorithm;
import com.megvii.demoface.models.faceModel;
import com.megvii.demoface.opengl.CameraMatrix;
import com.megvii.demoface.opengl.ICameraMatrix;
import com.megvii.demoface.opengl.PointsMatrix;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.CameraGlSurfaceView;
import com.megvii.demoface.view.FaceFramePointView;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import butterknife.BindView;
import butterknife.OnClick;
import mcv.facepass.FacePassException;
import mcv.facepass.FacePassHandler;
import mcv.facepass.types.FacePassAddFaceResult;
import mcv.facepass.types.FacePassConfig;
import mcv.facepass.types.FacePassDetectionResult;
import mcv.facepass.types.FacePassImage;
import mcv.facepass.types.FacePassImageType;
import mcv.facepass.types.FacePassModel;
import mcv.facepass.types.FacePassPose;

import static com.megvii.facepp.multi.sdk.FaceDetectApi.FaceppConfig.DETECTION_MODE_TRACKING;

public class FaceDetectFrameActivity extends BaseActivity implements GLSurfaceView.Renderer, CameraWrapper.CameraOpenCallback, SurfaceTexture.OnFrameAvailableListener, CameraWrapper.ICameraCallback {
    @BindView(R.id.view_face_point)
    FaceFramePointView viewFacePoint;
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_flip_camera)
    ImageView ivFlipCamera;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.rb_facepp_landmark_84)
    RadioButton rbFaceppLandmark84;
    @BindView(R.id.rb_facepp_landmark_106)
    RadioButton rbFaceppLandmark106;
    @BindView(R.id.opengl_layout_surfaceview)
    CameraGlSurfaceView mGlSurfaceView;

    private CameraManager mCameraManager;
    private Handler mHandler;
    RecognizeThread mRecognizeThread;
    FeedFrameThread mFeedFrameThread;
    FacePassHandler mFacePassHandler;
    ArrayBlockingQueue<byte[]> mFeedFrameQueue;
    ArrayBlockingQueue<FaceDetectApi.Face> mRecognizeDataQueue;
    private HandlerThread mHandlerThread = new HandlerThread("facepp");
    private DecimalFormat df;
    //private String type = "face";
    private String type = "faceplus";
    boolean busysearching = false;
    String TAG = "FACE DETECT RES";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_face_frame;
    }

    @Override
    protected void initView() {
//        myCameraTextureview.setSurfaceTextureListener(this);
        tvDetectTips.setMovementMethod(ScrollingMovementMethod.getInstance());
        mGlSurfaceView.setEGLContextClientVersion(2);// 创建一个OpenGL ES 2.0
        mGlSurfaceView.setRenderer(this);

        mGlSurfaceView.setRenderMode(mGlSurfaceView.RENDERMODE_WHEN_DIRTY);
    }


    @Override
    protected void initData() {
        df = new DecimalFormat("0.00");
        type = getIntent().getStringExtra("type");

        type = "faceplus";

        if ("face".equals(type)) {
            tvTitleBar.setText("Face Detection");
        } else if ("faceplus".equals(type)) {
            tvTitleBar.setText("Face detection advanced version");
        }
        FaceDetectApi.FaceppConfig config = FaceDetectApi.getInstance().getFaceppConfig();
        config.face_confidence_filter = 0.6f;
        config.detectionMode = DETECTION_MODE_TRACKING;
        config.is_bind_big_cpu = true;
        int minface = 40; // 640*480;
        float scale = (CameraFactory.mWidth * CameraFactory.mHeight) * 1.0f / (640 * 480);
        config.minFaceSize = (int) (scale * minface);
        FaceDetectApi.getInstance().setFaceppConfig(config);
        mCameraManager = new CameraManager(this);
        mCameraManager.setmCameraOpenCallback(this);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
        mFeedFrameQueue = new ArrayBlockingQueue<byte[]>(1);
        mRecognizeDataQueue = new ArrayBlockingQueue<FaceDetectApi.Face>(5);

        Log.d(TAG, FacePassHandler.getVersion());
        //initFaceHandler();

        Log.e(TAG,"STARTING THE THREADS");
        mFeedFrameThread = new FeedFrameThread();
        mFeedFrameThread.start();

        mRecognizeThread = new RecognizeThread();
        mRecognizeThread.start();



    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraManager.openCamera();
        viewFacePoint.setVisibility(View.VISIBLE);

    }


    @Override
    protected void onPause() {
        super.onPause();
        mCameraManager.closeCamera();
        viewFacePoint.setResult(null, false);
        viewFacePoint.setVisibility(View.GONE);
    }

    private void adjustTextureViewSize() {
        float scale = Math.max(Screen.mHeight * 1.0f / CameraFactory.mWidth, Screen.mWidth * 1.0f / CameraFactory.mHeight);
        int layout_width = (int) (CameraFactory.mHeight * scale);
        int layout_height = (int) (CameraFactory.mWidth * scale);

        mGlSurfaceView.refreshView(layout_width, layout_height);
        viewFacePoint.refreshView(layout_width, layout_height);

        RelativeLayout.LayoutParams surfaceParams = (RelativeLayout.LayoutParams) mGlSurfaceView.getLayoutParams();
        int topMargin = Math.min((Screen.mHeight - layout_height) / 2, 0);
        int leftMargin = Math.min((Screen.mWidth - layout_width) / 2, 0);
        surfaceParams.topMargin = topMargin;
        surfaceParams.leftMargin = leftMargin;
        mGlSurfaceView.setLayoutParams(surfaceParams);
        viewFacePoint.setLayoutParams(surfaceParams);
    }

    private FacePPImage.Builder imageBuilder;
    private boolean isShowLandmark = true;

    @Override
    public void onPreviewFrame(final byte[] bytes, Camera camera) {
        Thread.currentThread().setName("onPreviewFrame");

        Log.e(TAG,"thread id "+Thread.currentThread().getId());
        int width = CameraFactory.mWidth;
        int height = CameraFactory.mHeight;
        startTime = System.currentTimeMillis();

        mFeedFrameQueue.offer(bytes);

        long trackTime = System.currentTimeMillis() - startTime;
        final StringBuilder sb = new StringBuilder();
        /*final FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(imageBuilder.setData(bytes).build());

        if (faces != null) {
            if(!busysearching) {
                busysearching = true;
                mFeedFrameQueue.offer(bytes);
            }
            Log.v("CAMPARER","***************** start of process *********** "+faces.length);



            Log.v("CAMPARER","==============*end process of process ===== "+faces.length);


            ArrayList<ArrayList> pointsOpengl = new ArrayList<ArrayList>();
            for (int c = 0; c < faces.length; c++) {
                startTime = System.currentTimeMillis();
                FaceDetectApi.getInstance().getLandmark(faces[c], rbFaceppLandmark84.isChecked() ? FaceDetectApi.LMK_84 : FaceDetectApi.LMK_106, true);
                FaceDetectApi.getInstance().getRect(faces[c], true);
                trackTime = trackTime + (System.currentTimeMillis()-startTime);



                if (isShowLandmark) //Whether to draw face points
                {
                    //Before 0.4.7 (including) jni calculates the points of all angles to the vertical coordinates, so the points drawn outside need to be adjusted back to fit other angles
                    //At present, getLandmarkOrigin will obtain the original coordinates, so you only need to adapt to other angles for horizontal screen, because the angle relationship between texture and preview is fixed.
                    ArrayList<FloatBuffer> triangleVBList = new ArrayList<FloatBuffer>();

                    for (int i = 0; i < faces[c].points.length; i++) {
                        float x = (faces[c].points[i].x / width) * 2 - 1;
                        if (!mCameraManager.isFrontCam())
                            x = -x;
                        float y = (faces[c].points[i].y / height) * 2 - 1;
                        float[] pointf = new float[]{y, x, 0.0f};

                        FloatBuffer fb = mCameraMatrix.floatBufferUtil(pointf);
                        triangleVBList.add(fb);

                    }
                    pointsOpengl.add(triangleVBList);
                }

                if ("faceplus".equals(type)) {
                    startTime = System.currentTimeMillis();
                    FaceDetectApi.getInstance().getAttribute(faces[c]);//获取所有属性
                    trackTime = trackTime + (System.currentTimeMillis()-startTime);
                    sb.append("· ").append("gender:").append(faces[c].female > faces[c].male ? "女" : "男").append("\n");
                    sb.append("· ").append("age：").append(faces[c].age).append("\n");
                    sb.append("· ").append("ambiguity：").append(df.format(faces[c].blurness)).append("\n");
                    sb.append("· ").append("Confidence：").append(df.format(faces[c].confidence)).append("\n");
                    String expression = "";
                    switch (faces[c].expressionType) {
                        case MG_EXPRESSION_TYPE_ANGRY:
                            expression = "angry";
                            break;
                        case MG_EXPRESSION_TYPE_DISGUST:
                            expression = "disgust";
                            break;
                        case MG_EXPRESSION_TYPE_FEAR:
                            expression = "fear";
                            break;
                        case MG_EXPRESSION_TYPE_HAPPY:
                            expression = "happy";
                            break;
                        case MG_EXPRESSION_TYPE_SAD:
                            expression = "sad";
                            break;
                        case MG_EXPRESSION_TYPE_SURPRISE:
                            expression = "surprise";
                            break;
                        case MG_EXPRESSION_TYPE_NEUTRAL:
                            expression = "neutral";
                            break;
                    }
                    sb.append("· ").append("expression：").append(expression).append("\n");
                    sb.append("· ").append("Sight (left eye)：").append(faces[c].gazeResult.getLeftEyeGaze().toString()).append("\n");
                    sb.append("· ").append("Sight (right eye)：").append(faces[c].gazeResult.getRightEyeGaze().toString()).append("\n");
                    sb.append("· ").append("Eye condition (left eye)：").append(getEyeStatus(faces[c].leftEyeType)).append("\n");
                    sb.append("· ").append("Eye condition (right eye)：").append(getEyeStatus(faces[c].rightEyeType)).append("\n");
                    sb.append("· ").append("Mouth status：").append(getMouthStatus(faces[c].mouthType)).append("\n");
                    sb.append("· ").append("3DPose：{pitch=").append(faces[c].pitch).append(",roll=").append(faces[c].roll).append(",yaw=").append(faces[c].yaw).append("}\n");
                    sb.append("\n");
                }
            }
            synchronized (mPointsMatrix) {
                mPointsMatrix.points = pointsOpengl;
                viewFacePoint.setResult(faces, !mCameraManager.isFrontCam());

            }
        }*/
        mGlSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                mGlSurfaceView.requestRender();
            }
        });
        final long finalTimeConst = trackTime;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                calculateEfficiEency(finalTimeConst, sb);
            }
        });
        Trace.endSection();

    }

    private class FeedFrameThread extends Thread {
        boolean isInterrupt;


        @Override
        public void run() {
            while (!isInterrupt) {

                Log.e(TAG,"Trying to do face matching here on a safe thread");
                long startTime = System.currentTimeMillis();
                Log.v(TAG,"ddddddddd THREAD "+startTime);

                try {
                    Log.e(TAG, "mFeedFrameQueue Queue Size "+mFeedFrameQueue.size());
                    byte[] face_from_cam = mFeedFrameQueue.take();
                    if(face_from_cam !=null){


                        FacePPImage.Builder imageBuilderx = imageBuilder = new FacePPImage.Builder().setWidth(CameraFactory.mWidth).setHeight(CameraFactory.mHeight).setMode(FacePPImage.IMAGE_MODE_NV21).setRotation(faceRotation);
                        final FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(imageBuilderx.setData(face_from_cam).build());

                        if(faces.length > 0) {

                            Log.e(TAG, "mRecognizeDataQueue Queue Size "+mRecognizeDataQueue.size());


                            mRecognizeDataQueue.offer(faces[0]);


                            FaceDetectApi.Face face1 = faces[0];
                            FaceDetectApi.getInstance().getExtractFeature(face1);


                            Log.e("faceTemplateModel","faceTemplatesModel.size() "+Global.faceTemplatesModel.size());


                            dbhelper sd;
                            sd = new dbhelper(cntx,act);


                            Global.faceTemplatMap = sd.getAllTemplate();

                            for (String key : Global.faceTemplatMap.keySet()) {
                                //System.out.println(key + "============= :" + Global.faceTemplatMap.get(key));

                                FaceDetectApi.Face face2 = new FaceDetectApi.Face();

                                face2.feature = CompressionAlgorithm.decompress(Global.faceTemplatMap.get(key));

                                double score = FaceDetectApi.getInstance().faceCompare(face1, face2);
                                Log.d("CAMPARER ", key+"run: Score = "+score);

                                if(score > 92){

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_two);
                                            showToast("ABC",Toast.LENGTH_LONG, true, myBitmap);
                                        }
                                    });

                                }
                            }


                            /*for (File f : list) {

                                String name = f.getName();

                                File imgFile_ = new File(exportDir, name);
                                //
                                if (imgFile_.exists()) {
                                    //Bitmap myBitmap = BitmapFactory.decodeFile(imgFile_.getAbsolutePath());

                                    Bitmap myBitmap = ConUtil.getImage(imgFile_.getAbsolutePath());

                                    //Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_two);
                                    if(myBitmap != null){
                                        Log.e("CAMPARER", "onPreviewFrame: "+imgFile_);
                                        //byte[] imageBgr = ImageTransformUtils.bitmap2BGR(myBitmap);
                                        byte[] imageBgr = ImageTransformUtils.bitmap2RGB(myBitmap);
                                        FacePPImage facePPImagex = new FacePPImage.Builder()
                                                .setData(imageBgr)
                                                .setWidth(myBitmap.getWidth())
                                                .setHeight(myBitmap.getHeight())
                                                .setMode(FacePPImage.IMAGE_MODE_BGR)
                                                .setRotation(FacePPImage.FACE_UP).build();
                                        //startTimes = System.currentTimeMillis();
                                        if(facePPImagex==null){
                                            Log.e("CAMPARER", "facePPImage IS NULL: ");
                                        }
                                        FaceDetectApi.Face[] faces_from_db = FaceDetectApi.getInstance().detectFace(facePPImagex);

                                        Log.e("CAMPARER", "faces_from_db: "+faces_from_db.length);

                                        if(faces_from_db.length > 0) {
                                            FaceDetectApi.Face face2 = faces_from_db[0];
                                            FaceDetectApi.getInstance().getExtractFeature(face2);

                                            if (face2 !=null) {
                                                double score = FaceDetectApi.getInstance().faceCompare(face1, face2);
                                                Log.v("CAMPARER", "Comparison scrore ==== " + score);
                                            }
                                            else{
                                                Log.v("CAMPARER","NO FACE FROM FILE ");
                                            }
                                        }
                                        else{
                                            Log.v("CAMPARER","NO FACE FROM FILE xxxxxxxxxxxxxx");
                                        }
                                    }
                                    else{
                                        Log.v("CAMPARER","Bitmap is dead on arrival");
                                    }
                                }
                                count++;
                            }*/

                        }
                    }
                    //busysearching = false;


                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        @Override
        public void interrupt() {
            isInterrupt = true;
            super.interrupt();
        }
    }

    private class RecognizeThread extends Thread {
        boolean isInterrupt;

        @Override
        public void run() {
            while (!isInterrupt) {


                FaceDetectApi.Face face1 = null;
                try {
                    if(mRecognizeDataQueue !=null && !mRecognizeDataQueue.isEmpty()){
                        face1 = mRecognizeDataQueue.take();
                        FaceDetectApi.getInstance().getExtractFeature(face1);
                        Log.e(TAG,"faceTemplatesModel.size() "+Global.faceTemplatesModel.size());


                        for (String key : Global.faceTemplatMap.keySet()) {
                            //System.out.println(key + "============= :" + Global.faceTemplatMap.get(key));

                            FaceDetectApi.Face face2 = new FaceDetectApi.Face();



                            face2.feature = CompressionAlgorithm.decompress(Global.faceTemplatMap.get(key));

                            double score = FaceDetectApi.getInstance().faceCompare(face1, face2);
                            Log.d(TAG, key+"run: Score = "+score);

                            if(score > 92){

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_two);
                                        showToast(TAG,Toast.LENGTH_LONG, true, myBitmap);
                                    }
                                });


                            }
                        }
                    }


                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }


            }
        }

        @Override
        public void interrupt() {
            isInterrupt = true;
            super.interrupt();
        }
    }

    private String getEyeStatus(int eyeType) {
        String reuslt = "";
        switch (eyeType) {
            case 0:
                reuslt = "Without glasses and with eyes open";
                break;
            case 1:
                reuslt = "No glasses and eyes closed";
                break;
            case 2:
                reuslt = "Wear regular glasses and keep your eyes open";
                break;
            case 3:
                reuslt = "Wear regular glasses and close your eyes";
                break;
            case 4:
                reuslt = "Wearing sunglasses";
                break;
            case 5:
                reuslt = "Glasses are covered";
                break;
        }
        return reuslt;
    }

    private String getMouthStatus(int mouthStatus) {
        String result = "";
        switch (mouthStatus) {
            case 0:
                result = "带着面具或者带着口罩";
                break;
            case 1:
                result = "被其他东西遮挡着嘴巴";
                break;
            case 2:
                result = "闭嘴状态";
                break;
            case 3:
                result = "张嘴状态";
                break;
        }
        return result;
    }

    @Override
    public void onCapturePicture(byte[] bytes) {

    }

    boolean isFinish = true;
    private int faceRotation = FacePPImage.FACE_LEFT;

    @Override
    public void onOpenSuccess() {
        isFliping = false;
        if (mCameraManager.isFrontCam()) {
            faceRotation = FacePPImage.FACE_RIGHT;
        } else {
            faceRotation = FacePPImage.FACE_LEFT;
        }
        imageBuilder = new FacePPImage.Builder().setWidth(CameraFactory.mWidth).setHeight(CameraFactory.mHeight).setMode(FacePPImage.IMAGE_MODE_NV21).setRotation(faceRotation);
        adjustTextureViewSize();
        viewFacePoint.setVisibility(View.VISIBLE);

        mGlSurfaceView.setVisibility(View.GONE);
        mGlSurfaceView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onOpenFailed() {

    }

    private int frame = 0;
    private long startTime = 0;
    private long totalTime = 0;

    private void calculateEfficiEency(long tracktime, StringBuilder tipSb) {
        frame++;
        totalTime = totalTime + tracktime;
        int patchFrame = "faceplus".equals(type) ? 10 : 30;
        if (frame >= patchFrame) {
            tipSb.append("Single frame time (ms):");
            float timeconst =totalTime * 1.0f / frame;
            tipSb.append(String.format("%.2f",timeconst));
            long fps = (long) (1000.0f / timeconst);
            tipSb.append("\n");
            tipSb.append("Frame rate(fps):");
            tipSb.append(fps);
            frame = 0;
            totalTime = 0;
            tvDetectTips.setText(tipSb);
        }
    }

    private boolean isFliping = false; //相机切换中

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips, R.id.iv_flip_camera})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_flip_camera:
                isFliping = true;
                mCameraManager.switchCamera();
                viewFacePoint.setResult(null, false);
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }

    private int mTextureID = -1;
    private SurfaceTexture mSurface;
    private CameraMatrix mCameraMatrix;
    private PointsMatrix mPointsMatrix;

    private final float[] mProjMatrix = new float[16];
    private final float[] mVMatrix = new float[16];
    private final float[] mMVPMatrix = new float[16];
    private boolean needShow = true;
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // 黑色背景
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        surfaceInit();
    }

    private void surfaceInit() {
        mTextureID = ICameraMatrix.getOESTexture();

        mSurface = new SurfaceTexture(mTextureID);
//        checkCamera();
        // This interface does one thing. When data comes in, it will enter the onFrameAvailable method
        mSurface.setOnFrameAvailableListener(this);// Enter when the camera has data
        mCameraMatrix = new CameraMatrix(mTextureID);
        mPointsMatrix = new PointsMatrix(false);
//        mICamera.startPreview(mSurface);// 设置预览容器
//        mICamera.actionDetect(this);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        mCameraManager.startPreview(mSurface);
        mCameraManager.actionDetect(this);

        // 设置画面的大小
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;
        ratio = 1; // In this way, OpenGL can draw according to the screen frame, not a square.

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        final long actionTime = System.currentTimeMillis();
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);// 清除屏幕和深度缓存
        float[] mtx = new float[16];
        mSurface.getTransformMatrix(mtx);
        mCameraMatrix.draw(mtx);
        // Set the camera position (View matrix)
        Matrix.setLookAtM(mVMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1f, 0f);
        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, mVMatrix, 0);
        mPointsMatrix.draw(mMVPMatrix);

        mSurface.updateTexImage();// To update the image, the onFrameAvailable method will be called.


    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//            mGlSurfaceView.requestRender();
       // Log.e("IMAGE RECEIVED","IMAGE RECEIVED ==== ");

        //mFeedFrameQueue.offer(cameraPreviewData);
    }












    private Toast mRecoToast;
    public void showToast(CharSequence text, int duration, boolean isSuccess, Bitmap bitmap) {
        LayoutInflater inflater = getLayoutInflater();
        View toastView = inflater.inflate(R.layout.toast, null);
        LinearLayout toastLLayout = (LinearLayout) toastView.findViewById(R.id.toastll);
        if (toastLLayout == null) {
            return;
        }
       // toastLLayout.getBackground().setAlpha(100);
        ImageView imageView = (ImageView) toastView.findViewById(R.id.toastImageView);
        TextView idTextView = (TextView) toastView.findViewById(R.id.toastTextView);
        TextView stateView = (TextView) toastView.findViewById(R.id.toastState);
        SpannableString s;
        if (isSuccess) {
            s = new SpannableString("验证成功");
            imageView.setImageResource(R.drawable.success);
        } else {
            s = new SpannableString("验证失败");
            imageView.setImageResource(R.drawable.success);
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        stateView.setText(s);
        idTextView.setText(text);

        if (mRecoToast == null) {
            mRecoToast = new Toast(getApplicationContext());
            mRecoToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        }
        mRecoToast.setDuration(duration);
        mRecoToast.setView(toastView);

        mRecoToast.show();
    }





}
