package com.megvii.demoface.facecompare;

import static com.megvii.demoface.Global.faceString;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.Global;
import com.megvii.demoface.R;
import com.megvii.demoface.models.CompressionAlgorithm;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;


import butterknife.BindView;
import butterknife.OnClick;

public class FaceCompareActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.iv_compera_face_one)
    ImageView ivComperaFaceOne;
    @BindView(R.id.bt_upload_image_one)
    Button btUploadImageOne;
    @BindView(R.id.iv_compera_face_two)
    ImageView ivComperaFaceTwo;
    @BindView(R.id.bt_upload_image_two)
    Button btUploadImageTwo;
    @BindView(R.id.ll_compare_image)
    LinearLayout llCompareImage;
    @BindView(R.id.tv_comapare_score)
    TextView tvComapareScore;
    @BindView(R.id.ll_compare_success)
    LinearLayout llCompareSuccess;
    @BindView(R.id.tv_image_detect_result)
    TextView tvImageDetectResult;
    @BindView(R.id.ll_compare_failure)
    LinearLayout llCompareFailure;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;

    private Bitmap bitmap1;
    private Bitmap bitmap2;

    private int imageOneResult = -1; // 1 ok 2 无人脸 3 多人脸
    private int imageTwoResult = -1;
    FaceDetectApi.Face face1 = null;
    FaceDetectApi.Face face2 = null;
    private DecimalFormat df;
    public static final int REQ_GALLERY_CODE = 101;
    public static final int GALLERY_CODE = 101;
    private int currentDetectIndex = -1; // 当前要处理的图片
    private long startTimes = 0;
    private long detectCostTimes1 = 0;
    private long detectCostTimes2 = 0;
    private long getFeatureCostTimes1 = 0;
    private long getFeatureCostTimes2 = 0;
    private long compareCostTimes = 0;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_compare_image;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("face comparison");
    }

    @Override
    protected void initData() {
        df = new DecimalFormat("0.00");
        bitmap1 = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_one);
        bitmap2 = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_two);
        ivComperaFaceOne.setImageBitmap(bitmap1);
        ivComperaFaceTwo.setImageBitmap(bitmap2);
        doDetect(bitmap1, 0);
        doDetect(bitmap2, 1);
    }

    private void doDetect(Bitmap bitmap, int index) {
        if (index == 0) {
            face1 = null;
        } else {
            face2 = null;
        }
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        startTimes = System.currentTimeMillis();
        FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(facePPImage);
        Log.v("FACES IN PHOTO","FACES IN PHOTO = "+faces.length);
        if (faces.length == 0) {
            if (index == 0) {
                imageOneResult = 2;
            } else {
                imageTwoResult = 2;
            }
            updateFailureInfo();
        } else if (faces.length > 1) {
            if (index == 0) {
                imageOneResult = 3;
            } else {
                imageTwoResult = 3;
            }
            updateFailureInfo();
        } else {
            if (index == 0) {
                detectCostTimes1 = System.currentTimeMillis() - startTimes;
                face1 = faces[0];
                startTimes = System.currentTimeMillis();
                FaceDetectApi.getInstance().getExtractFeature(face1);
                getFeatureCostTimes1 = System.currentTimeMillis() - startTimes;
                imageOneResult = 1;
            } else {
                detectCostTimes2 = System.currentTimeMillis() - startTimes;
                face2 = faces[0];
                startTimes = System.currentTimeMillis();
                FaceDetectApi.getInstance().getExtractFeature(face2);
                getFeatureCostTimes2 = System.currentTimeMillis() - startTimes;
                imageTwoResult = 1;
            }
            doCompare();
        }
    }

    public void showImages() throws IOException {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("theImages", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        File exportDir = new File(Environment.getExternalStorageDirectory(), "capturephotos");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        File[] list = exportDir.listFiles();
        int count = 0;

        for (File f : list) {

            //String name = f.getName();

            File imgFile_ = new File(exportDir, f.getName());
            //
            if (imgFile_.exists()) {
                //Bitmap myBitmap = BitmapFactory.decodeFile(imgFile_.getAbsolutePath());

                Bitmap myBitmap = ConUtil.getImage(imgFile_.getAbsolutePath());

                //Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.iv_compare_image_two);
                if(myBitmap != null){
                    Log.e("CAMPARER", count+" onPreviewFrame: "+imgFile_);
                    //byte[] imageBgr = ImageTransformUtils.bitmap2BGR(myBitmap);
                    byte[] imageBgr = ImageTransformUtils.bitmap2RGB(myBitmap);
                    FacePPImage facePPImagex = new FacePPImage.Builder()
                            .setData(imageBgr)
                            .setWidth(myBitmap.getWidth())
                            .setHeight(myBitmap.getHeight())
                            .setMode(FacePPImage.IMAGE_MODE_BGR)
                            .setRotation(FacePPImage.FACE_UP).build();
                    //startTimes = System.currentTimeMillis();
                    if(facePPImagex==null){
                        Log.e("CAMPARER", "facePPImage IS NULL: ");
                    }
                    FaceDetectApi.Face[] faces_from_db = FaceDetectApi.getInstance().detectFace(facePPImagex);

                    Log.e("CAMPARER", "faces_from_db: "+faces_from_db.length);

                    if(faces_from_db.length > 0) {
                        FaceDetectApi.Face face2 = faces_from_db[0];
                        FaceDetectApi.getInstance().getExtractFeature(face2);

                        byte[] template = face2.feature;
                       /* faceModel fm = new faceModel();
                        fm.setId(count+"");
                        fm.setName(f.getName());
                        fm.setTemplate(template);
                        Log.v("CAMPARER",fm.getName()+"We have faces ohh "+Global.faceTemplatesModel.size());*/
                        //Log.e("CAMPARER", "showImages: Bytre = "+new String(template));


                        byte[] plain = template;
                        System.out.println("B4= "+plain.length); // 33


                        byte[] compressed = CompressionAlgorithm.compress(plain);
                        System.out.println("compressed= "+compressed.length); // 33


                        Global.faceTemplatMap.put(count+"-"+f.getName(), compressed);


                    }
                    else{
                        Log.v("CAMPARER","NO FACE FROM FILE xxxxxxxxxxxxxx");
                    }
                }
                else{
                    Log.v("CAMPARER","Bitmap is dead on arrival");
                }
            }
            count++;
        }



        /*for (String key : Global.faceTemplatMap.keySet()) {
            System.out.println(key + "============= :" + Global.faceTemplatMap.get(key));
        }*/

    }

    private void updateFailureInfo() {
        StringBuilder sb = new StringBuilder();
        if (imageOneResult == 2) {
            sb.append("No face detected in image 1");
        } else if (imageOneResult == 3) {
            sb.append("Multiple faces detected in picture 1");
        }

        if (imageTwoResult == 2) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append("No face detected in picture 2");
        } else if (imageTwoResult == 3) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append("Multiple faces detected in picture 2");
        }

        llCompareSuccess.setVisibility(View.GONE);
        tvImageDetectResult.setText(sb.toString());
        llCompareFailure.setVisibility(View.VISIBLE);
    }

    private void doCompare() {
        if (face1 != null && face2 != null) {
            startTimes = System.currentTimeMillis();
            double score = FaceDetectApi.getInstance().faceCompare(face1, face2);
            Log.v("SCRORE CARD","SCORE REGOGNITION = "+score);
            compareCostTimes = System.currentTimeMillis() - startTimes;
            llCompareFailure.setVisibility(View.GONE);
            llCompareSuccess.setVisibility(View.VISIBLE);
            tvComapareScore.setText(df.format(score));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Picture 1 detection time (ms)：").append(detectCostTimes1).append("\n");
            stringBuilder.append("Picture 1 feature extraction time (ms)：").append(getFeatureCostTimes1).append("\n");
            stringBuilder.append("Image 2 detection time (ms)：").append(detectCostTimes2).append("\n");
            stringBuilder.append("Picture 2 feature extraction time (ms)：").append(getFeatureCostTimes2).append("\n");
            stringBuilder.append("Total comparison time (ms)：").append(compareCostTimes + detectCostTimes1 + detectCostTimes2 + getFeatureCostTimes1 + getFeatureCostTimes2);
            tvDetectTips.setText(stringBuilder.toString());
        } else {
            updateFailureInfo();
        }
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips, R.id.bt_upload_image_one, R.id.bt_upload_image_two})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_upload_image_one:
                currentDetectIndex = 0;
                try {
                    showImages();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                requestGalleryPerm();
                break;
            case R.id.bt_upload_image_two:
                currentDetectIndex = 1;
                requestGalleryPerm();
                break;
        }
    }

    private void requestGalleryPerm() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //进行权限请求
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_CODE);
        } else {
            openGalleryActivity();
        }

    }

    private void openGalleryActivity() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQ_GALLERY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_GALLERY_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    String path = ConUtil.getRealPathFromURI(this, uri);
                    Log.e("PATH","PATH FULL "+path);
                    Bitmap bitmap = ConUtil.getImage(path);
                    if (currentDetectIndex == 0) {
                        ivComperaFaceOne.setImageBitmap(bitmap);
                    } else {
                        ivComperaFaceTwo.setImageBitmap(bitmap);
                    }
                    doDetect(bitmap, currentDetectIndex);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == GALLERY_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {// Permission Granted
                showSettingDialog("read memory card");
            } else {
                openGalleryActivity();
            }
        }
    }

    public void showSettingDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("无" + msg + "Permissions, go to the settings to open");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ConUtil.getAppDetailSettingIntent(FaceCompareActivity.this);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }



}
