package com.megvii.demoface.segment;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.FileUtils;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.utils.SegResultHandleUtils;
import com.megvii.demoface.view.SegmentOptionView;
import com.megvii.facepp.multi.sdk.BodySegmentApi;
import com.megvii.facepp.multi.sdk.FacePPImage;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

public class SegmentImageActivity extends BaseActivity implements View.OnTouchListener {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_seg_image)
    ImageView ivSegImage;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.seg_option_blackwhite)
    SegmentOptionView segOptionBlackwhite;
    @BindView(R.id.seg_option_transparent)
    SegmentOptionView segOptionTransparent;
    @BindView(R.id.seg_option_bgmixture1)
    SegmentOptionView segOptionBgmixture1;
    @BindView(R.id.seg_option_bgmixture2)
    SegmentOptionView segOptionBgmixture2;
    @BindView(R.id.seg_option_bgmixture3)
    SegmentOptionView segOptionBgmixture3;
    @BindView(R.id.ll_segment_option)
    LinearLayout llSegmentOption;
    @BindView(R.id.iv_save_image)
    ImageView ivSaveImage;

    private Bitmap originalBitmap;
    private Bitmap newBitmap;
    private float[] result;
    private Bitmap mixtureBitmap1;
    private Bitmap mixtureBitmap2;
    private Bitmap mixtureBitmap3;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_segment_image;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("人体抠像");
        ivSegImage.setOnTouchListener(this);
    }

    @Override
    protected void initData() {
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String path = ConUtil.getRealPathFromURI(this, uri);
        originalBitmap = ConUtil.getImage(path);
        int imageWidth = originalBitmap.getWidth();
        int imageHeight = originalBitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivSegImage.getLayoutParams();
        layoutParams.width = (int) (scale * imageWidth);
        layoutParams.height = (int) (scale * imageHeight);
        ivSegImage.setLayoutParams(layoutParams);
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(originalBitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(originalBitmap.getWidth())
                .setHeight(originalBitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        long startTime = System.currentTimeMillis();
        result = BodySegmentApi.getInstance().bodySegment(facePPImage);
        long timeConst = System.currentTimeMillis() - startTime;
        if (result != null) {
            newBitmap = SegResultHandleUtils.setBlackWhite(originalBitmap, result);
            ivSegImage.setImageBitmap(newBitmap);
        }
        tvDetectTips.setText("单帧耗时(ms):" + timeConst);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (originalBitmap != null) {
                ivSegImage.setImageBitmap(originalBitmap);
                return true;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (newBitmap != null) {
                ivSegImage.setImageBitmap(newBitmap);
                return true;
            }
        }
        return false;
    }

    @OnClick(R.id.ll_go_back)
    public void onLlGoBackClicked() {
        finish();
    }

    @OnClick(R.id.iv_show_tips)
    public void onLlDetectTipsClicked() {
        if (llDetectTips.getVisibility() == View.VISIBLE)
            llDetectTips.setVisibility(View.GONE);
        else
            llDetectTips.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.seg_option_blackwhite)
    public void onSegOptionBlackwhiteClicked() {
        if (!segOptionBlackwhite.isChecked()) {
            segOptionBlackwhite.setChecked(true);
            segOptionTransparent.setChecked(false);
            segOptionBgmixture1.setChecked(false);
            segOptionBgmixture2.setChecked(false);
            segOptionBgmixture3.setChecked(false);
            if (result != null && originalBitmap != null) {
                newBitmap = SegResultHandleUtils.setBlackWhite(originalBitmap, result);
                ivSegImage.setImageBitmap(newBitmap);
            }
        }
    }

    @OnClick(R.id.seg_option_transparent)
    public void onSegOptionTransparentClicked() {
        if (!segOptionTransparent.isChecked()) {
            segOptionBlackwhite.setChecked(false);
            segOptionTransparent.setChecked(true);
            segOptionBgmixture1.setChecked(false);
            segOptionBgmixture2.setChecked(false);
            segOptionBgmixture3.setChecked(false);
            if (result != null && originalBitmap != null) {
                newBitmap = SegResultHandleUtils.setBitmapAlpha(originalBitmap, result);
                ivSegImage.setImageBitmap(newBitmap);
            }
        }
    }

    @OnClick(R.id.seg_option_bgmixture1)
    public void onSegOptionBgmixture1Clicked() {
        if (!segOptionBgmixture1.isChecked()) {
            segOptionBlackwhite.setChecked(false);
            segOptionTransparent.setChecked(false);
            segOptionBgmixture1.setChecked(true);
            segOptionBgmixture2.setChecked(false);
            segOptionBgmixture3.setChecked(false);
            if (result != null && originalBitmap != null) {
                if (mixtureBitmap1 == null) {
                    mixtureBitmap1 = ConUtil.getImage(this, R.mipmap.iv_seg_mixture_bg1, originalBitmap.getWidth(), originalBitmap.getHeight());
                }
                newBitmap = SegResultHandleUtils.backGroudBlend(originalBitmap, mixtureBitmap1, result);
                ivSegImage.setImageBitmap(newBitmap);
            }
        }
    }

    @OnClick(R.id.seg_option_bgmixture2)
    public void onSegOptionBgmixture2Clicked() {
        if (!segOptionBgmixture2.isChecked()) {
            segOptionBlackwhite.setChecked(false);
            segOptionTransparent.setChecked(false);
            segOptionBgmixture1.setChecked(false);
            segOptionBgmixture2.setChecked(true);
            segOptionBgmixture3.setChecked(false);
            if (result != null && originalBitmap != null) {
                if (mixtureBitmap2 == null) {
                    mixtureBitmap2 = ConUtil.getImage(this, R.mipmap.iv_seg_mixture_bg2, originalBitmap.getWidth(), originalBitmap.getHeight());
                }
                newBitmap = SegResultHandleUtils.backGroudBlend(originalBitmap, mixtureBitmap2, result);
                ivSegImage.setImageBitmap(newBitmap);
            }
        }
    }

    @OnClick(R.id.seg_option_bgmixture3)
    public void onSegOptionBgmixture3Clicked() {
        if (!segOptionBgmixture3.isChecked()) {
            segOptionBlackwhite.setChecked(false);
            segOptionTransparent.setChecked(false);
            segOptionBgmixture1.setChecked(false);
            segOptionBgmixture2.setChecked(false);
            segOptionBgmixture3.setChecked(true);
            if (result != null && originalBitmap != null) {
                if (mixtureBitmap3 == null) {
                    mixtureBitmap3 = ConUtil.getImage(this, R.mipmap.iv_seg_mixture_bg3, originalBitmap.getWidth(), originalBitmap.getHeight());
                }
                newBitmap = SegResultHandleUtils.backGroudBlend(originalBitmap, mixtureBitmap3, result);
                ivSegImage.setImageBitmap(newBitmap);
            }
        }
    }


    @OnClick(R.id.iv_save_image)
    public void onViewClicked() {
        ivSegImage.setDrawingCacheEnabled(true);
        Bitmap bitmap = ivSegImage.getDrawingCache();
        String path = FileUtils.saveBitmap(this,bitmap,"segment");
        ivSegImage.setDrawingCacheEnabled(false);
        ConUtil.updateAlbum(this,new File(path));
        Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();
    }
}
