package com.megvii.demoface.segment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.SettingActivity;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.utils.SensorEventUtil;
import com.megvii.demoface.view.SegmentOptionView;
import com.megvii.facepp.multi.sdk.BodySegmentApi;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SegmentFrameActivity extends BaseActivity implements CameraWrapper.CameraOpenCallback {
    @BindView(R.id.my_camera_glsurfaceview)
    public SegmentGlSurfaceView myCameraGlsurfaceview;
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_flip_camera)
    ImageView ivFlipCamera;
    @BindView(R.id.iv_goto_setting)
    ImageView ivGotoSetting;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.seg_option_blackwhite)
    SegmentOptionView segOptionBlackwhite;
    @BindView(R.id.seg_option_transparent)
    SegmentOptionView segOptionTransparent;
    @BindView(R.id.seg_option_bgmixture1)
    SegmentOptionView segOptionBgmixture1;
    @BindView(R.id.seg_option_bgmixture2)
    SegmentOptionView segOptionBgmixture2;
    @BindView(R.id.seg_option_bgmixture3)
    SegmentOptionView segOptionBgmixture3;

    private CameraManager mCameraManager;
    private SensorEventUtil sensorUtil;

    private int resolutionType = 1; // 1 2 3 低 中 高

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_segment_frame;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("人体抠像");
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);
        mCameraManager = new CameraManager(this);
        mCameraManager.setmCameraOpenCallback(this);
        sensorUtil = new SensorEventUtil(this);
        myCameraGlsurfaceview.setCameraManager(mCameraManager, sensorUtil);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (resolutionType == 1) {
//            CameraFactory.mWidth = 640;
//            CameraFactory.mHeight = 480;
//        } else if (resolutionType == 2) {
//            CameraFactory.mWidth = 1280;
//            CameraFactory.mHeight = 720;
//        } else if (resolutionType == 3) {
//            CameraFactory.mWidth = 1920;
//            CameraFactory.mHeight = 1080;
//        }
        mCameraManager.openCamera();
        myCameraGlsurfaceview.onResume();
        myCameraGlsurfaceview.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraManager.closeCamera();
        myCameraGlsurfaceview.onPause();
//        myCameraGlsurfaceview.setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        resolutionType = resultCode;
        if (resolutionType == 1) {
            mCameraManager.switchCamera(640, 480);
        } else if (resolutionType == 2) {
            mCameraManager.switchCamera(1280, 720);
        } else if (resolutionType == 3) {
            mCameraManager.switchCamera(1920, 1080);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItermEvent(ItemEvent event) {
        tvDetectTips.setText(event.msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ll_go_back, R.id.iv_flip_camera, R.id.iv_goto_setting, R.id.iv_show_tips, R.id.seg_option_blackwhite, R.id.seg_option_transparent, R.id.seg_option_bgmixture1, R.id.seg_option_bgmixture2, R.id.seg_option_bgmixture3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_flip_camera:
                BodySegmentApi.getInstance().reset();
                myCameraGlsurfaceview.getCameraRender().mCameraChange = true;
                mCameraManager.switchCamera();
                myCameraGlsurfaceview.setVisibility(View.GONE);
                myCameraGlsurfaceview.setVisibility(View.VISIBLE);
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
            case R.id.iv_goto_setting:
                Intent setIntent = new Intent(this, SettingActivity.class);
                setIntent.putExtra("resolutionType", resolutionType);
                startActivityForResult(setIntent, 100);
                break;
            case R.id.seg_option_blackwhite:
                if (!segOptionBlackwhite.isChecked()) {
                    segOptionBlackwhite.setChecked(true);
                    segOptionTransparent.setChecked(false);
                    segOptionBgmixture1.setChecked(false);
                    segOptionBgmixture2.setChecked(false);
                    segOptionBgmixture3.setChecked(false);
                    myCameraGlsurfaceview.getCameraRender().changeSegmentType(1);
                }
                break;
            case R.id.seg_option_transparent:
                if (!segOptionTransparent.isChecked()) {
                    segOptionBlackwhite.setChecked(false);
                    segOptionTransparent.setChecked(true);
                    segOptionBgmixture1.setChecked(false);
                    segOptionBgmixture2.setChecked(false);
                    segOptionBgmixture3.setChecked(false);
                    myCameraGlsurfaceview.getCameraRender().changeSegmentType(2);

                }
                break;
            case R.id.seg_option_bgmixture1:
                if (!segOptionBgmixture1.isChecked()) {
                    segOptionBlackwhite.setChecked(false);
                    segOptionTransparent.setChecked(false);
                    segOptionBgmixture1.setChecked(true);
                    segOptionBgmixture2.setChecked(false);
                    segOptionBgmixture3.setChecked(false);
                    myCameraGlsurfaceview.getCameraRender().changeSegmentType(3);

                }
                break;
            case R.id.seg_option_bgmixture2:
                if (!segOptionBgmixture2.isChecked()) {
                    segOptionBlackwhite.setChecked(false);
                    segOptionTransparent.setChecked(false);
                    segOptionBgmixture1.setChecked(false);
                    segOptionBgmixture2.setChecked(true);
                    segOptionBgmixture3.setChecked(false);
                    myCameraGlsurfaceview.getCameraRender().changeSegmentType(4);

                }
                break;
            case R.id.seg_option_bgmixture3:
                if (!segOptionBgmixture3.isChecked()) {
                    segOptionBlackwhite.setChecked(false);
                    segOptionTransparent.setChecked(false);
                    segOptionBgmixture1.setChecked(false);
                    segOptionBgmixture2.setChecked(false);
                    segOptionBgmixture3.setChecked(true);
                    myCameraGlsurfaceview.getCameraRender().changeSegmentType(5);
                }
                break;
        }
    }

    @Override
    public void onOpenSuccess() {
        myCameraGlsurfaceview.setVisibility(View.VISIBLE);

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onOpenFailed() {

    }
}
