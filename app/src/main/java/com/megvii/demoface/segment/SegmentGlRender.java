package com.megvii.demoface.segment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.opengl.ICameraMatrix;
import com.megvii.demoface.opengl.ImageMixMatrix;
import com.megvii.demoface.opengl.NV21Matrix;
import com.megvii.demoface.opengl.OpenGLUtils;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.SegResultHandleUtils;
import com.megvii.demoface.utils.SensorEventUtil;
import com.megvii.facepp.multi.sdk.BodySegmentApi;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.segment.SegmentResult;

import org.greenrobot.eventbus.EventBus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class SegmentGlRender implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener, CameraWrapper.ICameraCallback {
    private Context mContext;
    private int mWidth = 0, mHeight = 0;
    private SurfaceTexture mSurfaceTexture;
    private CameraManager mCameraManager;
    private ICameraMatrix mCameraMatrix;
    //    private com.megvii.beautify.cameragl.ImageMatrix mImageMatrix;
    private ImageMixMatrix mImageMatrix;
    private SensorEventUtil sensorUtil;
    private final FloatBuffer mVertexBuffer;
    private final FloatBuffer mTextureBuffer;
    private SegmentGlSurfaceView.RequestRenderListener requestRenderListener;
    private NV21Matrix nv21Matrix = new NV21Matrix();
    float[] textureCords;
    private int mFrontTexture = -1;
    public int mRenderW = 1920;
    public int mRenderH = 1080;
    private FacePPImage.Builder imageBuilder;
    private int faceRotation = FacePPImage.FACE_LEFT;
    private HandlerThread segThread = new HandlerThread("segment");
    private Handler segHandler;
    public boolean mCameraChange = false;
    private int bgTextureId = -1; //黑底
    private int fgTextureId = -1; //白像

    private int backImageTextureId = -1;

    private float scale = 0.0f;

    private int segmentType = 1; // 黑底白像  2 背景融合

    public SegmentGlRender(Context context, CameraManager cameraManager, SensorEventUtil sensorUtil) {
        mContext = context;
        mCameraManager = cameraManager;
        mCameraMatrix = new ICameraMatrix(context);
        mImageMatrix = new ImageMixMatrix(context);
        mImageMatrix.initCameraFrameBuffer(mRenderW, mRenderH);

        this.sensorUtil = sensorUtil;
        mVertexBuffer = ByteBuffer.allocateDirect(OpenGLUtils.CUBE.length * 4).order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mVertexBuffer.put(OpenGLUtils.CUBE).position(0);

        mTextureBuffer = ByteBuffer.allocateDirect(OpenGLUtils.TEXTURE_NO_ROTATION.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextureBuffer.put(OpenGLUtils.TEXTURE_NO_ROTATION).position(0);

        segThread.start();
        segHandler = new Handler(segThread.getLooper());

//        byte[] white = {(byte) 0xff, (byte) 0xff, (byte) 0xff, 0};
//        ByteBuffer whiteBuffer = ByteBuffer.wrap(white);
//        bgTextureId = ICameraMatrix.get2DTextureID();
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, bgTextureId);
//        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
//                1, 1, 0,
//                GLES20.GL_RGBA,
//                GLES20.GL_UNSIGNED_BYTE, whiteBuffer);
//
//        byte[] black = {(byte) 0, (byte) 0, (byte) 0, 0};
//        ByteBuffer blackBuffer = ByteBuffer.wrap(black);
//        fgTextureId = ICameraMatrix.get2DTextureID();
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fgTextureId);
//        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
//                1, 1, 0,
//                GLES20.GL_RGBA,
//                GLES20.GL_UNSIGNED_BYTE, blackBuffer);


//        fgTextureId = initTextureID(1, 1)[0];
//        bgTextureId=initTextureID(1,1)[0];
//        bgTextureId = initTextureID3(ConUtil.getImage(context, R.mipmap.iv_seg_mixture_bg1));
    }

    public static int[] initTextureID(int width, int height) {
        byte[] greenB = {(byte) 0xff, (byte) 0xff, (byte) 0xff, 0};
        ByteBuffer greenBuffer = ByteBuffer.wrap(greenB);

        int[] mTextureOutID = new int[1];
        GLES20.glGenTextures(1, mTextureOutID, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureOutID[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0, GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, greenBuffer);
        return mTextureOutID;
    }


    public static int[] initTextureID2(int width, int height) {
        byte[] greenB = {(byte) 0, (byte) 0, (byte) 0, 0};
        ByteBuffer greenBuffer = ByteBuffer.wrap(greenB);

        int[] mTextureOutID = new int[1];
        GLES20.glGenTextures(1, mTextureOutID, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureOutID[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0, GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, greenBuffer);
        return mTextureOutID;
    }

    public int initTextureID3(Bitmap bitmap) {
        byte rgba[] = SegResultHandleUtils.getPixelsRGBA(bitmap);
        ByteBuffer greenBuffer = ByteBuffer.wrap(rgba);
        int[] mTextureOutID = new int[1];
        GLES20.glGenTextures(1, mTextureOutID, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureOutID[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, bitmap.getWidth(), bitmap.getHeight(), 0, GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, greenBuffer);

        float cameraRatio = mCameraManager.cameraHeight * 1.0f / mCameraManager.cameraWidth;
        float bgRatio = bitmap.getWidth() * 1.0f / bitmap.getHeight();
        scale = cameraRatio / bgRatio;
        return mTextureOutID[0];
    }


    public void changeSegmentType(final int segmentType) {
        requestRenderListener.runOnRenderThread(new Runnable() {
            @Override
            public void run() {
                if (segmentType == 3) {
                    backImageTextureId = initTextureID3(ConUtil.getImage(mContext, R.mipmap.iv_seg_mixture_bg1));
                } else if (segmentType == 4) {
                    backImageTextureId = initTextureID3(ConUtil.getImage(mContext, R.mipmap.iv_seg_mixture_bg2));
                } else if (segmentType == 5) {
                    backImageTextureId = initTextureID3(ConUtil.getImage(mContext, R.mipmap.iv_seg_mixture_bg3));
                }
            }
        });


        this.segmentType = segmentType;
    }


    public void setRequestRenderListener(SegmentGlSurfaceView.RequestRenderListener requestRenderListener) {
        this.requestRenderListener = requestRenderListener;
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {

    }


    boolean isBudySegDetecting;
    private float[] segResult;
    private int resultWidth;
    private int resultHeight;

    @Override
    public void onPreviewFrame(final byte[] data, final Camera camera) {
//        dealwithFrame(data, camera);
//        requestRenderListener.startRequestRender();
        if (mCameraManager.isFrontCam()) {
            faceRotation = FacePPImage.FACE_RIGHT;
        }else{
            faceRotation = FacePPImage.FACE_LEFT;

        }
        imageBuilder = new FacePPImage.Builder().setWidth(CameraFactory.mWidth).setHeight(CameraFactory.mHeight).setMode(FacePPImage.IMAGE_MODE_NV21).setRotation(faceRotation);
        if (isBudySegDetecting) {
            return;
        }
        isBudySegDetecting = true;
        segHandler.post(new Runnable() {
            @Override
            public void run() {
                startTime = System.currentTimeMillis();
                SegmentResult segmentResult = BodySegmentApi.getInstance().segmentFrame(imageBuilder.setData(data).build());
                calculateEfficiEency(System.currentTimeMillis() - startTime);
                if (segmentResult != null) {
                    segResult = segmentResult.getSegResult();
                    resultWidth = segmentResult.getImageWidth();
                    resultHeight = segmentResult.getImageHeight();
                    Log.w("segmentFrame", "segResult length = " + segResult.length);
                    dealwithFrame(segmentResult.getOriginImage(), resultWidth, resultHeight, camera);
                } else {
                    dealwithFrame(data, mCameraManager.cameraWidth, mCameraManager.cameraHeight, camera);
                }
                requestRenderListener.startRequestRender();
                Log.w("segmentFrame", "timeconst:" + (System.currentTimeMillis() - startTime));
                isBudySegDetecting = false;
            }
        });
    }

    private int frame = 0;
    private long startTime = 0;
    private long totalTime = 0;

    private void calculateEfficiEency(long detectTime) {
        frame++;
        totalTime = totalTime + detectTime;
        if (frame == 30) {
            StringBuilder tipSb = new StringBuilder();
            tipSb.append("单帧耗时(ms):");
            long timeconst = (long) (totalTime * 1.0f / frame);
            tipSb.append(timeconst);
            long fps = (long) (1000.0f / timeconst);
            tipSb.append("\n");
            tipSb.append("帧率(fps):");
            tipSb.append(fps);
            frame = 0;
            totalTime = 0;
            ItemEvent itemEvent = new ItemEvent();
            itemEvent.msg = tipSb.toString();
            EventBus.getDefault().post(itemEvent);
        }


    }

    @Override
    public void onCapturePicture(byte[] bytes) {

    }

    private void dealwithFrame(final byte[] data, final int width, final int height, final Camera camera) {
        if (requestRenderListener != null) {
            requestRenderListener.runOnRenderThread(new Runnable() {
                @Override
                public void run() {
                    updateTexture(data, width, height);
                }
            });
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
//        fgTextureId = initTextureID(1, 1)[0];

//        bgTextureId = initTextureID3(ConUtil.getImage(mContext, R.mipmap.iv_seg_mixture_bg1));
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.w("Segment", "onSurfaceChanged...");
        Log.w("Segment", "width=" + width + ",height=" + height);
        BodySegmentApi.getInstance().reset();
        if (mWidth == width && mHeight == height && !mCameraChange) {
            return;
        } else {
            onDestroy();
            if (mCameraChange) {
                mCameraChange = false;
            }
        }
        mWidth = width;
        mHeight = height;
        GLES20.glClearColor(0, 0, 0, 1.0f);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        if (mCameraManager.isFrontCam()) {
            textureCords = OpenGLUtils.TEXTURE_ROTATED_FRONT;
        } else {
            textureCords = OpenGLUtils.TEXTURE_ROTATED_BACK;
        }
        fgTextureId = initTextureID(1, 1)[0];
        bgTextureId = initTextureID2(1, 1)[0];
        mTextureBuffer.clear();
        mTextureBuffer.put(textureCords).position(0);

        mFrontTexture = mCameraMatrix.get2DTextureID();
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrontTexture);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                mRenderW, mRenderH, 0,
                GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, null);
        if (mSurfaceTexture != null) {
            mSurfaceTexture.release();
        }
        mSurfaceTexture = new SurfaceTexture(10);
        mCameraManager.actionDetect(this);
        mCameraManager.startPreview(mSurfaceTexture);
        mCameraMatrix.init(mCameraManager.isFrontCam());
        mCameraMatrix.initCameraFrameBuffer(mRenderW, mRenderH);
        mCameraMatrix.onOutputSizeChanged(mRenderW, mRenderH);
        mImageMatrix.init();
        GLES20.glViewport(0, 0, width, height);
        sceenAutoFit(mWidth, mHeight, mCameraManager.cameraWidth, mCameraManager.cameraHeight, mCameraManager.Angle);

    }

    /**
     * 按照centercrop的源码修改，这里viewport相当于已经做过scale了，
     * 所以不需要额外scale，但是dx不需要除以2还不理解，centercrop 是要除的。
     *
     * @param screenW
     * @param screenH
     * @param cameraW
     * @param cameraH
     * @param angle
     */
    public void sceenAutoFit(int screenW, int screenH, int cameraW, int cameraH, int angle) {
        if (angle == 90 || angle == 270) {
            int temp = cameraW;
            cameraW = cameraH;
            cameraH = temp;
        }
        float scale;
        float dx = 0, dy = 0;
        float dxRatio = 0f;
        float dyRatio = 0f;


        if (cameraW * screenH > screenW * cameraH) {
            scale = (float) screenH / (float) cameraH;
            dx = (screenW - cameraW * scale);
            dxRatio = dx / screenW;
        } else {
            scale = (float) screenW / (float) cameraW;
            dy = (screenH - cameraH * scale);
            dyRatio = dy / screenH;
        }
        float[] cube = new float[]{
                OpenGLUtils.CUBE[0] + dxRatio, OpenGLUtils.CUBE[1] + dyRatio,
                OpenGLUtils.CUBE[2] - dxRatio, OpenGLUtils.CUBE[3] + dyRatio,
                OpenGLUtils.CUBE[4] + dxRatio, OpenGLUtils.CUBE[5] - dyRatio,
                OpenGLUtils.CUBE[6] - dxRatio, OpenGLUtils.CUBE[7] - dyRatio};
        mVertexBuffer.clear();
        mVertexBuffer.put(cube).position(0);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        drawTestHALFrame(gl);
    }

    public void drawTestHALFrame(GL10 gl) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

//        int textureID = mCameraMatrix.onDrawToTexture(mFrontTexture);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GLES20.glViewport(0, 0, mWidth, mHeight);
//        mImageMatrix.onDrawFrame(mFrontTexture, mVertexBuffer, mTextureBuffer, -1, 1, 0, scale, -1);

        if (segResult == null || segResult.length == 0) {
            Log.w("drawTestHALFrame", "drawTestHALFrame error");
            mImageMatrix.onDrawFrame(mFrontTexture, mVertexBuffer, mTextureBuffer, -1, 1, 0, scale, -1);
        } else {
            Log.w("drawTestHALFrame", "drawTestHALFrame success");
            setTextureAlpha();
            if (segmentType == 1) {
                mImageMatrix.onDrawFrame(fgTextureId, mVertexBuffer, mTextureBuffer, segTextureId, 1, 0, scale, bgTextureId);
            } else if (segmentType == 2) {
                mImageMatrix.onDrawFrame(mFrontTexture, mVertexBuffer, mTextureBuffer, segTextureId, 1, 0, scale, -1);
            } else {
                mImageMatrix.onDrawFrame(mFrontTexture, mVertexBuffer, mTextureBuffer, segTextureId, 1, 0, scale, backImageTextureId);

            }
        }

    }

    private int segTextureId = -1;

    public synchronized void setTextureAlpha() {
        try {
            if (-1 == segTextureId) {
                int[] mTextureOutID = new int[1];
                GLES20.glGenTextures(1, mTextureOutID, 0);
                segTextureId = mTextureOutID[0];
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureOutID[0]);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            }

//            FloatBuffer buffer = FloatBuffer.wrap(segResult);
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, segTextureId);
//            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_ALPHA, resultWidth, resultHeight, 0, GLES20.GL_ALPHA, GLES20.GL_FLOAT, buffer);

            byte[] bResult = new byte[segResult.length];
            for (int i = 0; i < segResult.length; i++) {
                bResult[i] = (byte) (segResult[i] * 255);
            }
            ByteBuffer byteBuffer = ByteBuffer.wrap(bResult);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, segTextureId);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_ALPHA, resultWidth, resultHeight, 0, GLES20.GL_ALPHA, GLES20.GL_UNSIGNED_BYTE, byteBuffer);
        } catch (Exception e) {
            //频繁切容易数组越界
        }

    }

    private void updateTexture(final byte[] data, int width, int height) {
        nv21Matrix.setOutputSize(mRenderW, mRenderH);
        nv21Matrix.renderNv21(data
                , mFrontTexture
                , width
                , height
                , mCameraManager.isFrontCam());
    }

    public void deleteTextures(GLSurfaceView mGlSurfaceView) {
        mGlSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                if (mCameraMatrix != null) {
                    mCameraMatrix.destroyFramebuffers();
                    mCameraMatrix.destroy();
                }

                if (mImageMatrix != null) {
                    mImageMatrix.destroy();
                }

                if (mFrontTexture != -1) {
                    int textures[] = new int[]{
                            mFrontTexture
                    };
                    GLES20.glDeleteTextures(1, textures, 0);
                }
                if (fgTextureId != -1) {
                    int textures[] = new int[]{
                            fgTextureId
                    };
                    GLES20.glDeleteTextures(1, textures, 0);
                }
                if (bgTextureId != -1) {
                    int textures[] = new int[]{
                            bgTextureId
                    };
                    GLES20.glDeleteTextures(1, textures, 0);
                }
                if (backImageTextureId != -1) {
                    int textures[] = new int[]{
                            backImageTextureId
                    };
                    GLES20.glDeleteTextures(1, textures, 0);
                }

                if (segTextureId != -1){
                    int textures[] = new int[]{
                            segTextureId
                    };
                    GLES20.glDeleteTextures(1, textures, 0);
                    segTextureId = -1;
                }
            }
        });
    }

    public void onDestroy() {
        mWidth = 0;
        mHeight = 0;
        if (null != mSurfaceTexture) {
            mSurfaceTexture.release();
            mSurfaceTexture = null;
        }
    }
}
