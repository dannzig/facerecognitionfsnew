package com.megvii.demoface.segment;

import android.net.Uri;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.videoutils.VideoSurfaceView;
import com.megvii.demoface.videoutils.YuvEncodeHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class SegmentVideoActivity extends BaseActivity implements YuvEncodeHelper.OnHandleOneFinishCallback {
    @BindView(R.id.facepp_layout_surfaceview)
    VideoSurfaceView glSurfaceView;
    @BindView(R.id.facepp_layout_root)
    LinearLayout rootView;
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_segment_video;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("人体抠像");

    }

    @Override
    protected void initData() throws IOException {
        Uri uri = getIntent().getParcelableExtra("videopath");
        String path = ConUtil.getVideoRealPathFromURI(this, uri);
        File file = new File(path);
        if (file.exists()) {
            ArrayList<String> paths = new ArrayList<>();
            paths.add(path);
            startAfterProcessing(paths, 0, 1);
        } else {
            Toast.makeText(this, "文件路径无效：" + path, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConUtil.acquireWakeLock(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ConUtil.releaseWakeLock();
    }

    private YuvEncodeHelper yuvEncodeHelper;

    private void startAfterProcessing(final ArrayList<String> pickeVideos, int index, int count) throws IOException {
        yuvEncodeHelper = new YuvEncodeHelper(this, pickeVideos, rootView, index, count);
        yuvEncodeHelper.setHandleOneFinishCallback(this);
        yuvEncodeHelper.start();
    }


    @Override
    public void onHandleOneFinish() {
        rootView.removeView(glSurfaceView);
        glSurfaceView = new VideoSurfaceView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        glSurfaceView.setLayoutParams(layoutParams);
        rootView.addView(glSurfaceView);
    }

    @Override
    public void onHandleAllFinish(boolean isForce) {
        finish();
    }

    @OnClick(R.id.ll_go_back)
    public void onViewClicked() {
        finish();
    }
}
