package com.megvii.demoface.segment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.SelectedActivity;
import com.megvii.facepp.multi.sdk.BodySegmentApi;
import com.megvii.facepp.multi.sdk.FaceppApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SegmentModeActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_segment_robust)
    TextView tvSegmentRobust;
    @BindView(R.id.tv_segment_fast)
    TextView tvSegmentFast;
    @BindView(R.id.rl_loading_view)
    LinearLayout rlLoadingView;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_segment_mode;
    }

    @Override
    protected void initView() {
        tvVersion.setText(FaceppApi.getInstance().getApiVersion());
        tvTitleBar.setText("human body cutout");
    }

    @Override
    protected void initData() {

    }

    private boolean isClickAble = true;
    private long lastClickTime = 0;

    @OnClick({R.id.ll_go_back, R.id.tv_segment_robust, R.id.tv_segment_fast})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.tv_segment_robust:
                if (isClickAble && (System.currentTimeMillis() - lastClickTime) > 500) {
                    isClickAble = false;
                    lastClickTime = System.currentTimeMillis();
                    goToSegment(BodySegmentApi.SEGMENT_MODE_ROBUST);
                }
                break;
            case R.id.tv_segment_fast:
                if (isClickAble && (System.currentTimeMillis() - lastClickTime) > 500) {
                    isClickAble = false;
                    lastClickTime = System.currentTimeMillis();
                    goToSegment(BodySegmentApi.SEGMENT_MODE_FAST);
                }
                break;
        }
    }

    private static final int REQUESTCODE = 1;

    private void goToSegment(int segMode) {
        Intent intent = new Intent(this, SelectedActivity.class);
        intent.putExtra("sdkType", SEGMENT);
        intent.putExtra("segMode", segMode);
        startActivityForResult(intent, REQUESTCODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.w("onActivityResult", "requestCode = " + requestCode + " ,resultCode = " + resultCode);
        if (requestCode == REQUESTCODE) {
            isClickAble = true;
            rlLoadingView.setVisibility(View.GONE);
            if (resultCode != 100) {
                Toast.makeText(this, "Model loading failed", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
