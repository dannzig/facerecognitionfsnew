package com.megvii.demoface.segment;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.utils.SensorEventUtil;

public class SegmentGlSurfaceView extends GLSurfaceView {
    private Context mContext;
    private SensorEventUtil sensorUtil;
    private CameraManager mCameraManager;
    private SegmentGlRender render;
    private RequestRenderListener requestRenderListener = new RequestRenderListener() {
        @Override
        public void startRequestRender() {
            requestRender();
        }

        @Override
        public void runOnRenderThread(Runnable runnable) {
            queueEvent(runnable);
        }
    };

    public SegmentGlSurfaceView(Context context) {
        super(context);
        mContext = context;
    }

    public SegmentGlSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void startPreview() {
//        if (render!= null){
//            render.startPreview();
//        }
    }

    public void setCameraManager(CameraManager mCameraManager, SensorEventUtil sensorUtil) {
        this.mCameraManager = mCameraManager;
        this.sensorUtil = sensorUtil;
        init();
    }

    public SegmentGlRender getCameraRender() {
        return render;
    }

    private void init() {
        render = new SegmentGlRender(mContext, mCameraManager, sensorUtil);
        render.setRequestRenderListener(requestRenderListener);
        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(3);
        setRenderer(render);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroyRender();
    }

    private void destroyRender() {
        if (render != null) {
            render.deleteTextures(this);
            render.onDestroy();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        destroyRender();
    }

    public interface RequestRenderListener {
        void startRequestRender();

        void runOnRenderThread(Runnable runnable);
    }


}
