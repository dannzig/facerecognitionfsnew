package com.megvii.demoface.videoutils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import com.megvii.facepp.multi.sdk.FaceppApi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.app.ProgressDialog.STYLE_HORIZONTAL;

public class YuvEncodeHelper implements MegviiDecoder.Callback {

    private final static String TAG = "YuvEncodeHelper";
    private Activity mActivity;
    private ArrayList<String> mPaths;
    private String mCurrPath;

    private final static String SAVE_DIR = "/sdcard/DCIM/facePP/";

    private MegviiEncoder mMegviiEncoder;

    private HandlerThread mHandlerThread;

    private Handler mWorkHandler;

    private MegviiDecoder mMegviiDecoder;

    private ViewGroup mHintView;

    private ProgressDialog progressDialog;

    public static final int WINDOW_TITLE = 0;
    public static final int DIALOG = 1;

    private final int mViewType = WINDOW_TITLE;

    private int mVideoWidth, mVideoHeight, mSegmentColor, mRotation, mVideoFrameCount, mHandleCount, mTotalCount, currIndex;

    private long mTimeStamp;

    private final int WAIT_PERIOD = 1500;//等待解码和编码结束的时间

    private final static int ENCODE_YUV = 0;
    private final static int ENCODE_TEXTURE = 1;
    private final static int ENCODE_TEXTURE_YUV = 2;

    private int encodeType = ENCODE_TEXTURE;


    private OnHandleOneFinishCallback mHandleOneFinishCallback;

    public YuvEncodeHelper(Activity activity, ArrayList<String> paths, ViewGroup hintView, int index, int count) {
        mActivity = activity;
        mPaths = paths;
        currIndex = index + 1;
        mHintView = hintView;
//        mTotalCount = paths.size();
        mHandleCount = mTotalCount = count;//todo 使用paths.size()

        File file = new File(SAVE_DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mActivity);

        mHandlerThread = new HandlerThread(TAG);
        mHandlerThread.start();
        mWorkHandler = new Handler(mHandlerThread.getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Log.d(TAG, "handleMessage: " + msg.arg1);//只要收到消息就认为已经结束了
                mMegviiEncoder.setHandleFinish();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mViewType == DIALOG) {
                            progressDialog.setProgress(100);
                        } else {
                            mActivity.setTitle("100%");
                        }

                        Toast.makeText(mActivity, "转换为文件:" + mCurrPath, Toast.LENGTH_LONG).show();
                        if (mHandleCount == mTotalCount) {
                            stopDecode();
                            mHintView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "run release: ");
                                    stopEncode();
                                    release();
                                    if (mViewType == DIALOG) {
                                        progressDialog.cancel();
                                    }
                                    if (mHandleOneFinishCallback != null) {
                                        mHandleOneFinishCallback.onHandleAllFinish(false);
                                    }
                                }
                            }, WAIT_PERIOD);//1s.5的时间留给剩余要写入的视频帧
                        } else {
                            stopDecode();
                            mHintView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "run stop: ");
                                    stopEncode();
                                    if (mHandleOneFinishCallback != null) {
                                        mHandleOneFinishCallback.onHandleOneFinish();
                                    }
                                    try {
                                        handleOne(mPaths.get(mHandleCount));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, WAIT_PERIOD);
                        }

                    }
                });

                return false;
            }
        });

    }

    public void start() throws IOException {
        showDialog();
        handleOne(mPaths.get(0));
    }

    private void handleOne(final String path) throws IOException {
        currFrameCount = 0;
        String title = "正在转换" + currIndex + "/" + mTotalCount + "...";
        if (mViewType == DIALOG) {
            progressDialog.setTitle(title);
        } else {
            mActivity.setTitle(title);
        }

        File file = new File(path);
        if (!file.exists()) {
            return;
        }
        int[] infos = new int[5];
        getPlayInfo(path, infos);
        mVideoWidth = infos[0];
        mVideoHeight = infos[1];
        Log.w(TAG,"mVideoWidth:"+mVideoWidth+",mVideoHeight"+mVideoHeight);
        mRotation = infos[2];
        mVideoFrameCount = (int) (infos[3] * infos[4] / 1000.0);
        mCurrPath = SAVE_DIR + FaceppApi.getInstance().getApiVersion() +"_" + "rotation" + infos[2] + "_" + file.getName();

        Log.d(TAG, "handleOne: " + mCurrPath + " mVideoFrameCount:" + mVideoFrameCount);
        if (encodeType == ENCODE_YUV) {
            mMegviiEncoder = new YuvEncoder(mActivity, infos[0], infos[1], infos[2], infos[4], 20000000, mCurrPath);
        } else if (encodeType == ENCODE_TEXTURE) {
            mMegviiEncoder = new TextureEncoder(mActivity, infos[0], infos[1], infos[2], infos[4], 20000000, mCurrPath);
            for (int i = 0; i < mHintView.getChildCount(); i++) {//mHintView 中一定要包含
                if (mHintView.getChildAt(i) instanceof VideoSurfaceView)
                    mMegviiEncoder.setTextureSurfaceView((VideoSurfaceView) mHintView.getChildAt(i));
            }

        } else {//todo ENCODE_TEXTURE_YUV

        }

        mMegviiEncoder.init(new OnEncodeInitFinishCallback() {
            @Override
            public void onEncodeInitFinish() {
                try {
                    mMegviiDecoder = new MegviiDecoder(path);
                    mMegviiDecoder.setDecodeCallback(YuvEncodeHelper.this);
                    mMegviiEncoder.start();
                    mMegviiDecoder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });//callback to start then


    }


    private void getPlayInfo(String path, int[] data) throws IOException {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            mmr.setDataSource(path);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);//时长(毫秒)
            String width = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);//宽
            String height = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);//高
            String rotation = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);//角度
            data[0] = Integer.valueOf(width);
            data[1] = Integer.valueOf(height);
            data[2] = Integer.valueOf(rotation);
            data[3] = Integer.valueOf(duration);

            Log.d(TAG, "playtime:" + duration + " w=" + width + " h=" + height + " r=" + rotation);

        } catch (Exception ex) {
            Log.e(TAG, "MediaMetadataRetriever exception " + ex);
        } finally {
            mmr.release();
        }

        MediaExtractor extractor = new MediaExtractor();
        int frameRate = 30; //may be default
        try {
            //Adjust data source as per the requirement if file, URI, etc.
            extractor.setDataSource(path);
            int numTracks = extractor.getTrackCount();
            for (int i = 0; i < numTracks; ++i) {
                MediaFormat format = extractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);
                if (mime.startsWith("video/")) {
                    if (format.containsKey(MediaFormat.KEY_FRAME_RATE)) {
                        data[4] = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                        Log.d(TAG, "getPlayTime frameRate: " + frameRate);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "getPlayTime can not get frameRate: ");
        } finally {
            //Release stuff
            extractor.release();
        }

    }

    private void showDialog() {
        if (mViewType == DIALOG) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog = new ProgressDialog(mActivity);
                    progressDialog.setProgressStyle(STYLE_HORIZONTAL);
                    progressDialog.setMax(100);
                    progressDialog.setTitle("正在转换" + currIndex + "/" + mTotalCount + "...");
                    progressDialog.setCancelable(false);
                    progressDialog.setButton("终止", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            forceStop();
                        }
                    });
                    progressDialog.show();
                }
            });
        } else {
            mActivity.setTitleColor(Color.BLACK);
        }
    }

    private void stopDecode() {
        if (mMegviiDecoder != null) {
            mMegviiDecoder.stopDecode();
            mMegviiDecoder.release();
            mMegviiDecoder = null;
        }
    }

    private synchronized void stopEncode() {

        if (mMegviiEncoder != null) {
            mMegviiEncoder.release();
            mMegviiEncoder = null;
        }
    }

    private synchronized void release() {
        Log.d(TAG, "release: ");
        if (mHandlerThread != null) {
            mHandlerThread.quit();
            mHandlerThread = null;
        }
    }

    public void forceStop() {
        Toast.makeText(mActivity, "转换为文件:" + mCurrPath, Toast.LENGTH_LONG).show();
        mMegviiEncoder.setHandleFinish();
        stopDecode();
        mHintView.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopEncode();
                release();
                if (mViewType == DIALOG) {
                    progressDialog.cancel();
                }
                if (mHandleOneFinishCallback != null) {
                    mHandleOneFinishCallback.onHandleAllFinish(true);
                }
            }
        }, WAIT_PERIOD);
    }

    @Override
    public void decodeSuccess(final MegviiDecoder.ImageBean imageBean, final int cropWidth, final int cropHeight) {
        mWorkHandler.post(new Runnable() {
            @Override
            public void run() {
                onBudySegFrame(imageBean);
            }
        });
    }

    private boolean debug = false;

    private synchronized void onBudySegFrame(final MegviiDecoder.ImageBean imageBean) {
        Log.d(TAG, "onBudySegFrame: ");
        if (mHandlerThread == null) {
            return;
        }
        mTimeStamp = imageBean.getTimeStamp();
        updateEncodeStatus(imageBean, new EncodeFrameFinishCallback() {
            @Override
            public void onFrameFinish() {
                Log.d(TAG, "onFrameFinish: ");
                currFrameCount++;
                final int progress = (int) (currFrameCount / (float) mVideoFrameCount * 100);

                if (imageBean.getImageStatus() == MegviiDecoder.ImageBean.LAST_FRAME) {
                    mWorkHandler.sendEmptyMessage(0x123);
                }

                Log.d(TAG, "handleOutputIndex timestamp: " + mTimeStamp + " mhandleCount:" + mHandleCount + " totalCount:" + mTotalCount + " currFrameCount:" + currFrameCount + " mVideoFrameCount:" + mVideoFrameCount);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mViewType == DIALOG) {
                            if (progressDialog != null)
                                progressDialog.setProgress(progress);
                        } else {
                            mActivity.setTitle(String.valueOf(progress > 100 ? 100 : progress) + "%");
                        }

                    }
                });
                if (mMegviiDecoder != null)
                    mMegviiDecoder.setHandleFinish();//对于yuv编码，直接segment完毕认为结束，对于texture编码，则为textureid成功返回后完毕，让mMegviiDecoder开启下一帧
            }
        });
//        if (debug) {
//            FileOutputStream fos = null;
//            try {
//                fos = new FileOutputStream("/sdcard/test_yuv.gray");
//                fos.write(imageBean.getData());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            debug = false;
//        }

    }

    private int currFrameCount;

    private void updateEncodeStatus(MegviiDecoder.ImageBean imageBean, EncodeFrameFinishCallback callback) {
        mMegviiEncoder.eatFrame(imageBean, callback);
    }

    @Override
    public void onFinish() {

    }

    public void setHandleOneFinishCallback(OnHandleOneFinishCallback oneFinishCallback) {
        mHandleOneFinishCallback = oneFinishCallback;
    }

    public interface EncodeFrameFinishCallback {
        void onFrameFinish();
    }

    public interface OnHandleOneFinishCallback {
        void onHandleOneFinish();

        void onHandleAllFinish(boolean isForce);
    }

    public interface OnEncodeInitFinishCallback {
        void onEncodeInitFinish();
    }

}
