package com.megvii.demoface.videoutils;

import android.app.Activity;

import androidx.annotation.NonNull;

public abstract class MegviiEncoder {

    protected int bWidth, bHeight, bOrientation, bFrameRate, bBitRate;
    protected String bOutputFile;

    protected Activity bActivity;

    public MegviiEncoder(Activity activity, int width, final int height, int orientation, int frameRate, int bitRate, @NonNull String outputFile) {
        bWidth = width;
        bHeight = height;
        bOrientation = orientation;
        bFrameRate = frameRate;
        bBitRate = bitRate;
        bOutputFile = outputFile;
        bActivity = activity;
    }

    public abstract void init(YuvEncodeHelper.OnEncodeInitFinishCallback onEncodeInitFinishCallback);


    public abstract void release();

    public abstract void start();

    public abstract void eatFrame(MegviiDecoder.ImageBean imageBean,YuvEncodeHelper.EncodeFrameFinishCallback callback);

    /**
     * 让 megvii encoder认为目前自己已经无需工作
     */
    public abstract void setHandleFinish();

    public void setTextureSurfaceView(VideoSurfaceView videoSurfaceView) {

    }



}
