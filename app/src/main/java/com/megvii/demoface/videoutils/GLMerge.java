package com.megvii.demoface.videoutils;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import com.megvii.demoface.R;
import com.megvii.demoface.opengl.OpenGLUtils;
import com.megvii.demoface.videoutils.gl.GlUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by xiejiantao on 08/12/2017.
 */

public class GLMerge {
    private static String TAG = "GLMerge";
    private final String mVertexShader;
    private final String mFragmentShader;
    //2d
    private final String m2dTo2dShader;


    protected int mGLProgId;
    protected int mGL2dProgId;


    protected int mGLAttribPosition;
    protected int mGLUniformAlphaTexture;
    protected int mGLUniformSrcTexture;
    protected int mGLAttribTextureCoordinate;


    protected int mGL2dAttribPosition;
    protected int mGL2dUniformSrcTexture;
    protected int mGL2dAttribTextureCoordinate;

    protected boolean mIsInitialized;
    protected FloatBuffer mGLCubeBuffer;
    protected FloatBuffer mGLTextureBuffer;


    private int mFrameWidth = -1;
    private int mFrameHeight = -1;
    protected int[] mFrameBuffers = null;
    protected int[] mFrameBufferTextures = null;
    protected int[] mFrame2dBufferTextures = null;

    private final float vertexPoint[] = {
            -1.0f, 1.0f,
            -1.0f, -1.0f,
            1.0f, 1.0f,
            1.0f, -1.0f,
    };
    private final float texturePoint[] = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
    };

    public GLMerge(Context context) {

        mVertexShader = OpenGLUtils.loadFromRawFile(context, R.raw.vertex_merge);
        mFragmentShader = OpenGLUtils.loadFromRawFile(context, R.raw.merge_fragment);

        m2dTo2dShader = OpenGLUtils.loadFromRawFile(context, R.raw.rgba2dto2d_fragment);


        mGLCubeBuffer = ByteBuffer.allocateDirect(vertexPoint.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(vertexPoint).position(0);

        mGLTextureBuffer = ByteBuffer.allocateDirect(texturePoint.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(texturePoint).position(0);
        init();
    }

    protected void init() {
        GlUtil.checkGlError("kkk");

        mGLProgId = OpenGLUtils.loadProgram(mVertexShader, mFragmentShader);
        GlUtil.checkGlError("iii1"+mGLProgId);

        mGLAttribPosition = GLES20.glGetAttribLocation(mGLProgId, "position");
        GlUtil.checkGlError("iii2"+mGLAttribPosition+"dd"+mGLProgId);

        mGLUniformAlphaTexture = GLES20.glGetUniformLocation(mGLProgId, "alMaskTexture");

        mGLUniformSrcTexture = GLES20.glGetUniformLocation(mGLProgId, "srcTexture");


        mGLAttribTextureCoordinate = GLES20.glGetAttribLocation(mGLProgId,
                "inputTextureCoordinate");


        GlUtil.checkGlError("iii");
//        Log.d("glerror", "printGLerror: glerror location="+"mGLUniformAlphaTexture"+mGLUniformAlphaTexture+"mGLUniformSrcTexture"+mGLUniformSrcTexture+"mGLProgId"+mGLProgId);

        mGL2dProgId = OpenGLUtils.loadProgram(mVertexShader, m2dTo2dShader);


        mGL2dAttribPosition = GLES20.glGetAttribLocation(mGL2dProgId, "position");
        mGL2dUniformSrcTexture = GLES20.glGetUniformLocation(mGL2dProgId, "Texture");
        mGL2dAttribTextureCoordinate = GLES20.glGetAttribLocation(mGL2dProgId,
                "inputTextureCoordinate");


        mIsInitialized = true;
    }


    public void initCameraFrameBuffer(int outTexture, int width, int height) {
        if (mFrameBuffers != null && (mFrameWidth != width || mFrameHeight != height))
            destroyFramebuffers();
        if (mFrameBuffers == null) {
            mFrameWidth = width;
            mFrameHeight = height;
            mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];
            mFrameBufferTextures[0] = outTexture;
//            Log.e("glerror", "printGLerror: glerror blurtextureid "+mFrameBufferTextures[0]);


            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);

            bindFrameBuffer(mFrameBufferTextures[0], mFrameBuffers[0], width, height);
        }
    }


    public void initCameraFrameBufferV2(int outTexture, int width, int height) {
        if (mFrameBuffers != null && (mFrameWidth != width || mFrameHeight != height))
            destroyFramebuffers();
        if (mFrameBuffers == null) {
            mFrameWidth = width;
            mFrameHeight = height;
            mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];


            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);

            mFrame2dBufferTextures = new int[1];
            GLES20.glGenTextures(1, mFrame2dBufferTextures, 0);

//            Log.e("glerror", "printGLerror: glerror blurtextureid "+mFrameBufferTextures[0]);


            bindFrameBufferV2(mFrameBufferTextures[0], mFrameBuffers[0], width, height);
            bindFrameBufferV2(mFrame2dBufferTextures[0], mFrameBuffers[0], width, height);

        }
    }

    private void bindFrameBuffer(int textureId, int frameBuffer, int width, int height) {

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, textureId, 0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    private void bindFrameBufferV2(int textureId, int frameBuffer, int width, int height) {

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0,
                GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, textureId, 0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    public void destroyFramebuffers() {
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameWidth = -1;
        mFrameHeight = -1;
    }


    public void destroy() {
        mIsInitialized = false;
    }

    boolean isCached = false;

    public int onDrawFrame(int alpha, int src) {
        GlUtil.checkGlError("s10");

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);
//        Log.e("glerror", "printGLerror: glerror alpah    "+alpha+"src"+src+"mGLUniformAlphaTexture"+mGLUniformAlphaTexture+"mGLUniformSrcTexture"+mGLUniformSrcTexture);
//        Log.e("glerror", "printGLerror: glerror "+"mGLAttribPosition"+mGLAttribPosition+"mGLAttribTextureCoordinate"+mGLAttribTextureCoordinate);
        GlUtil.checkGlError("s101");


        GLES20.glUseProgram(mGLProgId);
        if (!mIsInitialized) {
            return OpenGLUtils.NOT_INIT;
        }
        GlUtil.checkGlError("s11");
        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);
        GlUtil.checkGlError("s12");

        mGLTextureBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mGLTextureBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);
        GlUtil.checkGlError("s22");

        if (alpha != OpenGLUtils.NO_TEXTURE) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, alpha);
            GLES20.glUniform1i(mGLUniformAlphaTexture, 0);


            GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
            if (isCached) {
                Log.d(TAG, "onDrawFrame: render logic cache");
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrame2dBufferTextures[0]);

            } else {
                Log.d(TAG, "onDrawFrame: render logic src");

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, src);
            }
            GLES20.glUniform1i(mGLUniformSrcTexture, 1);


        }
        GlUtil.checkGlError("s33");

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GlUtil.checkGlError("s331");

        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GlUtil.checkGlError("s44");
//        Log.e("glerror", "printGLerror: glerror blurtexture ret id  "+mFrameBufferTextures[0]);
        return mFrameBufferTextures[0];
    }


    public int onDrawtoCacheFrame(int srcTexure) {

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, mFrame2dBufferTextures[0], 0);
//        Log.e("glerror", "printGLerror: glerror 2d src    "+srcTexure+"  mGL2dUniformSrcTexture"+mGL2dUniformSrcTexture);
//        Log.e("glerror", "printGLerror: glerror          "+"  mGL2dAttribPosition"+mGL2dAttribPosition+"    mGL2dAttribTextureCoordinate"+mGL2dAttribTextureCoordinate);


        GLES20.glUseProgram(mGL2dProgId);
        if (!mIsInitialized) {
            return OpenGLUtils.NOT_INIT;
        }
        GlUtil.checkGlError("s11");
        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGL2dAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGL2dAttribPosition);

        mGLTextureBuffer.position(0);
        GLES20.glVertexAttribPointer(mGL2dAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mGLTextureBuffer);
        GLES20.glEnableVertexAttribArray(mGL2dAttribTextureCoordinate);
        GlUtil.checkGlError("s22");

        if (srcTexure != OpenGLUtils.NO_TEXTURE) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, srcTexure);
            GLES20.glUniform1i(mGL2dUniformSrcTexture, 0);
        }
        GlUtil.checkGlError("s33");

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GlUtil.checkGlError("s331");

        GLES20.glDisableVertexAttribArray(mGL2dAttribPosition);
        GLES20.glDisableVertexAttribArray(mGL2dAttribTextureCoordinate);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GlUtil.checkGlError("s44");
        isCached = true;
//        Log.e("glerror", "printGLerror: glerror blurtexture ret id  "+mFrameBufferTextures[0]);
        return mFrame2dBufferTextures[0];
    }

}
