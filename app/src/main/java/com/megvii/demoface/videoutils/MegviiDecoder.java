package com.megvii.demoface.videoutils;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.media.Image;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.nio.ByteBuffer;


public class MegviiDecoder {

    private static final String TAG = "MegviiDecoder";
    private boolean VERBOSE = false;
    private String mVideoPath;
    private MediaExtractor mMediaExtractor;
    private MediaFormat mMediaFormat;
    private MediaCodec mDecoder;
    private boolean isDecoding;
    private Callback mCallback;

    private Thread workThread;

//    public final static int CAPACITY = 8;
//    private ArrayBlockingQueue<byte[]> recycle = new ArrayBlockingQueue<>(CAPACITY);
//    private ArrayBlockingQueue<byte[]> buffer = new ArrayBlockingQueue<>(CAPACITY);

    private int mHandleType;

    private boolean handleFinish = true;

    public MegviiDecoder(@NonNull String path) throws IOException {
        mVideoPath = path;
        initMediaExtractor();
        initDecoder();
    }

    private void initMediaExtractor() throws IOException {
        mMediaExtractor = new MediaExtractor();
        mMediaExtractor.setDataSource(mVideoPath);

        int trackCount = mMediaExtractor.getTrackCount();
        for (int i = 0; i < trackCount; i++) {
            //寻找视频轨
            MediaFormat format = mMediaExtractor.getTrackFormat(i);
            if (format.getString(MediaFormat.KEY_MIME).startsWith("video/")) {
                mMediaExtractor.selectTrack(i);
                mMediaFormat = format;
                return;
            }
        }
    }

    private void initDecoder() throws IOException {
        if (mMediaFormat == null) {
            throw new RuntimeException("MediaExtractor not select video track.");
        }

        mMediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible);

        mDecoder = MediaCodec.createDecoderByType(mMediaFormat.getString(MediaFormat.KEY_MIME));
        mDecoder.configure(mMediaFormat, null, null, 0);
        mDecoder.start();
    }

    private boolean isFirst = true;
    private int skipCount = 0;

    private final Object mLock = new Object();

    public void start() {
        if (mDecoder == null) {
            throw new UnsupportedOperationException("no decoder found.");
        }

        workThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isDecoding = true;
                    MediaCodec.BufferInfo outputInfo = new MediaCodec.BufferInfo();

                    while (isDecoding) {
//                        Log.d("sxf", "run isDecoding Thread id: "+Thread.currentThread().getId());
                        if (!isInputOver) {
                            int inputIndex = mDecoder.dequeueInputBuffer(1000);
                            if (inputIndex >= 0) {
                                ByteBuffer buffer = mDecoder.getInputBuffer(inputIndex);

                                if (buffer != null) {

                                    int size = mMediaExtractor.readSampleData(buffer, 0);
                                    long time = mMediaExtractor.getSampleTime();

                                    if (size > 0 && time >= 0 && mMediaExtractor.advance()) {
                                        mDecoder.queueInputBuffer(inputIndex, 0, size, time, 0);
                                    } else {
                                        Log.d(TAG, "input is over ,sample size: " + size + " time:" + time + " advance:" + mMediaExtractor.advance());
                                        mDecoder.queueInputBuffer(inputIndex, 0, size < 0 ? 0 : size, time < 0 ? 0 : time, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                                        isInputOver = true;
                                    }
                                }
                            }
                        }

                        int outputIndex = mDecoder.dequeueOutputBuffer(outputInfo, 0);
                        if (outputIndex >= 0) {
                            if (frameCount == 1) {
                                Thread.sleep(500);
                            }
                            if ((outputInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {//最后一帧要多调用一次
                                handleOutputIndex(outputIndex, ImageBean.LAST_FRAME);
                            } else if (frameCount == 0) {//第一帧的结果不要
                                handleOutputIndex(outputIndex, ImageBean.FIRST_FRAME);
                            } else {//其他帧正常输入输出
                                handleOutputIndex(outputIndex, ImageBean.NORMAL_FRAME);
                            }

                            mDecoder.releaseOutputBuffer(outputIndex, false);

                            if ((outputInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                                Log.d(TAG, "finish: ");
                                //结束循环
                                break;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                release();
            }
        });

        workThread.start();

    }

    private boolean debug = true;
    private int frameCount = 0;

    private void handleOutputIndex(int outputIndex, int imageStatus) {//应该和stop release互斥，保证结束以后不能调用
        try {

            waitHandlePeriod(30);
            synchronized (mLock) {
                Image image = mDecoder.getOutputImage(outputIndex);

                if (image != null) {
                    frameCount++;
                    Log.d(TAG, "handleOutputIndex timestamp: " + image.getTimestamp() + " frameCount:" + frameCount);
                    if (image.getFormat() == ImageFormat.YUV_420_888) {
                        Rect rect = image.getCropRect();

                        int length = rect.width() * rect.height();
//                    int length = image.getWidth() * image.getHeight();
                        byte[] nv21 = new byte[length * 3 / 2];

//                    readImageIntoBuffer(image, nv21);
                        readCropImageIntoBuffer(image, nv21);
                        revertHalf(nv21);
                        if (debug) {
//                            FileOutputStream fos = new FileOutputStream("/sdcard/test_yuv.gray");
//                            fos.write(nv21);
//                            debug = false;
//                            File file = new File("/sdcard/frames");
//                            if (!file.exists()) {
//                                file.mkdirs();
//                            }
//                            YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, rect.width(), rect.height(), null);
//                            yuvImage.compressToJpeg(rect, 100, new FileOutputStream("/sdcard/frames/" + frameCount + "_time_" + image.getTimestamp() + ".jpeg"));
                        }

                        if (mCallback != null && isDecoding) {
                            Log.d(TAG, "run width: " + image.getWidth() + " height:" + image.getHeight() + " cropWidth:" + rect.width() + " cropHeight:" + rect.height());
                            handleFinish = false;
                            ImageBean imageBean = new ImageBean(image.getTimestamp(), nv21, imageStatus);
                            mCallback.decodeSuccess(imageBean, rect.width(), rect.height());//currently we do not need rotation data
                        }

                    }

                    image.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void stopDecode() {
        synchronized (mLock) {
            isDecoding = false;
            workThread.interrupt();
        }
        Log.d(TAG, "stopDecode isDecoding: " + isDecoding);
    }

    /**
     * 一次回调多次post runnable可能会导致乱序，所以保证一次只post一个runnable
     */
    private void waitHandlePeriod(int milli) {
        while (!handleFinish) {
            if (!isDecoding) return;
            try {
//                Log.d(TAG, "waitHandlePeriod: " + isDecoding);
                Thread.sleep(milli);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 超过buffer的容量时候就等待post的runnable执行完毕
     */
//    private void waitBufferPeriod(int milli) {
//        while (buffer.size() >= CAPACITY) {
//            if (!isDecoding) return;
//            try {
//                Log.d(TAG, "waitBufferPeriod: " + isDecoding);
//                Thread.sleep(milli);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }


    private boolean isInputOver = false;

    public void release() {
        synchronized (mLock) {
            if (mMediaExtractor != null) {
                mMediaExtractor.release();
                mMediaExtractor = null;
            }

            if (mDecoder != null) {
                mDecoder.release();
                mMediaExtractor = null;
            }
            if (mCallback != null) {
                mCallback.onFinish();
            }
        }
    }

    public void setDecodeCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void decodeSuccess(ImageBean imageBean, int cropWidth, int cropHeight);//I420 callback

        void onFinish();
    }

    public void setHandleFinish() {
        Log.d(TAG, "setHandleFinish handleFinish: " + handleFinish);
        handleFinish = true;
    }


    private void readImageIntoBuffer(Image image, byte[] data) {
        final int imageWidth = image.getWidth();
        final int imageHeight = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        int offset = 0;
        for (int plane = 0; plane < planes.length; ++plane) {
            final ByteBuffer buffer = planes[plane].getBuffer();
            final int rowStride = planes[plane].getRowStride();
            // Experimentally, U and V planes have |pixelStride| = 2, which
            // essentially means they are packed. That's silly, because we are
            // forced to unpack here.
            final int pixelStride = planes[plane].getPixelStride();
            final int planeWidth = (plane == 0) ? imageWidth : imageWidth / 2;
            final int planeHeight = (plane == 0) ? imageHeight : imageHeight / 2;
            if (pixelStride == 1 && rowStride == planeWidth) {
                Log.d(TAG, "readImageIntoBuffer rowStride == planeWidth: ");
                // Copy whole plane from buffer into |data| at once.
                buffer.get(data, offset, planeWidth * planeHeight);
                offset += planeWidth * planeHeight;
            } else {
                Log.d(TAG, "readImageIntoBuffer plane: " + plane + " pixelStride:" + pixelStride + " rowStride:" + rowStride);
                // Copy pixels one by one respecting pixelStride and rowStride.
                byte[] rowData = new byte[rowStride];
                for (int row = 0; row < planeHeight - 1; ++row) {
                    buffer.get(rowData, 0, rowStride);
                    for (int col = 0; col < planeWidth; ++col) {
                        data[offset++] = rowData[col * pixelStride];
                    }
                }
                // Last row is special in some devices and may not contain the full
                // |rowStride| bytes of data. See  http://crbug.com/458701  and
                // http://developer.android.com/reference/android/media/Image.Plane.html#getBuffer()
                buffer.get(rowData, 0, Math.min(rowStride, buffer.remaining()));
                for (int col = 0; col < planeWidth; ++col) {
                    data[offset++] = rowData[col * pixelStride];
                }
            }
        }
    }


    private void readCropImageIntoBuffer(Image image, byte[] data) {
        final int imageWidth = image.getWidth();
        final int imageHeight = image.getHeight();
        final int cropWidth = image.getCropRect().width();
        final int cropHeight = image.getCropRect().height();
        Log.d(TAG, "readCropImageIntoBuffer start: cropWidth: " + cropWidth + " imageWidth:" + imageWidth + " cropHeight:" + cropHeight + " imageHeight:" + imageHeight);
        if (imageWidth == cropWidth && imageHeight == cropHeight) {
            readImageIntoBuffer(image, data);
            return;
        }
        final Image.Plane[] planes = image.getPlanes();
        int offset = 0;
        for (int plane = 0; plane < planes.length; ++plane) {
            final ByteBuffer buffer = planes[plane].getBuffer();
            final int rowStride = planes[plane].getRowStride();
            // Experimentally, U and V planes have |pixelStride| = 2, which
            // essentially means they are packed. That's silly, because we are
            // forced to unpack here.
            final int pixelStride = planes[plane].getPixelStride();
            final int planeWidth = (plane == 0) ? cropWidth : cropWidth / 2;
            final int planeHeight = (plane == 0) ? cropHeight : cropHeight / 2;
//            if (pixelStride == 1) {
////                if (rowStride == planeWidth){
//                    // Copy whole plane from buffer into |data| at once.
//                    if (cropWidth == imageWidth) {
//                        buffer.get(data, offset, imageWidth * cropHeight);
//                        offset += imageWidth * cropHeight;
//                        buffer.position(buffer.position() + (imageHeight - cropHeight) * imageWidth);
//                    } else {
//                        Log.d(TAG, "readCropImageIntoBuffer else cropWidth: "+cropWidth+" imageWidth:"+imageWidth+" cropHeight:"+cropHeight+" imageHeight:"+imageHeight);
//                        for (int row = 0; row < cropHeight; ++row) {
//                            buffer.get(data, offset, cropWidth);
//                            buffer.position(buffer.position() + imageWidth - cropWidth);
//                            offset += cropWidth;
//                        }
//                        if (cropHeight != imageHeight) {
//                            buffer.position(buffer.position() + (imageHeight - cropHeight) * imageWidth);
//                        }
//                    }
//
//                    Log.d(TAG, "readCropImageIntoBuffer offset: " + offset +" normal should be:"+(1080*1920));
//                    Log.d(TAG, "readCropImageIntoBuffer buffer position: " + buffer.position() +" normal should be:"+(1088*1920));
//                }

//            } else {
            Log.d(TAG, "readCropImageIntoBuffer plane: " + plane + " pixelStride:" + pixelStride + " rowStride:" + rowStride + " ByteBuffer size:" + buffer.capacity());
            // Copy pixels one by one respecting pixelStride and rowStride.
            byte[] rowData = new byte[rowStride];
            for (int row = 0; row < planeHeight - 1; ++row) {
                buffer.get(rowData, 0, rowStride);
                for (int col = 0; col < planeWidth; ++col) {
                    data[offset++] = rowData[col * pixelStride];
                }
            }
            // Last row is special in some devices and may not contain the full
            // |rowStride| bytes of data. See  http://crbug.com/458701  and
            // http://developer.android.com/reference/android/media/Image.Plane.html#getBuffer()
            buffer.get(rowData, 0, Math.min(rowStride, buffer.remaining()));
            for (int col = 0; col < planeWidth; ++col) {
                data[offset++] = rowData[col * pixelStride];
            }

//            }
        }
    }

    public static class ImageBean {
        private long timeStamp;
        private byte[] data;
        private int imageStatus;

        public final static int FIRST_FRAME = 0;
        public final static int NORMAL_FRAME = 1;
        public final static int LAST_FRAME = 2;


        public ImageBean(long timeStamp, byte[] data, int imageStatus) {
            this.timeStamp = timeStamp;
            this.data = data;
            this.imageStatus = imageStatus;
        }


        public long getTimeStamp() {
            return timeStamp;
        }

        public byte[] getData() {
            return data;
        }

        public int getImageStatus() {
            return imageStatus;
        }
    }

    private void revertHalf(byte[] yuvData) {
        int SIZE = yuvData.length;
        byte[] uv = new byte[SIZE / 3];
        int u = SIZE / 6 * 4;
        int v = SIZE / 6 * 5;
        for (int i = 0; i < uv.length - 1; i += 2) {
            uv[i] = yuvData[v++];
            uv[i + 1] = yuvData[u++];
        }
        for (int i = SIZE / 3 * 2; i < SIZE; i++) {
            yuvData[i] = uv[i - SIZE / 3 * 2];
        }

    }

    public String getmVideoPath() {
        return mVideoPath;
    }
}
