package com.megvii.demoface.videoutils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import androidx.annotation.NonNull;

import com.megvii.demoface.opengl.ImageMixMatrix;
import com.megvii.demoface.opengl.NV21Matrix;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.NoDoubleClickUtil;
import com.megvii.demoface.videoutils.gl.GlUtil;
import com.megvii.demoface.videoutils.gl.OpenglUtilNormal;
import com.megvii.facepp.multi.sdk.BodySegmentApi;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.segment.SegmentResult;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayDeque;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class TextureEncoder extends MegviiEncoder implements GLSurfaceView.Renderer {

    private final static String TAG = "TextureEncoder";

    private int mSurfaceWidth, mSurfaceHeight;

    private SurfaceTexture mSurfaceTexture;

    private ImageMixMatrix mImageMixMatrix;

    private NV21Matrix mNv21Matrix;

    private FloatBuffer mVertexBuffer;
    private FloatBuffer mVideoVertexBuffer;
    private FloatBuffer mVideoTextureBuffer;
    private FloatBuffer mTextureBuffer;


    YuvEncodeHelper.OnEncodeInitFinishCallback mEncodeInitFinishCallback;

    /**
     * OpenGL params
     */
    private VideoSurfaceView.RequestRenderListener requestRenderListener;
    private int[] mInTextureId, mOutTextureId, mCacheTexId;
    private int mTextureId;
    float[] textureCords;

    public static float segMinValue = 0.0f;
    public static float segMaxValue = 1.0f;

    public int segTextureId;

    private float[] segResult;
    private int resultWidth;
    private int resultHeight;

    private boolean isFirstIn = true;  //为了适配低端机 ，单独线程检测，可能开始没数据。

    private VideoSurfaceView mVideoSurfaceView;

    private TextureMovieEncoder mVideoEncoder;

    private FacePPImage.Builder imageBuilder;

    private GLMerge glMerge;

    public TextureEncoder(Activity activity, int width, int height, int orientation, int frameRate, int bitRate, @NonNull String outputFile) {
        super(activity, width, height, orientation, frameRate, bitRate, outputFile);
        imageBuilder = new FacePPImage.Builder().setWidth(width).setHeight(height).setMode(FacePPImage.IMAGE_MODE_NV21).setRotation((360 - orientation) % 360);

    }

    public void setTextureSurfaceView(VideoSurfaceView videoSurfaceView) {
        mVideoSurfaceView = videoSurfaceView;
        mVideoSurfaceView.init(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(TAG, "onSurfaceCreated: ");
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GlUtil.checkGlError("change1");

        Log.d(TAG, "onSurfaceChanged: ");
        if (NoDoubleClickUtil.isDoubleChanged()) {
            //防止异常
            Log.e(TAG, "surface changed twice");
            return;
        }

        mSurfaceWidth = width;
        mSurfaceHeight = height;

        GLES20.glClearColor(0, 0, 0, 1.0f);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GlUtil.checkGlError("change2");

        textureCords = OpenglUtilNormal.getRotation(bOrientation, false, false);
//        textureCords = OpenglUtil.TEXTURE_NO_ROTATION;//for fit rotation in mux,here always take it as no rotation
        mImageMixMatrix = new ImageMixMatrix(bActivity);
        mImageMixMatrix.initCameraFrameBuffer(bWidth, bHeight);
        GlUtil.checkGlError("change3");

        mVertexBuffer = ByteBuffer.allocateDirect(OpenglUtilNormal.CUBE.length * 4).order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mVertexBuffer.put(OpenglUtilNormal.CUBE).position(0);
        GlUtil.checkGlError("change4");

        mVideoVertexBuffer = ByteBuffer.allocateDirect(OpenglUtilNormal.CUBE.length * 4).order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mVideoVertexBuffer.put(OpenglUtilNormal.CUBE).position(0);

        mTextureBuffer = ByteBuffer.allocateDirect(textureCords.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextureBuffer.put(textureCords).position(0);

        mVideoTextureBuffer = ByteBuffer.allocateDirect(textureCords.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
//        if (bOrientation  == 0){
//            mVideoTextureBuffer.put(OpenglUtilNormal.getRotation(180, false, false)).position(0);
//        }else{
        mVideoTextureBuffer.put(OpenglUtilNormal.getRotation(0, false, true)).position(0);

//        }

        //set up surfacetexture------------------
        SurfaceTexture oldSurfaceTexture = mSurfaceTexture;
        GlUtil.checkGlError("change5");

//        mTextureId = ICameraMatrix.getExternalOESTextureID();
        mSurfaceTexture = new SurfaceTexture(mTextureId);
        GlUtil.checkGlError("change6");
//        mSurfaceTexture.setOnFrameAvailableListener(this);
        if (oldSurfaceTexture != null) {
            oldSurfaceTexture.release();
        }

        mNv21Matrix = new NV21Matrix();
        GlUtil.checkGlError("change7");

        mImageMixMatrix.init();
        GlUtil.checkGlError("change8");

        mInTextureId = OpenglUtilNormal.initTextureID(bWidth, bHeight);

        mOutTextureId = OpenglUtilNormal.initTextureID(bWidth, bHeight);
        mCacheTexId = OpenglUtilNormal.initTextureID(bWidth, bHeight);
        GlUtil.checkGlError("change9-1");
//        glMerge = new GLMerge(bActivity);
//        GlUtil.checkGlError("change9-2");
//        glMerge.initCameraFrameBufferV2(0, bWidth, bHeight);
//        GlUtil.checkGlError("change9-3");

        GLES20.glViewport(0, 0, width, height);
        //changeImageDisplaySize(width, height);
        sceenAutoFit(width, height, bWidth, bHeight, bOrientation);//todo 先写死是0
        isFirstIn = true;

        if (mEncodeInitFinishCallback != null)
            mEncodeInitFinishCallback.onEncodeInitFinish();
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GlUtil.checkGlError("draw1");

        long startTime = System.currentTimeMillis();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (segSize[0] > 0 && segSize[1] > 0/*&& segAlphabt != null && segAlphabt.length > 0*/) {
//            setTextureAlpha();
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            GLES20.glViewport(0, 0, mSurfaceWidth, mSurfaceHeight);
            if (DRAW_FRAME) {
                Log.d(TAG, "onDrawFrame: ");
                float cameraRatio = bWidth * 1.0f / bHeight;
                float bgRatio = mImageMixMatrix.bgWidth * 1.0f / mImageMixMatrix.bgHeight;
                float scaler = cameraRatio / bgRatio;
                GlUtil.checkGlError("draw212");

                mImageMixMatrix.onDrawFrame(mInTextureId[0], mVertexBuffer, mTextureBuffer, segTextureId, segMaxValue, segMinValue, scaler, -1);
                GlUtil.checkGlError("draw2");

                if (debug) {
                    saveFrame();
                    debug = false;
                }
            }
            isFirstIn = false;

        } else {
            if (!isFirstIn) {
                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
                GLES20.glViewport(0, 0, mSurfaceWidth, mSurfaceHeight);
                //// TODO: 2017/9/6
            }
        }
        Log.d("sxf", "onDrawFrame cost time: " + (System.currentTimeMillis() - startTime));
    }


    /**
     * 停止录制，并刷新相册
     */
    public void stopRecording() {
        Log.d(TAG, "stopRecording: ");
        mVideoEncoder.stopRecording();
        mVideoEncoder = null;
        ConUtil.updateAlbum(bActivity, new File(bOutputFile));
    }


    public void startRecord() {
        Log.d(TAG, "onStart: ");
//        if (bOrientation == 270 || bOrientation == 90) {
//            mVideoEncoder.startRecording(new TextureMovieEncoder.EncoderConfig(
//                    new File(bOutputFile), bHeight, bWidth, bBitRate, EGL14.eglGetCurrentContext(),bOrientation), textureBuffer);
//        } else {
        mVideoEncoder.startRecording(new TextureMovieEncoder.EncoderConfig(
                new File(bOutputFile), bWidth, bHeight, bBitRate, EGL14.eglGetCurrentContext(), bOrientation));
//        }
    }

    private int handleCount = 0;

    private final static boolean DRAW_FRAME = true;

    public synchronized void setTextureAlpha() {
        try {
            if (0 == segTextureId) {
                int[] mTextureOutID = new int[1];
                GLES20.glGenTextures(1, mTextureOutID, 0);
                segTextureId = mTextureOutID[0];
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureOutID[0]);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            }

//            FloatBuffer buffer = FloatBuffer.wrap(segResult);
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, segTextureId);
//            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_ALPHA, resultWidth, resultHeight, 0, GLES20.GL_ALPHA, GLES20.GL_FLOAT, buffer);

            byte[] bResult = new byte[segResult.length];
            for (int i = 0; i < segResult.length; i++) {
                bResult[i] = (byte) (segResult[i] * 255);
            }
            ByteBuffer byteBuffer = ByteBuffer.wrap(bResult);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, segTextureId);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_ALPHA, resultWidth, resultHeight, 0, GLES20.GL_ALPHA, GLES20.GL_UNSIGNED_BYTE, byteBuffer);
        } catch (Exception e) {
            //频繁切容易数组越界
        }

    }

    public void setRequestRenderListener(VideoSurfaceView.RequestRenderListener requestRenderListener) {
        this.requestRenderListener = requestRenderListener;
    }


    int[] segSize = new int[2]; //wh
    //volatile float[] segAlpha;
    private long mTimeStamp = -1;

    private boolean debug = false;

    public void deleteTextures(GLSurfaceView mGlSurfaceView) {
        Log.d(TAG, "deleteTextures: ");
        if (mTextureId != OpenglUtilNormal.NO_TEXTURE)
            mGlSurfaceView.queueEvent(new Runnable() {
                @Override
                public void run() {
                    mImageMixMatrix.destroy();
                    mNv21Matrix.destroy();
                    GLES20.glDeleteTextures(1, new int[]{mTextureId}, 0);
                    mTextureId = OpenglUtilNormal.NO_TEXTURE;
                    if (mInTextureId != null) {
                        GLES20.glDeleteTextures(1, mInTextureId, 0);
                        mInTextureId = null;
                    }
                    if (mOutTextureId != null) {
                        GLES20.glDeleteTextures(1, mOutTextureId, 0);
                        mOutTextureId = null;
                    }
                    if (mCacheTexId != null) {
                        GLES20.glDeleteTextures(1, mCacheTexId, 0);
                        mCacheTexId = null;
                    }
                    mNv21Matrix = null;
                    mImageMixMatrix = null;
                }
            });
    }


    /**
     * 按照centercrop的源码修改，这里viewport相当于已经做过scale了，
     * 所以不需要额外scale，dx不需要除以2，centercrop 是要除的。
     *
     * @param screenW
     * @param screenH
     * @param cameraW
     * @param cameraH
     * @param angle
     */
    public void sceenAutoFit(int screenW, int screenH, int cameraW, int cameraH, int angle) {
        if (angle == 90 || angle == 270) {
            int temp = cameraW;
            cameraW = cameraH;
            cameraH = temp;
        }
        float scale;
        float dx = 0, dy = 0;
        float dxRatio = 0f;
        float dyRatio = 0f;

        if (cameraW * screenH > screenW * cameraH) {
            scale = (float) screenW / (float) cameraW;
            dy = (screenH - cameraH * scale);
            dyRatio = dy / screenH;
        } else {
            scale = (float) screenW / (float) cameraW;
            dy = (screenH - cameraH * scale);
            dyRatio = dy / screenH;
        }
        float[] cube = new float[]{
                OpenglUtilNormal.CUBE[0] + dxRatio, OpenglUtilNormal.CUBE[1] + dyRatio,
                OpenglUtilNormal.CUBE[2] - dxRatio, OpenglUtilNormal.CUBE[3] + dyRatio,
                OpenglUtilNormal.CUBE[4] + dxRatio, OpenglUtilNormal.CUBE[5] - dyRatio,
                OpenglUtilNormal.CUBE[6] - dxRatio, OpenglUtilNormal.CUBE[7] - dyRatio};
        mVertexBuffer.clear();
        mVertexBuffer.put(cube).position(0);
    }


    @Override
    public void init(YuvEncodeHelper.OnEncodeInitFinishCallback callback) {
        Log.d(TAG, "init: ");
        mEncodeInitFinishCallback = callback;
        mVideoEncoder = new TextureMovieEncoder(bFrameRate);
        mVideoEncoder.updateProgramType(Texture2dProgram.ProgramType.TEXTURE_2D_GRAY);
    }

    @Override
    public void release() {
        Log.d(TAG, "release: ");
        stopRecording();
        mSurfaceTexture.release();
        mSurfaceTexture = null;
    }

    private ArrayDeque<Long> timeQueue = new ArrayDeque<>(ConUtil.SEGMENT_THREAD_COUNT);
    private long lastTime = -1;
    private long totalTime = 0;
    /**
     * 用编码器吃掉这一帧数据
     */
    @Override
    public void eatFrame(final MegviiDecoder.ImageBean imageBean, final YuvEncodeHelper.EncodeFrameFinishCallback callback) {
        if (mSurfaceTexture == null) {//防止停止以后继续回调api
            return;
        }
        segSize[0] = bWidth;
        segSize[1] = bHeight;
        mTimeStamp = imageBean.getTimeStamp();
        timeQueue.addLast(mTimeStamp);

        mVideoSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                GlUtil.checkGlError("s130");
//                mNv21Matrix.setOutputSize(segSize[0], segSize[1]);
//                mNv21Matrix.renderNv21(imageBean.getData(), mInTextureId[0], segSize[0], segSize[1], true);
                int outArray[] = {mCacheTexId[0], mOutTextureId[0]};
                long startTime = System.currentTimeMillis();
                SegmentResult segmentResult = BodySegmentApi.getInstance().segmentFrame(imageBuilder.setData(imageBean.getData()).build());
                totalTime = totalTime + (System.currentTimeMillis()-startTime);
                if (segmentResult != null) {
                    mNv21Matrix.setOutputSize(segSize[0], segSize[1]);
                    mNv21Matrix.renderNv21(segmentResult.getOriginImage(), mInTextureId[0], segmentResult.getImageWidth(), segmentResult.getImageHeight(), true);
                    segResult = segmentResult.getSegResult();
                    resultWidth = segmentResult.getImageWidth();
                    resultHeight = segmentResult.getImageHeight();
                    GlUtil.checkGlError("s110");
                    setTextureAlpha();
                    GlUtil.checkGlError("s120");
                    Log.d(TAG, "segResult size: " + segResult.length + ",resultWidth = " + resultWidth + ",resultHeight = " + resultHeight);
//                    mOutTextureId[0] = glMerge.onDrawFrame(segTextureId,mInTextureId[0]);
                    mOutTextureId[0] = mImageMixMatrix.onDrawToTexture(mInTextureId[0], mVideoVertexBuffer, mVideoTextureBuffer, segTextureId, segMaxValue, segMinValue, 1, -1);
                    GLES20.glFinish();
                    mVideoEncoder.setTextureId(mOutTextureId[0]);
                    long timeStamp = 0;
                    try {
                        timeStamp = timeQueue.pollFirst();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mVideoEncoder.frameAvailable(timeStamp);

//                    mVideoEncoder.frameAvailable(timeStamp == 0 ? lastTime == -1 ? 0 : lastTime + 33000000 : imageBean.getTimeStamp());
                    if (imageBean.getImageStatus() != MegviiDecoder.ImageBean.LAST_FRAME) {
                        callback.onFrameFinish();
                    }
                    handleCount++;
                    requestRenderListener.startRequestRender();
                } else {
                    callback.onFrameFinish();
                }


                if (imageBean.getImageStatus() == MegviiDecoder.ImageBean.LAST_FRAME) { //多线程情况下需把缓存的数据提取干净
                    for (int i = 0; i < ConUtil.SEGMENT_THREAD_COUNT - 1; i++) {
                        try {
                            Thread.sleep(30);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        startTime = System.currentTimeMillis();
                        segmentResult = BodySegmentApi.getInstance().segmentFrame(imageBuilder.setData(imageBean.getData()).build());
                        totalTime = totalTime + (System.currentTimeMillis()-startTime);
                        if (segmentResult != null) {
                            mNv21Matrix.setOutputSize(segSize[0], segSize[1]);
                            mNv21Matrix.renderNv21(segmentResult.getOriginImage(), mInTextureId[0], segmentResult.getImageWidth(), segmentResult.getImageHeight(), true);
                            segResult = segmentResult.getSegResult();
                            resultWidth = segmentResult.getImageWidth();
                            resultHeight = segmentResult.getImageHeight();
                            setTextureAlpha();
                            Log.d(TAG, "segResult size: " + segResult.length + ",resultWidth = " + resultWidth + ",resultHeight = " + resultHeight);
//                    mOutTextureId[0] = glMerge.onDrawFrame(segTextureId,mInTextureId[0]);
                            mOutTextureId[0] = mImageMixMatrix.onDrawToTexture(mInTextureId[0], mVideoVertexBuffer, mVideoTextureBuffer, segTextureId, segMaxValue, segMinValue, 1, -1);
                            GLES20.glFinish();
                            mVideoEncoder.setTextureId(mOutTextureId[0]);
                            long timeStamp = 0;
                            try {
                                timeStamp = timeQueue.pollFirst();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            mVideoEncoder.frameAvailable(timeStamp);
                            handleCount++;
                            requestRenderListener.startRequestRender();
                        }
                    }

                    callback.onFrameFinish();
                    BodySegmentApi.getInstance().reset();
                }
                lastTime = mTimeStamp;

                Log.d(TAG, "eatFrame timestamp: " + mTimeStamp + " lastTime:" + lastTime + " handleCount:" + handleCount + " imageStatus:" + imageBean.getImageStatus() + " mOutTextureId[0]:" + mOutTextureId[0]);
                Log.e("totalTime",""+totalTime);
                GlUtil.checkGlError("s140");

                //todo do segment here

            }
        });

    }
    @Override
    public void setHandleFinish() {
        //do nothing on this implementation , for it's encoder has no loop to finish
    }

    @Override
    public void start() {
        startRecord();
    }


    public void saveFrame() {
        String filename = "/sdcard/out.jpeg";

        ByteBuffer buf = ByteBuffer.allocateDirect(bWidth * bHeight * 4);
        //buf.order(ByteOrder.LITTLE_ENDIAN);
        GLES20.glReadPixels(0, 0, bHeight, bWidth,
                GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buf);
        GlUtil.checkGlError("glReadPixels");
        buf.rewind();

        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            Bitmap bmp = Bitmap.createBitmap(bHeight, bWidth, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsFromBuffer(buf);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bos);
            bmp.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
