package com.megvii.demoface.videoutils;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;


public class VideoSurfaceView extends GLSurfaceView {
    TextureEncoder mTextureEncoder;
    private RequestRenderListener requestRenderListener = new RequestRenderListener() {
        @Override
        public void startRequestRender() {
            requestRender();
        }
    };

    public VideoSurfaceView(Context context) {
        super(context);
    }

    public VideoSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(TextureEncoder textureEncoder) {

        mTextureEncoder = textureEncoder;
        mTextureEncoder.setRequestRenderListener(requestRenderListener);
        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(2);
        setRenderer(mTextureEncoder);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mTextureEncoder.deleteTextures(this);
    }


    public interface RequestRenderListener {
        void startRequestRender();
    }


}
