package com.megvii.demoface.videoutils;

import android.app.Activity;
import android.graphics.ImageFormat;
import android.media.Image;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMuxer;

import android.util.Log;

import androidx.annotation.NonNull;

import com.megvii.demoface.utils.ConUtil;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by shaoxiaofei on 09/12/2017.
 * 编码摄像头
 */

public class YuvEncoder extends MegviiEncoder {

    private final static String TAG = "YuvEncoder";

    private MediaCodec mediaCodec;
    private LinkedList<MegviiDecoder.ImageBean> mInDataQueue = new LinkedList<>();
    private final static int BUFFER_SIZE = 5;
    private int trackIndex = 0;
    private MediaMuxer mMediaMuxer;

    private byte[] yuv420;

    private boolean isFinish = false;

    private int matchedFormat;

    private long lastTime;

    public YuvEncoder(Activity activity, int width, int height, int orientation, int frameRate, int bitRate, @NonNull String outputFile) {
        super(activity, width, height, orientation, frameRate, bitRate, outputFile);
    }

    @Override
    public void init(YuvEncodeHelper.OnEncodeInitFinishCallback callback) {
        try {
            if (supportAvcCodec()) {

                matchedFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;//always consider it to be yuv420flexible
                yuv420 = new byte[calculateLength(ImageFormat.YUV_420_888, bWidth, bHeight)];
                Log.d(TAG, "YuvEncoder matchedFormat: " + matchedFormat + " yuv420 length:" + yuv420.length);
                mMediaMuxer = new MediaMuxer(bOutputFile, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
                MediaFormat mediaFormat = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, bWidth, bHeight);
                mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, bBitRate);
                mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, bFrameRate);
                mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, matchedFormat);
                mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 0);
                mediaCodec = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_VIDEO_AVC);

                mediaCodec.setCallback(new MediaCodec.Callback() {

                    @Override
                    public void onInputBufferAvailable(@NonNull MediaCodec codec, int inputBufferId) {
                        Log.d(TAG, "onInputBufferAvailable: ");
                        ByteBuffer inputBuffer = mediaCodec.getInputBuffer(inputBufferId);
                        int capacity = inputBuffer.capacity();
                        while (!isFinish) {
                            MegviiDecoder.ImageBean availableData = getAvailableInput();
                            if (availableData != null) {
                                inputBuffer.clear();
                                if (matchedFormat == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar) {
                                    Yuv420Util.Nv21ToYuv420SP(availableData.getData(), yuv420, bWidth, bHeight);
                                    inputBuffer.put(yuv420);
                                } else if (matchedFormat == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar) {
                                    Yuv420Util.Nv21ToI420(availableData.getData(), yuv420, bWidth, bHeight);
                                    inputBuffer.put(yuv420);
                                } else {
//                                    SegJni.NV21toYUV420P(availableData.getData(), yuv420, bWidth, bHeight);
                                    Image image = mediaCodec.getInputImage(inputBufferId);
                                    if (image != null) {
                                        Log.d(TAG, "onInputBufferAvailable image bWidth: " + image.getWidth() + " bHeight:" + image.getHeight() + " cropWidth:" + image.getCropRect().width() + " cropHeight:" + image.getCropRect().height());
                                        renderNv21ToImage(image, availableData.getData(), bWidth, bHeight);
                                    }

                                }
                                Log.d(TAG, "eatFrame inputBuffer index: " + inputBufferId + " length:" + inputBuffer.limit() + " timeStamp:" + availableData.getTimeStamp());
                                if (availableData.getTimeStamp() == 0 && lastTime > 0) {//config to finish
                                    Log.d(TAG, "onInputBufferAvailable finish: ");
                                    codec.queueInputBuffer(inputBufferId, 0, capacity, availableData.getTimeStamp() / 1000, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                                } else {
                                    codec.queueInputBuffer(inputBufferId, 0, capacity, availableData.getTimeStamp() / 1000, 0);
                                }


                                lastTime = availableData.getTimeStamp();
                                break;
                            } else {
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }

                    @Override
                    public void onOutputBufferAvailable(@NonNull MediaCodec codec, int index, @NonNull MediaCodec.BufferInfo info) {
                        ByteBuffer outputBuffer = mediaCodec.getOutputBuffer(index);
                        Log.d(TAG, "onOutputBufferAvailable outputBuffer index: " + index + " flags:" + info.flags + " timeus:" + info.presentationTimeUs);
                        // bufferFormat is equivalent to mOutputFormat
                        if (outputBuffer != null) {
                            mMediaMuxer.writeSampleData(trackIndex, outputBuffer, info);
                        }

                        codec.releaseOutputBuffer(index, false);

                    }

                    @Override
                    public void onError(@NonNull MediaCodec codec, @NonNull MediaCodec.CodecException e) {
                        Log.d(TAG, "onError: ");
                    }

                    @Override
                    public void onOutputFormatChanged(@NonNull MediaCodec codec, @NonNull MediaFormat format) {
                        Log.d(TAG, "onOutputFormatChanged: ");
                        trackIndex = mMediaMuxer.addTrack(format);
                        mMediaMuxer.setOrientationHint(bOrientation);
                        mMediaMuxer.start();
                    }

                });

                mediaCodec.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
                callback.onEncodeInitFinish();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public synchronized void eatFrame(MegviiDecoder.ImageBean imageBean, YuvEncodeHelper.EncodeFrameFinishCallback callback) {
        //todo check whether orientation is right,and same input/output resource is supported
//        HumanEffectSegmentApi.getInstance().processYUV(imageBean.getData(),imageBean.getData(),bWidth,bHeight,bOrientation);
        if (mInDataQueue.size() >= BUFFER_SIZE) {
            Log.d(TAG, "eatFrame input count too large: ");
            mInDataQueue.pop();
            mInDataQueue.offerLast(imageBean);
        } else {
            Log.d(TAG, "eatFrame do not care input count: ");
            mInDataQueue.offerLast(imageBean);
        }
        callback.onFrameFinish();
    }

    private synchronized MegviiDecoder.ImageBean getAvailableInput() {
        if (mInDataQueue.size() == 0) return null;
        return mInDataQueue.pop();
    }

    /**
     * 结束等待input
     */
    public void setHandleFinish() {
        Log.d(TAG, "setFinish: ");
        isFinish = true;
    }

    @Override
    public void release() {
        Log.d(TAG, "release: ");
        ConUtil.updateAlbum(bActivity, new File(bOutputFile));
        if (mediaCodec != null) {
            mediaCodec.flush();
            mediaCodec.stop();
            mediaCodec.release();
        }
        if (mMediaMuxer != null) {
            try {
                mMediaMuxer.stop();
                mMediaMuxer.release();
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "release: " + e.getMessage());
            }

        }
    }

    private boolean supportAvcCodec() {
        MediaCodecInfo codecInfo = null;
        for (int j = MediaCodecList.getCodecCount() - 1; j >= 0; j--) {
            codecInfo = MediaCodecList.getCodecInfoAt(j);

            String[] types = codecInfo.getSupportedTypes();
            for (int i = 0; i < types.length; i++) {
                if (types[i].equalsIgnoreCase(MediaFormat.MIMETYPE_VIDEO_AVC)) {
                    Log.d(TAG, "supportAvcCodec: " + types[i]);
                    // Find a color profile that the codec supports
                    MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType("video/avc");
                    Log.e(TAG, "length-" + capabilities.colorFormats.length + "==" + Arrays.toString(capabilities.colorFormats));

                    for (int k = 0; k < capabilities.colorFormats.length; k++) {

                        int format = capabilities.colorFormats[k];
                        if (format == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar ||
                                format == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar) {
                            if (format >= matchedFormat) {
                                matchedFormat = format;
                            }
                        }

                    }
                    return true;
                }
            }
        }
        return false;
    }


    private int calculateLength(int format, int bWidth, int bHeight) {
        return bWidth * bHeight * ImageFormat.getBitsPerPixel(format) / 8;
    }


    private void renderNv21ToImage(Image image, byte[] data, int bWidth, int bHeight) {
        final Image.Plane[] planes = image.getPlanes();
        int offset = 0;
        for (int plane = 0; plane < planes.length; ++plane) {
            final ByteBuffer buffer = planes[plane].getBuffer();
            final int rowStride = planes[plane].getRowStride();
            // Experimentally, U and V planes have |pixelStride| = 2, which
            // essentially means they are packed. That's silly, because we are
            // forced to unpack here.
            final int pixelStride = planes[plane].getPixelStride();
            final int planeWidth = (plane == 0) ? bWidth : bWidth / 2;
            final int planeHeight = (plane == 0) ? bHeight : bHeight / 2;
            byte[] rowData = new byte[rowStride];
            switch (plane) {
                case 0://y plane
                    Log.d(TAG, "renderNv21ToImage rowStride == planeWidth: ");
                    // Copy whole plane from buffer into |data| at once.
                    buffer.put(data, offset, planeWidth * planeHeight);
                    offset += planeWidth * planeHeight;
                    break;
                case 1://u plane
                    Log.d(TAG, "renderNv21ToImage plane: " + plane + " pixelStride:" + pixelStride + " rowStride:" + rowStride);
                    // Copy pixels one by one respecting pixelStride and rowStride.

                    for (int row = 0; row < planeHeight - 1; ++row) {
                        for (int col = 0; col < planeWidth; ++col) {
                            rowData[col * pixelStride] = data[offset + row * rowStride + col * 2 + 1];
                            rowData[col * pixelStride + 1] = data[offset + row * rowStride + col * 2];
                        }
                        buffer.put(rowData);
                    }
                    // Last row is special in some devices and may not contain the full
                    // |rowStride| bytes of data. See  http://crbug.com/458701  and
                    // http://developer.android.com/reference/android/media/Image.Plane.html#getBuffer()

                    for (int col = 0; col < planeWidth; ++col) {
                        rowData[col * pixelStride] = data[offset + rowStride * (planeHeight - 1) + col * 2 + 1];
                        rowData[col * pixelStride + 1] = data[offset + rowStride * (planeHeight - 1) + col * 2];

                    }
                    buffer.put(rowData, 0, Math.min(rowStride, buffer.remaining()));
                    break;
                case 2:///v plane
                    Log.d(TAG, "renderNv21ToImage plane: " + plane + " pixelStride:" + pixelStride + " rowStride:" + rowStride);
                    // Copy pixels one by one respecting pixelStride and rowStride.
                    for (int row = 0; row < planeHeight - 1; ++row) {

                        for (int col = 0; col < planeWidth; ++col) {
                            rowData[col * pixelStride] = data[offset + row * rowStride + col * 2];
                            rowData[col * pixelStride + 1] = data[offset + row * rowStride + col * 2 + 1];
                        }
                        buffer.put(rowData);
                    }
                    // Last row is special in some devices and may not contain the full
                    // |rowStride| bytes of data. See  http://crbug.com/458701  and
                    // http://developer.android.com/reference/android/media/Image.Plane.html#getBuffer()

                    for (int col = 0; col < planeWidth; ++col) {
                        rowData[col * pixelStride] = data[offset + rowStride * (planeHeight - 1) + col * 2];
                        rowData[col * pixelStride + 1] = data[offset + rowStride * (planeHeight - 1) + col * 2 + 1];
                    }
                    buffer.put(rowData, 0, Math.min(rowStride, buffer.remaining()));
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void start() {
        Log.d(TAG, "onStart: ");
        mediaCodec.start();
    }
}
