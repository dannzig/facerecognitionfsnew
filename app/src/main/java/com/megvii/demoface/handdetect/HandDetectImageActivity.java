package com.megvii.demoface.handdetect;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.HandImagePointView;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.HandDetectApi;
import com.megvii.facepp.multi.sdk.handskeleton.HandDetectConfig;
import com.megvii.facepp.multi.sdk.handskeleton.HandInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HandDetectImageActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.mHandImagePointView)
    HandImagePointView mHandImagePointView;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_hand_image;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("手部检测");
    }

    @Override
    protected void initData() {
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String path = ConUtil.getRealPathFromURI(this, uri);
        final Bitmap bitmap = ConUtil.getImage(path);
        mHandImagePointView.setBitmapAndResult(bitmap, null);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        HandDetectConfig config = HandDetectApi.getInstance().getConfig();
        config.setMinHandSize(100);
        HandDetectApi.getInstance().setConfig(config);
        mHandImagePointView.refreshView((int) (imageWidth * scale), (int) (imageHeight * scale));
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        final FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();

        new Thread(new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                final HandInfo[] handInfos = HandDetectApi.getInstance().detectHand(facePPImage);
                Log.w("timeCost", "1:" + (System.currentTimeMillis() - startTime));
                long detectCost = System.currentTimeMillis() - startTime;
                long detectPointCost = 0;
                long detectGestureConst = 0;
                if (handInfos != null) {
                    for (HandInfo handInfo : handInfos) {
                        startTime = System.currentTimeMillis();
                        HandDetectApi.getInstance().getHandPoint(handInfo);
                        detectPointCost = detectPointCost + (System.currentTimeMillis() - startTime);
                        HandDetectApi.getInstance().getHandRectInfo(handInfo);
                        startTime = System.currentTimeMillis();
                        HandDetectApi.getInstance().getGestureInfo(handInfo);
                        detectGestureConst = detectGestureConst + (System.currentTimeMillis() - startTime);
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("手部区域检测(ms)：");
                sb.append(detectCost);
                sb.append("\n");
                sb.append("手部骨骼点检测(ms)：");
                sb.append(detectPointCost);
                sb.append("\n");
                sb.append("手势检测(ms)：");
                sb.append(detectGestureConst);
                sb.append("\n");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mHandImagePointView.setBitmapAndResult(bitmap, handInfos);
                        tvDetectTips.setText(sb.toString());
                    }
                });

            }
        }).start();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }
}
