package com.megvii.demoface;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import mcv.facepass.FacePassException;
import mcv.facepass.types.FacePassAddFaceResult;


public class dbhelper {
    static String TAG = dbhelper.class.getSimpleName();
    static Context act;
    public static boolean loaded_db = false;
    public static SQLiteDatabase database = null;


    private ArrayList<String> name = new ArrayList<String>();
    private ArrayList<String> national = new ArrayList<String>();
    private ArrayList<String> phone = new ArrayList<String>();
    private ArrayList<String> id = new ArrayList<String>();
    private ArrayList<String> member_id = new ArrayList<String>();


    public dbhelper(Context act) {
        dbhelper.act = act;
        if (!loaded_db) {
            try {
                setup_db_ann();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SharedPreferences sharedPref = act.getApplicationContext().getSharedPreferences("com.capturesolutions.weightcapture", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("version_code", "" + getAppVersion(act.getApplicationContext()));
        editor.apply();


    }

    public void updateDBAppVersion(String version_code) {
        ContentValues ctx = new ContentValues();
        ctx.put("version_code", version_code);
        ctx.put("version_name", version_code);
        database.insert("app_version", null, ctx);
    }

    public void RunDBUpdates(Context act) {
        dbhelper.act = act;
        if (!loaded_db) {
            try {
                setup_db_ann();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public dbhelper(Context ctx, Activity act) {
        String dbpath = act.getExternalFilesDir(null).getAbsolutePath() + "/" + "xclgdjwc11hen.db";
        SQLiteDatabase.loadLibs(act);
        act = act;
        Log.v("gggggggg here ", "jjjjjjjjjj===== init");
        AESHelper aesHelper = new AESHelper(ctx);
        if (database == null)
            database = SQLiteDatabase.openOrCreateDatabase(dbpath, aesHelper.getkey2(), null);

    }

    public dbhelper(Context ctx, Context act) {
        String dbpath = act.getExternalFilesDir(null).getAbsolutePath() + "/" + "xclgdjwc11hen.db";
        SQLiteDatabase.loadLibs(act);
        Log.v("gggggggg here ", "jjjjjjjjjj===== init");
        AESHelper aesHelper = new AESHelper(ctx);
        if (database == null)
            database = SQLiteDatabase.openOrCreateDatabase(dbpath, aesHelper.getkey2(), null);

    }

    void setup_db_ann() {
        String dbpath = act.getExternalFilesDir(null).getAbsolutePath() + "/" + "xclgdjwc11hen.db";
        SQLiteDatabase.loadLibs(act);
        Log.v("gggggggg here ", "jjjjjjjjjj===== ");
        AESHelper aesHelper = new AESHelper(act);
        database = SQLiteDatabase.openOrCreateDatabase(dbpath, aesHelper.getkey2(), null);

        initdb();
        create_indexs();

    }

    public void create_indexs() {
        try {
            Log.e("atindex", "==========> ");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS checktype_id ON checkofftypes (_id)");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS search_indexs_inventory  ON TBL_inventory (supplier_id, receipt_no,center_id, session_no, tanktaskid, tapperNo, name);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS search_indexs_memberid  ON TBL_members (member_id);");

            database.execSQL("DELETE FROM cummulative_monthly");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS cummulative_indexs_memberid  ON cummulative_monthly (member_id);");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS polygonsearch  ON TBL_divisions_polygon (transaction_no);");
            database.execSQL("CREATE INDEX IF NOT EXISTS polygonsearchplygon_id  ON TBL_divisions_polygon (polygon_id);");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS farmers_ded_ass_index  ON farmers_deductions_association_table (sid);");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS county_sid  ON TBL_counties (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS district_sid  ON TBL_districts (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS town_sid  ON TBL_towns (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS nationality_sid  ON TBL_nationality (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS marital_sid  ON TBL_marital_status (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS gender_sid  ON TBL_gender (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS inspc_qns_sid  ON TBL_insp_questions (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS bank_branch_sid  ON TBL_bank_branches (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS block_sid  ON block_table (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS bank_sid  ON TBL_banks (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS rubber_clones_sid  ON TBL_rubber_clones (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS planting_types_sid  ON TBL_planting_types (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS deductions_sid  ON TBL_farmer_deductions (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS cpa_type_sid  ON cpa_types (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS cpa_commercial_name_sid  ON cpa_commercial_names (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS weather_type_sid  ON weather_types (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS intervention_type_sid  ON intervention_types_fc (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS tapping_system_sid  ON TBL_tapping_system (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS panel_position_sid  ON TBL_panel_position (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS training_topics_sid  ON TBL_training_topics (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS answer_type_sid  ON TBL_qnanswer_types (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS visit_reason_sid  ON TBL_visit_reasons (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS training_subtopics_sid  ON TBL_training_subtopics (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS mem_no  ON TBL_next_of_kin (farmer_member_no);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS education_level_sid  ON TBL_education_level (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS polygon_category_sid  ON TBL_polygon_category (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS crops_sid  ON TBL_crops (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS relations_sid  ON TBL_relations (sid);");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS block_tree_count_sid  ON blocks_tree_count (block_sid);");
            database.execSQL("DROP INDEX block_tree_count_sid;");

            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS damage_sid  ON damage_causes_fc (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS application_mthd_sid  ON application_mthds_fc (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS actions_reasons_sid  ON actions_reasons_fc (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS diseses_sid  ON diseases_fc (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS infra_sid  ON TBL_infrastructure_types (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS reserves_sid  ON TBL_reserves_list (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS farm_inputs_sid  ON farm_inputs_table (sid);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS dailymusterindex  ON dailyMuster (receipt_no);");
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS receipt_no_search_TBL_inv  ON TBL_inventory (receipt_no);");


//            database.execSQL("DROP INDEX block_sid;");
//            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS block_sid_new  ON block_table (sid);");

//            database.execSQL("DELETE FROM TBL_next_of_kin");
//            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS birth_cert_no  ON TBL_next_of_kin (birth_cert);");


        } catch (Exception e) {
            Log.e("this_index_exist", "==========> " + e.getMessage());
        }


    }

    public int runningDBVersion(Context ct) {
        int code = 0;
        try {
            code = ct.getSharedPreferences("com.capturesolutions.weightcapture", Context.MODE_PRIVATE).getInt("version_code", 0);
        } catch (Exception e) {
            code = 0;
        }
        return code;
    }

    public int getAppVersion(Context cont) {
        PackageInfo pinfo = null;
        try {
            pinfo = cont.getPackageManager().getPackageInfo(cont.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pinfo.versionCode;
    }

    public void initdb() {

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS vehicles (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,  vehicle_no VARCHAR,vehicle_category_id VARCHAR, owner VARCHAR,capacity VARCHAR,tare_weigh VARCHAR, vehicle_type VARCHAR, manufacture_year VARCHAR, nfc_tag VARCHAR, QR_code VARCHAR, IsActive VARCHAR, transacting_branch_id INTEGER, status VARCHAR, img VARCHAR, datecomparer VARCHAR, photo_front VARCHAR,sid VARCHAR,sync_datetime LONG )");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS stock_transactions (id INTEGER PRIMARY KEY  NOT NULL ,transaction_no VARCHAR UNIQUE DEFAULT (null),transaction_date VARCHAR,transaction_type VARCHAR,zone_Code VARCHAR,route_code VARCHAR,location_code VARCHAR,contact_type VARCHAR,contact_code VARCHAR,item_code VARCHAR,quantity_in VARCHAR DEFAULT (null) ,transaction_remarks VARCHAR,separator VARCHAR,session VARCHAR,status , quantity_out VARCHAR, date_out VARCHAR, truck_id VARCHAR, net_weight Double, user_id VARCHAR, weight_type VARCHAR, trasporter_name VARCHAR, grade_name VARCHAR, silo VARCHAR, bushel VARCHAR, moisture VARCHAR, farm_delivery_no VARCHAR, wc_ticket VARCHAR, grn_no VARCHAR, signature VARCHAR, guard_name VARCHAR, driver_name VARCHAR, driver_phone VARCHAR, more1 VARCHAR, more2 VARCHAR, more3 VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS weighbridgeorders (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid INTEGER, order_no VARCHAR, datetime VARCHAR, route VARCHAR, itemcode VARCHAR, more VARCHAR, datecomparer LONG, vehicle_no VARCHAR, vehicle_id VARCHAR, item_name VARCHAR, item_code VARCHAR, grade_name VARCHAR, grade_id VARCHAR, driver_name VARCHAR, transporter VARCHAR, destination VARCHAR, trailer VARCHAR, crop_stock VARCHAR, status VARCHAR, customer_name VARCHAR, customer_no VARCHAR, flight_no VARCHAR, pieces VARCHAR, agent_no VARCHAR, mawb VARCHAR,hawb VARCHAR, staff_no VARCHAR, scale_number VARCHAR, ship_to VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_members (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,member_id INTEGER UNIQUE DEFAULT (null) ,member_no VARCHAR DEFAULT (null) ,sub_account_id INTEGER DEFAULT (null) ,full_name VARCHAR DEFAULT (null) ,reg_date DATETIME DEFAULT (null) ,marital_status_id INTEGER DEFAULT (null) ,phone1 VARCHAR DEFAULT (null) ,phone2 VARCHAR DEFAULT (null) ,gender_type_id INTEGER DEFAULT (null) ,email VARCHAR DEFAULT (null) ,nat_id VARCHAR DEFAULT (null) ,membership_type_id INTEGER DEFAULT (null) ,branch_id INTEGER DEFAULT (null) ,address VARCHAR,postal_address VARCHAR DEFAULT (null) ,temp_member_no VARCHAR DEFAULT (null) ,date_of_birth DATETIME DEFAULT (null) , transacting_branch_id INTEGER, status VARCHAR, sync_datetime LONG, employee_id INTEGER DEFAULT (null), membership_sub_type_id INTEGER DEFAULT (null), nfc_id VARCHAR, route INTEGER, location VARCHAR, leafcenter VARCHAR DEFAULT (null), bankname VARCHAR DEFAULT (null), branch VARCHAR DEFAULT (null), acc_no VARCHAR DEFAULT (null), lrno VARCHAR DEFAULT (null), acres DOUBLE DEFAULT (null), monthlylimit INTEGER DEFAULT (null), is_active VARCHAR DEFAULT 1, acc_name VARCHAR, bank_details_id INTEGER, datenfc VARCHAR, pluckers_group_id INTEGER DEFAULT 0,department_id VARCHAR,main_category_code VARCHAR,is_roaming VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_vehicles (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,member_id INTEGER UNIQUE DEFAULT (null) ,member_no VARCHAR DEFAULT (null) ,sub_account_id INTEGER DEFAULT (null) ,full_name VARCHAR DEFAULT (null) ,reg_date DATETIME DEFAULT (null) ,marital_status_id INTEGER DEFAULT (null) ,phone1 VARCHAR DEFAULT (null) ,phone2 VARCHAR DEFAULT (null) ,gender_type_id INTEGER DEFAULT (null) ,email VARCHAR DEFAULT (null) ,nat_id VARCHAR DEFAULT (null) ,membership_type_id INTEGER DEFAULT (null) ,branch_id INTEGER DEFAULT (null) ,address VARCHAR,postal_address VARCHAR DEFAULT (null) ,temp_member_no VARCHAR DEFAULT (null) ,date_of_birth DATETIME DEFAULT (null) , transacting_branch_id INTEGER, status VARCHAR, sync_datetime LONG, employee_id INTEGER DEFAULT (null), membership_sub_type_id INTEGER DEFAULT (null), nfc_id VARCHAR, route INTEGER, location VARCHAR, leafcenter VARCHAR DEFAULT (null), bankname VARCHAR DEFAULT (null), branch VARCHAR DEFAULT (null), acc_no VARCHAR DEFAULT (null), lrno VARCHAR DEFAULT (null), acres DOUBLE DEFAULT (null), monthlylimit INTEGER DEFAULT (null), is_active VARCHAR DEFAULT 1, acc_name VARCHAR, bank_details_id INTEGER)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS users (_id INTEGER PRIMARY KEY  NOT NULL ,username VARCHAR,password VARCHAR,agent_id INTEGER,status_id VARCHAR DEFAULT (null) ,sync_datetime LONG,email_address VARCHAR,phone_no VARCHAR,account_id INTEGER,employee_id INTEGER,account_name VARCHAR,rid INTEGER UNIQUE , branch VARCHAR, branch_id INTEGER, name VARCHAR, is_active VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS user_route_assignment (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid INTEGER,user_id VARCHAR, username VARCHAR, route_id VARCHAR, routename VARCHAR, address VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS logs (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , user_id INTEGER, sync_datetime LONG DEFAULT CURRENT_TIMESTAMP, description VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS scale_settings (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , scale_name VARCHAR,scale_address VARCHAR,scale_type VARCHAR,status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS printer_settings (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , printer_name VARCHAR,printer_address VARCHAR,status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS is_new_scale_password_protected (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, yes_no  VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_supplier_account (\"_id\" INTEGER PRIMARY KEY  NOT NULL ,\"rid\" INTEGER,\"member_id\" INTEGER,\"account_id\" INTEGER,\"ref_id\" INTEGER  UNIQUE DEFAULT (null) ,\"acc_name\" VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_supplier_land_details (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, member_id VARCHAR, datecomparer VARCHAR, lr_number VARCHAR, no_of_trees VARCHAR, production_land VARCHAR,size VARCHAR, row_no VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS  finger_prints (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, member_id VARCHAR, finger_id VARCHAR, finger_print VARCHAR, date_time VARCHAR, status VARCHAR, server_id VARCHAR, bytess VARCHAR DEFAULT 0, bytes_length VARCHAR DEFAULT 0, member_no VARCHAR DEFAULT 0, datecomparer LONG DEFAULT 0, favourite INTEGER DEFAULT 0, is_active VARCHAR, more VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS  \"TBL_centres\" (\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"name\" VARCHAR, \"route_id\" INTEGER, \"rate\" DOUBLE, \"sync_datetime\" LONG, \"t_id\" INTEGER)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_inventory (_id INTEGER PRIMARY KEY NOT NULL, inventory_item_type_id INTEGER, unit_of_measure_id INTEGER, make_model_id INTEGER, net DOUBLE DEFAULT (NULL), gross DOUBLE, tare DOUBLE, rid INTEGER, name VARCHAR, status INTEGER, t_id INTEGER, supplier_id INTEGER, sync_datetime LONG, printed INTEGER, moisture_level DOUBLE, route_code INTEGER DEFAULT (0), mode_of_weighment_id VARCHAR DEFAULT 1, scale_address VARCHAR DEFAULT 1, incoming Double DEFAULT 0, outgoing Double DEFAULT 0, receipt_no VARCHAR, latitude VARCHAR, longitude VARCHAR, center_id INTEGER, no_weighments INTEGER, tranType VARCHAR, separator VARCHAR, member_id_field VARCHAR, plucker VARCHAR DEFAULT 0, Plucker_Cost_Id VARCHAR DEFAULT 1, session_no VARCHAR, mode_of_identification_id VARCHAR, tag_no VARCHAR, vehicle_id VARCHAR, instant_backup VARCHAR, daily_backup VARCHAR, back_update VARCHAR, more1 VARCHAR, more2 VARCHAR,soilcheck VARCHAR,tanktaskid VARCHAR,taskname VARCHAR,itemId VARCHAR,contractorId VARCHAR,description VARCHAR,typeOfDay VARCHAR,tankId VARCHAR,tapperNo VARCHAR,farmerNo VARCHAR,d_date VARCHAR,d_datetime VARCHAR,userId VARCHAR,total_amount VARCHAR,unit_price VARCHAR)");
        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS  route_users (\"id\" INTEGER PRIMARY KEY  NOT NULL ,\"_id\" VARCHAR,\"username\" VARCHAR,\"phone_number\" VARCHAR,\"account_id\" VARCHAR)");
        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS TBL_plucker_types (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, type_id VARCHAR, name VARCHAR, farmer_plucker_cost VARCHAR, company_plucker_cost VARCHAR, sync_datetime VARCHAR, active VARCHAR DEFAULT 0, type INTEGER DEFAULT 0, status VARCHAR, serial VARCHAR, brand VARCHAR, fuelcapacity VARCHAR, condition VARCHAR, nfctag_no VARCHAR, other VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_sessions (\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sync_date\" LONG UNIQUE , \"user_id\" INTEGER, \"tid\" INTEGER UNIQUE , \"status\" INTEGER DEFAULT 0, \"t_code\" VARCHAR, \"t_code2\" VARCHAR, \"sync_date2\" LONG, session VARCHAR, member_id INTEGER DEFAULT 0, vehicle_id INTEGER DEFAULT 0, tankId VARCHAR,forwardingOrderNo VARCHAR,total_weight VARCHAR,rubberSource VARCHAR,d_date VARCHAR,more1 VARCHAR,tranType VARCHAR,farmRouteId VARCHAR, divisionId VARCHAR,subforwadingorder VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS inventory_items_Main (\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"rid\" INTEGER UNIQUE , \"name\" VARCHAR, \"sync_datetime\" LONG, \"active\" INTEGER)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS company_details (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, company_id VARCHAR, company_name VARCHAR, address VARCHAR,mobile VARCHAR,phone VARCHAR,email VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS  TBL_active_route (\"_id\" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,\"code\" VARCHAR UNIQUE,\"rate\" DOUBLE,\"sync_datetime\" LONG,\"active\" INTEGER, transportcost Double DEFAULT 1)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS centers (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , center_id INTEGER,center_name VARCHAR,route_id INTEGER, active INTEGER DEFAULT 0, sync_datetime LONG,tagNo VARCHAR,code VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_all_inv_dtls (\"_id\" INTEGER PRIMARY KEY  NOT NULL ,\"rid\" INTEGER UNIQUE,\"inv_itemtype_id\" INTEGER,\"make_model_id\" INTEGER,\"inventory_id\" INTEGER,\"bp\" DOUBLE,\"transportcost\" DOUBLE,\"moisture\" DOUBLE,\"reduction_weight\" DOUBLE,\"weight_per_reduction\" DOUBLE,\"capacity\" VARCHAR,\"inv_name\" VARCHAR DEFAULT (null) ,\"routename\" VARCHAR,\"make_model_name\" VARCHAR,\"isactive\" BOOL, \"gridcollection_id\" INTEGER, \"inventory_make_id\" INTEGER, \"inventory_make_name\" VARCHAR, istied INTEGER DEFAULT 1, blockId VARCHAR DEFAULT 0, blockname INTEGER DEFAULT 0, others INTEGER DEFAULT 0, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS inventory_items (\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"rid\" INTEGER UNIQUE , \"name\" VARCHAR, \"sync_datetime\" LONG, \"active\" INTEGER, blockId VARCHAR DEFAULT 0, blockname INTEGER DEFAULT 0, others INTEGER DEFAULT 0, datecomparer LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS blocks_zones (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid INTEGER, name VARCHAR, isactive VARCHAR DEFAULT 1, more VARCHAR, sync_datetime LONG,division_code VARCHAR,rubberSource VARCHAR,details VARCHAR,head VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS route_users (\"id\" INTEGER PRIMARY KEY NOT NULL ,\"_id\" VARCHAR,\"username\" VARCHAR,\"phone_number\" VARCHAR,\"account_id\" VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS plucker_groups (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, group_id VARCHAR, group_no VARCHAR, name VARCHAR, tel_no VARCHAR, registration_date VARCHAR, employeeId VARCHAR, divisionId VARCHAR, active INTEGER DEFAULT 0, contractorId VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS cummulative_monthly (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , member_id VARCHAR, member_no VARCHAR, cummulative_weight VARCHAR, datetime VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS checkofftypes (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , _id VARCHAR, name VARCHAR, datecomparer VARCHAR, sync_datetime VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS checkoffdetails (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , deductionTypeId VARCHAR, memberId VARCHAR, totalAmount VARCHAR, transaction_no VARCHAR, checkOffdate VARCHAR,status VARCHAR,others VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS item_products (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , itemId VARCHAR, name VARCHAR, description VARCHAR, status VARCHAR, more VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS item_varieties (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , itemId VARCHAR, varietyId VARCHAR, name VARCHAR, description VARCHAR, status VARCHAR, buyingprice VARCHAR, deduction VARCHAR, sync_datetime LONG)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS weighbridge_items (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, itemId VARCHAR, weighbridge_item_code VARCHAR, name VARCHAR, main_item_id VARCHAR, status VARCHAR, main_item_code VARCHAR, main_item_name VARCHAR, rubber_source_id VARCHAR, sync_datetime LONG)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_inspection_wo (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, wo_number VARCHAR, transaction_no VARCHAR, vehicle_type VARCHAR, vehicle_no VARCHAR, mileage VARCHAR, hours VARCHAR, driver_name VARCHAR, department VARCHAR, account VARCHAR, reg VARCHAR, inspector VARCHAR, supervisor VARCHAR, signature VARCHAR, date_in VARCHAR, time_in VARCHAR, date_out VARCHAR, time_out VARCHAR, sync_datetime LONG, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_repair_requests (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, wo_number VARCHAR, transaction_no VARCHAR, repair_request VARCHAR, question_answered_id VARCHAR, status VARCHAR, comment VARCHAR, sync_datetime LONG, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");

        // firestone
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS tankTasks (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, taskId VARCHAR,code VARCHAR, tagno VARCHAR, taskname VARCHAR, tankId VARCHkAR, numberoftrees VARCHAR, status VARCHAR, variety VARCHAR, clone VARCHAR, memberId VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS collectionpointdistpatch (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, collectionCenterId VARCHAR, totalweight VARCHAR, fieldsessionNo VARCHAR, contractorId VARCHAR, status VARCHAR, vehicleID VARCHAR, itemId VARCHAR, clerkId VARCHAR, datetime VARCHAR,batchNo VARHCAR, forwardingOrderNo VARCHAR, typeOfRubber VARCHAR, status2 VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS forwardingorders (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, totalweight VARCHAR, status VARCHAR, vehicleID VARCHAR, clerkId VARCHAR, datetime VARCHAR, forwardingOrderNo VARCHAR, supritedantId VARCHAR, sourceOfRubber VARCHAR, status2 VARCHAR, ppd VARCHAR, ppd_signature VARCHAR, cartersTimeIn VARCHAR, chartersTimeOut VARCHAR,driver_code VARCHAR,item VARCHAR,location VARCHAR,vehicle_code VARCHAR,contractor_code VARCHAR,rubber_source_code VARCHAR,driver VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS contractorDetails (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, name VARCHAR, contractId VARCHAR,  ContNo VARCHAR, more2 VARCHAR, sync_status VARCHAR, sync_datetime LONG, active VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS rubberSources (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sourceId VARCHAR, name VARCHAR, sync_datetime LONG,code VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qualityinspectiondetails (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sourceId VARCHAR, name VARCHAR, sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS inspection_questions (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, name VARCHAR, level VARCHAR, is_multiple_choice VARCHAR,status VARCHAR,category VARCHAR, identifier_code VARCHAR, anstype VARCHAR, datatype VARCHAR, more1 VARCHAR, sync_status VARCHAR, sync_datetime LONG,is_compulsory VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS day_type (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, tyepId VARCHAR, name VARCHAR, symbol VARCHAR, status VARCHAR,sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS dailyMuster (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sessionNo VARCHAR, memberId VARCHAR, memberNo VARCHAR, dayType VARCHAR, dayTypeNo VARCHAR,date VARCHAR, datetimeIn VARCHAR,datetimeOut VARCHAR, taskId VARCHAR, taskNo VARCHAR, tankId VARCHAR, divisionId VARCHAR, verificationDateTime VARCHAR, status VARCHAR, statusVerification VARCHAR, verificationtime VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS sessionTasks (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sessionId VARCHAR, sessionNo VARCHAR, taskId VARCHAR, taskNo VARCHAR, tankId VARCHAR, status VARCHAR,tapperId VARCHAR,tapperNo VARCHAR, date_ VARCHAR,isAdditional VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS subforwardingorders (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, totalweight VARCHAR, status VARCHAR, vehicleID VARCHAR, clerkId VARCHAR, datetime VARCHAR, subforwardingOrderNo VARCHAR, supritedantId VARCHAR, sourceOfRubber VARCHAR, status2 VARCHAR,  division VARCHAR, driver VARCHAR, separator VARCHAR, forwadingOrder VARCHAR,more VARCHAR,more2 VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS forwardingordersMerge (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, totalweight VARCHAR, status VARCHAR, vehicleID VARCHAR, clerkId VARCHAR, datetime VARCHAR, forwardingOrderNo VARCHAR, supritedantId VARCHAR, sourceOfRubber VARCHAR, status2 VARCHAR, ppd VARCHAR, ppd_signature VARCHAR, cartersTimeIn VARCHAR, chartersTimeOut VARCHAR, driver VARCHAR, driver_code VARCHAR, item VARCHAR, location VARCHAR, vehicle_code VARCHAR, contractor_code VARCHAR, rubber_source_code VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS drivers (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sid VARCHAR, driver_no VARCHAR, name VARCHAR, contractor VARCHAR, more VARCHAR, status VARCHAR,sync_datetime LONG)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_answers (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,forwarding_no VARCHAR, question_id VARCHAR, is_checked VARCHAR, description VARCHAR,identifier_code VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')), sync_status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_verdict (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,forwarding_no VARCHAR, is_proceed VARCHAR, source_id VARCHAR, comments VARCHAR,identifier_code VARCHAR,weight_rejected VARCHAR,car_reg VARCHAR,user_id VARCHAR,to_weighbridge VARCHAR, partial_rejection VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')), sync_status VARCHAR,driver_name VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_item_percentage (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,forwarding_no VARCHAR, item_id VARCHAR, item_percentage VARCHAR,identifier_code VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')), sync_status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_questions_choices (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, question_id VARCHAR, choice_name VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_datetime LONG)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_central_weights (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, forwarding_no VARCHAR, car_reg VARCHAR, miller_user_id VARCHAR,lab_user_id VARCHAR, milling_time VARCHAR,wet_weight_miling VARCHAR, dry_weight_miling VARCHAR,product_tested_id VARCHAR, pre_oven_time VARCHAR,post_oven_time VARCHAR,is_revision VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_status VARCHAR,post_milling_time VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_lab_weights (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, forwarding_no VARCHAR,  cut_piece_id VARCHAR,wet_wgt VARCHAR, dry_wgt VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),is_revision VARCHAR,sync_status VARCHAR,transaction_no VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_factory_shifts (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, session_no VARCHAR,  shift_no VARCHAR, drier_id VARCHAR, pre_script VARCHAR, d_date VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')),destination_id VARCHAR,metal_detector_id VARCHAR,grade_id VARCHAR,user_id VARCHAR,thermometer_no VARCHAR,is_done VARCHAR,status VARCHAR)");


        database.execSQL("CREATE TABLE IF NOT EXISTS qc_factory_crates (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, transaction_no VARCHAR, session_no VARCHAR, crate_no VARCHAR, drier_id VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),destination_id VARCHAR,thermometer_no VARCHAR," +
                "shift_id VARCHAR, inc_no VARCHAR,  bale_count VARCHAR, packer_id VARCHAR, weigher_id VARCHAR,metal_detector_id VARCHAR,grade_id VARCHAR,user_id VARCHAR,comments VARCHAR,others VARCHAR,sync_status VARCHAR,prev_crate_no VARCHAR,is_revision VARCHAR DEFAULT '0',technician_id VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS qc_all_tests (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sid VARCHAR, test_name VARCHAR,  reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS qc_test_procedure (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sid VARCHAR, test_id VARCHAR,procedure_name VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_test_inputs (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sid VARCHAR, name VARCHAR,identifier VARCHAR,procedure_id VARCHAR,  reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_lab_results (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,crate_no VARCHAR, test_id VARCHAR,input_id VARCHAR,value_input VARCHAR,procedure_id VARCHAR,identifier VARCHAR,input_name VARCHAR, transaction_no VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_status VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_lab_entry (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,crate_no VARCHAR, test_id VARCHAR,grade_id VARCHAR,transaction_no VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),user_id VARCHAR,sync_status VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS qc_product_results (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,sid VARCHAR, crate_no VARCHAR, test_id VARCHAR,test_date VARCHAR,test_result VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS qc_factory_settings (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, item_code VARCHAR,  item_name VARCHAR, identifier VARCHAR, sid VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS qc_scales_weights (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, serial_no VARCHAR,  max_weight VARCHAR,name VARCHAR,location VARCHAR, identifier VARCHAR, sid VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')),sync_var VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_scale_review (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, scale_id VARCHAR, remark VARCHAR,sync_status VARCHAR, transaction_no VARCHAR, user_id VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')))");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS qc_scale_review_results (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,_id VARCHAR, scale_id VARCHAR,weight_id VARCHAR,weight_name VARCHAR, weight, resultant_weight VARCHAR,transaction_no VARCHAR,sync_status VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')))");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS tbl_nfc_data (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, forwarding_no VARCHAR, identifier_code VARCHAR, nfc_text VARCHAR,reg_time DATETIME  DEFAULT (datetime('now','localtime')))");

        // firestone timecapture
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS block_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, division_id VARCHAR, block_name VARCHAR, block_code VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS activity_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, unit_of_measure_id VARCHAR, activity_name VARCHAR, activity_code VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')),activity_type VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS unit_of_measure_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, uom_name VARCHAR, uom_code VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS camera_info_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, device_key VARCHAR, time VARCHAR, ip_address VARCHAR, person_count VARCHAR, face_count VARCHAR, app_version VARCHAR, is_for_clockin VARCHAR, is_for_clockout VARCHAR, department_assigned VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS camera_verifications_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, standard VARCHAR, trans_no VARCHAR, search_score VARCHAR, ip_address VARCHAR, device_key VARCHAR, verification_type VARCHAR, verification_photo VARCHAR, temperature_value VARCHAR, member_no VARCHAR, time VARCHAR, temperature_state VARCHAR, liveness_score VARCHAR, mask_value VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS departments_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, department_name VARCHAR, department_code VARCHAR, category VARCHAR, division_id VARCHAR, sub_group VARCHAR, main_group VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS member_category_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, category_name VARCHAR, category_code VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS member_subcategory_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, subcategory_name VARCHAR, subcategory_code VARCHAR, category_id VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS daily_muster_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, member_no VARCHAR, date_time VARCHAR, clock_in_time VARCHAR, clock_in_lati VARCHAR, clock_in_longi VARCHAR, clock_out_time VARCHAR, clock_out_lati VARCHAR, clock_out_longi VARCHAR, user_id VARCHAR, activity_code VARCHAR, block_code VARCHAR, division_code VARCHAR, day_type_code VARCHAR, points_remarks VARCHAR, transaction_no VARCHAR, block_remarks VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')), clockin_type VARCHAR, clockout_type VARCHAR, comment_onbehalf_clockin VARCHAR, other_location VARCHAR, other_activity VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS member_data_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, data VARCHAR, data_type VARCHAR, data_index VARCHAR, data_format VARCHAR, data_storage_mode VARCHAR, member_no VARCHAR, tracker_status VARCHAR, user_id VARCHAR, data_usage_frequency VARCHAR, transaction_no VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS work_order_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, transaction_no VARCHAR, inspector_id VARCHAR, engineer_id VARCHAR, workorder_status VARCHAR, raised_date VARCHAR, department_created_id VARCHAR, department_assigned_id VARCHAR, div_assigned VARCHAR, probles_desc VARCHAR, datecomparer VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')), inspector_name VARCHAR, server_id VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS inspection_details_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, inspcode VARCHAR, username VARCHAR, transaction_no VARCHAR, imgBase64 VARCHAR, no_of_workers VARCHAR, est_days VARCHAR, part_of_unit VARCHAR, defective_part VARCHAR, length VARCHAR, width VARCHAR, height VARCHAR, existing_condition VARCHAR, proposed_action VARCHAR, task_lati VARCHAR, task_longi VARCHAR, datecomparer VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')),inspector_name VARCHAR,server_id VARCHAR,remarks VARCHAR, other VARCHAR, other1 VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS work_order_attendance_tc (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, member_no VARCHAR, attendance_date VARCHAR, user_id VARCHAR, remarks VARCHAR, workorder_no VARCHAR, task_lat VARCHAR, task_long VARCHAR, clock_in_time VARCHAR, clock_out_time VARCHAR, clockin_type VARCHAR, clockout_type VARCHAR, comment_onbehalf_clockin VARCHAR, activity_code VARCHAR, block_code VARCHAR, division_code VARCHAR, day_type_code VARCHAR, points_remarks VARCHAR, transaction_no VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, other_location VARCHAR, other_activity VARCHAR, reg_time DATETIME  DEFAULT (datetime('now','localtime')))");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS inspection_code_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, datecomparer VARCHAR, name VARCHAR, code VARCHAR, status VARCHAR, sync_status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS sessions_table_tc (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, session_no VARCHAR, date_today VARCHAR, sync_status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_aggregates (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, date_time VARCHAR, no_of_workers VARCHAR, task_code VARCHAR, session_no VARCHAR, headman_username VARCHAR, headman_agg_rate VARCHAR, headman_agg_value VARCHAR, overseer_username VARCHAR, overseer_agg_rate VARCHAR, overseer_agg_value VARCHAR, superintendened_username VARCHAR, superintendened_agg_rate VARCHAR, task_block_code VARCHAR, task_div_code VARCHAR, other_one VARCHAR, other_two VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS div_blocks_tc (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, date_time VARCHAR, division VARCHAR, block VARCHAR)");
        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS headman_report (_id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,id VARCHAR, laborer_no VARCHAR, clockin VARCHAR,clockout VARCHAR,attendance_date VARCHAR,section_code VARCHAR,block_code VARCHAR,division_code VARCHAR,activity_code VARCHAR,other_activity_code VARCHAR,additional_activity_code VARCHAR,additional_hrs_worked VARCHAR,headman_no_incharge VARCHAR)");

        // TC

        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS checklistitems (_id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,id INTEGER, datecomparer VARCHAR, questions VARCHAR,category VARCHAR,status VARCHAR DEFAULT 0)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS gates(_id INTEGER  PRIMARY KEY  AUTOINCREMENT NOT NULL,id VARCHAR,category VARCHAR,gate_name VARCHAR,transacting_branch_id VARCHAR,zone VARCHAR,status VARCHAR DEFAULT 0,datecomparer VARCHAR,gatget_location_code VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS visit_codes(_id INTEGER  PRIMARY KEY  AUTOINCREMENT NOT NULL,id VARCHAR,code_no VARCHAR,farmer_id VARCHAR,farmer_name VARCHAR,date_time VARCHAR,telephone VARCHAR ,status VARCHAR,location VARCHAR,transaction_no varchar,user_id varchar)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS farmer_access_code(_id INTEGER  PRIMARY KEY  AUTOINCREMENT NOT NULL,id VARCHAR,access_code VARCHAR,farmer_id VARCHAR,gen_date_time VARCHAR,user_id VARCHAR ,end_time VARCHAR,datecomparer VARCHAR,transaction_no varchar,state varchar)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS vehicle_gatepass_codes(_id INTEGER  PRIMARY KEY  AUTOINCREMENT NOT NULL,id VARCHAR,gate_pass_no VARCHAR,vehicle_no VARCHAR,vehicle_id VARCHAR,driver_id VARCHAR ,destination_name VARCHAR,is_perimeter VARCHAR,no_of_trips varchar,bal_trips varchar,date_of_issue VARCHAR,datecomparer VARCHAR,start_date VARCHAR,end_date VARCHART,reason VARCHAR,approved_by VARCHAR,approval_status VARCHAR)");
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS visitations(_id INTEGER  PRIMARY KEY  AUTOINCREMENT NOT NULL,entry_no VARCHAR,category_employee_visit_code VARCHAR,in_out VARCHAR,visit_date VARCHAR,entry_date VARCHAR,departure_date VARCHAR,vehicle_registration_id VARCHAR,clock_out_user_id VARCHAR,clock_in_user_id VARCHAR,gate_in_id VARCHAR,gate_out_id VARCHAR,driver_name VARCHAR,forwarding_order_no VARCHAR,rubber_source_id VARCHAR,quantity_on_truck_descr VARCHAR,reception_date_time VARCHAR,vehicle_no VARCHAR,photo_front VARCHAR,QR_code VARCHAR,nfc_tag VARCHAR,color VARCHAR,gate_pass_no VARCHAR,gate_pass_id VARCHAR,status VARCHAR,driver_id VARCHAR,visit_purpose VARCHAR,no_farmers VARCHAR,combine_code VARCHAR,todat_date VARCHAR,entry_point VARCHAR,visitorTel_no VARCHAR,visitorGender VARCHAR, visitorEmail_address VARCHAR)");
        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS rubber_levels (_id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,id INTEGER, name VARCHAR,code VARCHAR, datecomparer VARCHAR,status VARCHAR DEFAULT 0)");

        database.execSQL(
                "CREATE TABLE  IF NOT EXISTS system_modules (_id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,id INTEGER, system VARCHAR,yesno VARCHAR, datecomparer VARCHAR,status VARCHAR)");

        // Farm Capture

        //sid,code,division_name,sync_datetime

        //FROM scale_settings where scale_type.
        //database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS your_unique_index ON TBL_inventory(receipt_no);");

        database.execSQL("CREATE TABLE IF NOT EXISTS item_products_deductions (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, itemId VARCHAR, name VARCHAR, description VARCHAR, status VARCHAR, more VARCHAR, sync_datetime LONG, unitCode VARCHAR, rate_type_id VARCHAR, rate_type VARCHAR, category VARCHAR, main_category_id VARCHAR, is_compulsory VARCHAR, rate VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS app_version  (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, version_code VARCHAR, version_name VARCHAR, datetime VARCHAR, status VARCHAR)");


        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_divisions_polygon_cache (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR,  division_id VARCHAR, polygon_id VARCHAR, longi VARCHAR, latitude VARCHAR, new_dev_block_id VARCHAR, transaction_no VARCHAR, datecomparer VARCHAR, sync_status VARCHAR)");

        database.execSQL(
                "CREATE TABLE IF NOT EXISTS TBL_testweight (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR,  user VARCHAR, d_date VARCHAR, datetime VARCHAR, collection_center VARCHAR, weight VARCHAR, printerd VARCHAR, transaction_no VARCHAR, datecomparer VARCHAR, more VARCHAR, session VARCHAR, scale_address VARCHAR, sync_status VARCHAR)");

        database.execSQL("CREATE TABLE IF NOT EXISTS farmer_plots_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, farmer_id VARCHAR, farmer_no VARCHAR, lr_id VARCHAR, lr_number VARCHAR, plot_no VARCHAR, plot_id VARCHAR, is_active VARCHAR, sync_status VARCHAR, sync_var LONG, reg_time DATETIME  DEFAULT (datetime('now','localtime')), planted_trees VARCHAR, tapping_system_Name VARCHAR, tapping_system_id VARCHAR, rubber_clones_Name VARCHAR, type_of_plantingname VARCHAR, no_of_Trees_in_the_block VARCHAR, stand_per_acre VARCHAR, panel_position_id VARCHAR, panel_position_Name VARCHAR, category_separator VARCHAR DEFAULT 'None', planted_year VARCHAR, type_of_planting_id VARCHAR, spacing VARCHAR, rubber_clones_id VARCHAR, area_acre VARCHAR, year_of_opening VARCHAR, no_tapping_tasks VARCHAR, tapping_frequency VARCHAR, tapping_frequency_id VARCHAR, main_division_id VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS collections_center_pricing (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, center_id VARCHAR, transport_cost VARCHAR, status VARCHAR, sync_var LONG)");

        database.execSQL("CREATE TABLE IF NOT EXISTS farmers_deductions_association_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR UNIQUE, farmer_id VARCHAR, is_active VARCHAR, farmer_deductions_item_id VARCHAR, more VARCHAR, itemname VARCHAR, datecomparer LONG)");

        database.execSQL("CREATE TABLE IF NOT EXISTS tare_recordings (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR, farmer_id VARCHAR, datetime VARCHAR, weight VARCHAR, more VARCHAR, status VARCHAR, station VARCHAR, scale VARCHAR, clerk VARCHAR, session VARCHAR)");
        database.execSQL("CREATE TABLE IF NOT EXISTS rubberSourceLocations (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR UNIQUE, code VARCHAR, name VARCHAR, category_separator VARCHAR, status VARCHAR, datecomparer LONG)");
        database.execSQL("CREATE TABLE IF NOT EXISTS contractor_divisions (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, sid VARCHAR UNIQUE, div_id VARCHAR, contractor_id VARCHAR, item_id VARCHAR, status VARCHAR, datecomparer LONG)");

        database.execSQL("CREATE TABLE IF NOT EXISTS session_tasks_transfer_table (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, session_no VARCHAR, tapper VARCHAR, tanktaskid VARCHAR, more VARCHAR, status VARCHAR, datetime VARCHAR)");


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(face_template) FROM member_data_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE member_data_table ADD COLUMN face_template BLOB");
            Log.e(TAG, " initdb : Creating column face_token successful");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(face_token) FROM member_data_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE member_data_table ADD COLUMN face_token VARCHAR");
            Log.e(TAG, " initdb : Creating column face_token successful");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(session_no) FROM camera_verifications_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE camera_verifications_table ADD COLUMN session_no VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(session_no) FROM work_order_attendance_tc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE work_order_attendance_tc ADD COLUMN session_no VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(verification_mode) FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN verification_mode VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(verification_mode) FROM camera_verifications_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE camera_verifications_table ADD COLUMN verification_mode VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(data_source) FROM member_data_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE member_data_table ADD COLUMN data_source VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_new) FROM farmers_deductions_association_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmers_deductions_association_table ADD COLUMN is_new VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(technician_2) FROM qc_factory_crates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE qc_factory_crates ADD COLUMN technician_2 VARCHAR");
            database.execSQL("ALTER TABLE qc_factory_crates ADD COLUMN technician_3 VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(active_status) FROM inventory_items", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE inventory_items ADD COLUMN active_status VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(receipt_no) FROM dailyMuster", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE dailyMuster ADD COLUMN receipt_no VARCHAR");
            database.execSQL("ALTER TABLE dailyMuster ADD COLUMN is_updated VARCHAR DEFAULT 0");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transport_rate) FROM Osrp_collectionSummary", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN transport_rate VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN transport_amount VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN driver_name VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN vehicle_no VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN forwardingOrderNo VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN description VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN printstatus VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(collectionCenter) FROM Osrp_collectionSummary", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN collectionCenter VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN total_lb_weight VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(near_division_sid) FROM surveys_fc_answers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN near_division_sid VARCHAR");
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN near_division_name VARCHAR");
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN near_division_code VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(headm_id) FROM tapping_inspection_trans_no", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE tapping_inspection_trans_no ADD COLUMN headm_id VARCHAR");
            database.execSQL("ALTER TABLE tapping_inspection_trans_no ADD COLUMN overs_id VARCHAR");
            database.execSQL("ALTER TABLE tapping_inspection_trans_no ADD COLUMN insp_round VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(clock_type) FROM camera_verifications_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE camera_verifications_table ADD COLUMN clock_type VARCHAR");
            database.execSQL("ALTER TABLE camera_verifications_table ADD COLUMN clock_value VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_active) FROM TBL_insp_questions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_insp_questions ADD COLUMN is_active VARCHAR");
        }

        try {

            Cursor cursor1 = database.rawQuery("SELECT count(itemId) FROM checkoffdetails", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN itemId VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN memberNo VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN userId VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN unitPrice VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN sessionNo VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN ddate VARCHAR");
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN separator VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_name) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN plot_name VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(ownership_type) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN ownership_type VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(contractor_code) FROM subforwardingorders", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN contractor_code VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN is_forwarded VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN creation_date VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN forwarding_date VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(map_category) FROM TBL_reserves_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_reserves_polygon ADD COLUMN map_category VARCHAR");
            database.execSQL("ALTER TABLE TBL_reserves_polygon ADD COLUMN land_details_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_reserves_polygon ADD COLUMN is_farmer_or_estate VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_name) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN plot_name VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_sid) FROM tapping_inspection_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE tapping_inspection_answers_fc ADD COLUMN farm_sid VARCHAR");
            database.execSQL("ALTER TABLE tapping_inspection_answers_fc ADD COLUMN tank_sid VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(overseer_name) FROM tapping_inspection_details_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE tapping_inspection_details_fc ADD COLUMN overseer_name VARCHAR");
            database.execSQL("ALTER TABLE tapping_inspection_details_fc ADD COLUMN overseer_member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(reg_time) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN reg_time VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM interventions_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN member_no VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM ipm_photos", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_photos ADD COLUMN member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(datecomparer_farmer) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN datecomparer_farmer VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(coordinate_type) FROM TBL_infrastructure_types", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_types ADD COLUMN coordinate_type VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(date_today) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN date_today VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_id) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN plot_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_id) FROM damage_count_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN plot_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM ipm_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM girth_measurement", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_plots_id) FROM girth_measurement", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN farm_plots_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(landsize) FROM TBL_infrastructure_mapping", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_mapping ADD COLUMN landsize VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(location_category_id) FROM TBL_infrastructure_mapping", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_mapping ADD COLUMN location_category_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_infrastructure_mapping ADD COLUMN entry_date VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(status) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN status VARCHAR");
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN signature VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(comments) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN comments VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(mainAlpCategory) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN mainAlpCategory VARCHAR");
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN date_closed VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(education_level) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN education_level VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(new_farmer_status) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN new_farmer_status VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(comments) FROM surveys_fc_answers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN comments VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(answ_option) FROM surveys_fc_answers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN answ_option VARCHAR");
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN activity_status VARCHAR");
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN estimated_acreage VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_inspector_visit_No) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN farm_inspector_visit_No VARCHAR");
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN polygon_category_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(polygon_id) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN polygon_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(my_nationality) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN my_nationality VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN payee VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN user_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM alp_quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE alp_quizes_answers_fc ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM child_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE child_chosen_fc ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM action_plan_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE action_plan_chosen_fc ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM reasons_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE reasons_chosen_fc ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM surveys_fc_answers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE surveys_fc_answers ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM training_fc_photos", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE training_fc_photos ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM coaching_main", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sid) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN sid VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM damage_count_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM ipm_photos", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_photos ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM girth_measurement", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM ipm_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN nat_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM interventions_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(coordinate_unique_no) FROM surveys_fc_coordinates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE surveys_fc_coordinates ADD COLUMN coordinate_unique_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(occupation) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN occupation VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM farmer_photo_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_photo_table ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM farmer_deductions_added", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_deductions_added ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM farm_managers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farm_managers_fc ADD COLUMN nat_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(status) FROM TBL_relations", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_relations ADD COLUMN status VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(registration_date) FROM farm_managers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farm_managers_fc ADD COLUMN registration_date VARCHAR");
            database.execSQL("ALTER TABLE farm_managers_fc ADD COLUMN user_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farmer_name) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN farmer_name VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN farmer_mem_no VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN plot_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(additionalSymbol) FROM dailyMuster", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE dailyMuster ADD COLUMN additionalSymbol VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transaction_no) FROM visit_covered_topics_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visit_covered_topics_fc ADD COLUMN transaction_no VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(additionalSymbol) FROM sessionTasks", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE sessionTasks ADD COLUMN additionalSymbol VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farmer_name) FROM damage_count_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN farmer_name VARCHAR");
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN farmer_mem_no VARCHAR");
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN plot_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transaction_no) FROM child_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE child_chosen_fc ADD COLUMN transaction_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transaction_no) FROM action_plan_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE action_plan_chosen_fc ADD COLUMN transaction_no VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(ap_transaction_no) FROM action_plan_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE action_plan_chosen_fc ADD COLUMN ap_transaction_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(rc_transaction_no) FROM reasons_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE reasons_chosen_fc ADD COLUMN rc_transaction_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transaction_no) FROM reasons_chosen_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE reasons_chosen_fc ADD COLUMN transaction_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(dominant_clone_sid) FROM gap_filling_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE gap_filling_fc ADD COLUMN dominant_clone_sid VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_name) FROM farmer_plots_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_plots_table ADD COLUMN farm_name VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM gap_filling_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE gap_filling_fc ADD COLUMN user_id VARCHAR");
            database.execSQL("ALTER TABLE gap_filling_fc ADD COLUMN transaction_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM girth_measurement", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN user_id VARCHAR");
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN transaction_no VARCHAR");
            database.execSQL("ALTER TABLE girth_measurement ADD COLUMN member_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM interventions_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN user_id VARCHAR");
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN transaction_no VARCHAR");
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN member_id VARCHAR");
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN comment VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_plots_id) FROM interventions_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE interventions_fc ADD COLUMN farm_plots_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM ipm_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN user_id VARCHAR");
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN transaction_no VARCHAR");
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN member_id VARCHAR");
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN damage_caused VARCHAR");
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN date_today VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(user_id) FROM damage_count_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE damage_count_fc ADD COLUMN user_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(mode_of_payment) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN mode_of_payment VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(ipm_photo_no) FROM ipm_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN ipm_photo_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_plots_id) FROM ipm_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE ipm_table ADD COLUMN farm_plots_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(supporting_document_string) FROM farmer_photo_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_photo_table ADD COLUMN supporting_document_string VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(photo_category) FROM farmer_photo_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_photo_table ADD COLUMN photo_category VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(additional_activity_id) FROM dailyMuster", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE dailyMuster ADD COLUMN additional_activity_id VARCHAR");
            database.execSQL("ALTER TABLE dailyMuster ADD COLUMN additional_activity_complete_status VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(clone_replanting) FROM gap_filling_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE gap_filling_fc ADD COLUMN clone_replanting VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(entry_date) FROM TBL_infrastructure_polygons", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_polygons ADD COLUMN entry_date VARCHAR");
            database.execSQL("ALTER TABLE TBL_infrastructure_polygons ADD COLUMN location_category_id VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(infrastructure_location_no) FROM TBL_infrastructure_polygons", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_polygons ADD COLUMN infrastructure_location_no VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(infrastructure_location_no) FROM TBL_infrastructure_mapping", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_infrastructure_mapping ADD COLUMN infrastructure_location_no VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_plots_id) FROM gap_filling_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE gap_filling_fc ADD COLUMN farm_plots_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farm_name) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN farm_name VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(status) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN status VARCHAR");
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN land_details_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN village_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN more VARCHAR");
/*            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN land_details_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN land_details_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN land_details_id VARCHAR");*/
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN member_no VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN nat_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_no) FROM TBL_supplier_land_details", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_supplier_land_details ADD COLUMN plot_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(main_category_separator) FROM TBL_insp_questions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_insp_questions ADD COLUMN main_category_separator VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(isactive) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN isactive VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sync_status) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN sync_status VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(no_of_line) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN no_of_line VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN planted_trees VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN no_of_life_tree VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN no_of_tree_supplied_planting VARCHAR");
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN average_girth VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(lastupdatedate) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN lastupdatedate VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(datecomparer) FROM blocks_tree_count", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_tree_count ADD COLUMN datecomparer VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(estimated_volume) FROM farmer_access_code", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_access_code ADD COLUMN estimated_volume VARCHAR");
            database.execSQL("ALTER TABLE farmer_access_code ADD COLUMN del_date VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(planted_trees) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN planted_trees VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(total_sales) FROM Osrp_collectionSummary", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN total_sales VARCHAR");
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN harbel_price VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farmer_member_no) FROM farmer_agric_activities", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_agric_activities ADD COLUMN farmer_member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(not_used_check) FROM farmer_agric_activities", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_agric_activities ADD COLUMN not_used_check VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(datetime) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN datetime VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_answered) FROM TBL_insp_questions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_insp_questions ADD COLUMN is_answered VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sync_status) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN sync_status VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_no) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN member_no VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(start_time) FROM visits_inspection_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visits_inspection_table ADD COLUMN start_time VARCHAR");
            database.execSQL("ALTER TABLE visits_inspection_table ADD COLUMN end_time VARCHAR");

        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(country_id) FROM TBL_counties", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_counties ADD COLUMN country_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(status) FROM TBL_nationality", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_nationality ADD COLUMN status VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(receipt_no) FROM Osrp_collectionSummary", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE Osrp_collectionSummary ADD COLUMN receipt_no VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(Category_separator) FROM day_type", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE day_type ADD COLUMN Category_separator VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(numofpeople) FROM coaching_main", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN numofpeople VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(member_id) FROM coaching_main", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN member_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(no_fslb_farmers) FROM coaching_main", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN no_fslb_farmers VARCHAR");
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN no_non_fslb_farmers VARCHAR");
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN no_family_members VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(category) FROM coaching_main", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE coaching_main ADD COLUMN category VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(block_no) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN block_no VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN map_category VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN landsize VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN land_details_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN member_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN member_no VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(main_division_id) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN main_division_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sessionId) FROM selected_topics", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE selected_topics ADD COLUMN sessionId VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sub_system) FROM system_modules", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE system_modules ADD COLUMN sub_system VARCHAR");
            database.execSQL("ALTER TABLE system_modules ADD COLUMN sub_sub_system VARCHAR");
            database.execSQL("ALTER TABLE system_modules ADD COLUMN sub_sub_sub_system VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sub_system_yesno) FROM system_modules", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE system_modules ADD COLUMN sub_system_yesno VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(tapping_system_Name) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN tapping_system_Name VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN tapping_system_id VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN rubber_clones_Name VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN type_of_plantingname VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(no_of_Trees_in_the_block) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN no_of_Trees_in_the_block VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN stand_per_acre VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN panel_position_id VARCHAR");
            database.execSQL("ALTER TABLE block_table ADD COLUMN panel_position_Name VARCHAR");
        }

        //TC
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(additional_task_code) FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN additional_task_code VARCHAR");
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN additional_task_hrs_worked VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_trans_no_corrected) FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN is_trans_no_corrected VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(div_section_code) FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN div_section_code VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(visit_status) FROM visits_inspection_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visits_inspection_table ADD COLUMN visit_status VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(visit_reason_id) FROM visits_inspection_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visits_inspection_table ADD COLUMN visit_reason_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(other_task_rate_desc) FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN other_task_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN additional_task_rate_desc VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(typeOfDay) FROM TBL_inventory", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN typeOfDay VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN tanktaskid VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN taskname VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN itemId VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN contractorId VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN description VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN tankId VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN tapperNo VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN farmerNo VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN d_date VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN d_datetime VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN userId VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(typeOfDayStatus) FROM TBL_inventory", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN typeOfDayStatus VARCHAR");
            database.execSQL("ALTER TABLE TBL_inventory ADD COLUMN more_status VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(division_id) FROM landcoordinates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE landcoordinates ADD COLUMN division_id VARCHAR");
            database.execSQL("ALTER TABLE landcoordinates ADD COLUMN block_no VARCHAR");
            database.execSQL("ALTER TABLE landcoordinates ADD COLUMN block_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(block_id) FROM landcoordinates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {

            database.execSQL("ALTER TABLE landcoordinates ADD COLUMN block_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transact_no) FROM coaching_attendance", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {

            database.execSQL("ALTER TABLE coaching_attendance ADD COLUMN transact_no VARCHAR");
            database.execSQL("ALTER TABLE coaching_attendance ADD COLUMN notes VARCHAR");
            database.execSQL("ALTER TABLE coaching_attendance ADD COLUMN comments VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(village_id) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN region_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN district_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN ward_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN village_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN other_programs VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(first_name) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN first_name VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN middle_name VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN last_name VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN other_names VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(collection_centre_Id) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN collection_centre_Id VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN passporturl VARCHAR");
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN photo_string VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nationality_id) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN nationality_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(birth_cert) FROM TBL_next_of_kin", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_next_of_kin ADD COLUMN birth_cert VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(farmer_member_no_true) FROM TBL_next_of_kin", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_next_of_kin ADD COLUMN farmer_member_no_true VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(datecomparer) FROM TBL_next_of_kin", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_next_of_kin ADD COLUMN datecomparer VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(Is_active) FROM TBL_next_of_kin", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_next_of_kin ADD COLUMN Is_active VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM TBL_next_of_kin", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_next_of_kin ADD COLUMN nat_id VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM quizes_answers_fc", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE quizes_answers_fc ADD COLUMN nat_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM farmer_agric_activities", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE farmer_agric_activities ADD COLUMN nat_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM visits_inspection_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visits_inspection_table ADD COLUMN nat_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(qty_ordered) FROM checkoffdetails", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE checkoffdetails ADD COLUMN qty_ordered VARCHAR");
        }

        ///////////////////////////////////////////
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(other_task_code) FROM TBL_aggregates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_code VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_code VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_headman_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_headman_rate_value VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_overseer_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_overseer_rate_value VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_superintendent_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN other_task_superintendent_rate_value VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_headman_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_headman_rate_value VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_overseer_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_overseer_rate_value VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_superintendent_rate_desc VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN additional_task_superintendent_rate_value VARCHAR");
        }
        ///////////////////////////////////////////

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(rubberSource) FROM blocks_zones", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE blocks_zones ADD COLUMN rubberSource VARCHAR");
            //database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN rubberSource VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(rubberSource) FROM TBL_sessions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN rubberSource VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(more1) FROM TBL_sessions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN more1 VARCHAR");
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN rubberSource VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(rubberSource) FROM TBL_sessions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            //database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN more1 VARCHAR");
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN rubberSource VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(d_date) FROM TBL_sessions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN d_date VARCHAR");
            //database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN rubberSource VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(item_code) FROM subforwardingorders", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN item_code VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN details VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(county) FROM subforwardingorders", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN county VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN collection_center VARCHAR");
            database.execSQL("ALTER TABLE subforwardingorders ADD COLUMN collection_center_code VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transaction_no) FROM qc_central_weights", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE qc_central_weights ADD COLUMN transaction_no VARCHAR");
        }
        try {
            Cursor cursor1 = database.rawQuery("SELECT identifier_code FROM vehicle_gatepass_codes", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE vehicle_gatepass_codes ADD COLUMN identifier_code VARCHAR");

        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT visitorTel_no FROM visitations", null);

            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE visitations ADD COLUMN visitorTel_no VARCHAR");
            database.execSQL("ALTER TABLE visitations ADD COLUMN visitorGender VARCHAR");
            database.execSQL("ALTER TABLE visitations ADD COLUMN visitorEmail_address VARCHAR");
        }
        try { // FROM scale_settings where scale_type
            Cursor cursor1 = database.rawQuery("SELECT scale_type FROM scale_settings", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE scale_settings ADD COLUMN scale_type VARCHAR");
        }

        try { // FROM scale_settings where scale_type
            Cursor cursor1 = database.rawQuery("SELECT is_roaming FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN is_roaming VARCHAR");
        }

        try { // user_id in TBL_Members
            Cursor cursor1 = database.rawQuery("SELECT user_assignment_id FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN user_assignment_id VARCHAR");
        }

        try { // id:1 - Superintendant to OverSeerer, id:2 - OverSeerer to HeadMan, id:3 - HeadMan to Labourer
            Cursor cursor1 = database.rawQuery("SELECT activity_assignment_type_id FROM daily_muster_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE daily_muster_table ADD COLUMN activity_assignment_type_id VARCHAR");
        }

        try { // id:1 - Superintendant to OverSeerer, id:2 - OverSeerer to HeadMan, id:3 - HeadMan to Labourer
            Cursor cursor1 = database.rawQuery("SELECT superintendened_agg_value FROM TBL_aggregates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN superintendened_agg_value VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT headman_user_id FROM TBL_aggregates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN headman_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN overseer_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN superintendened_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN datecomparer VARCHAR");
        }


        try { // id:1 - Superintendant to OverSeerer, id:2 - OverSeerer to HeadMan, id:3 - HeadMan to Labourer
            Cursor cursor1 = database.rawQuery("SELECT superintendened_user_id FROM TBL_aggregates", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN superintendened_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN headman_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN overseer_user_id VARCHAR");
            database.execSQL("ALTER TABLE TBL_aggregates ADD COLUMN datecomparer VARCHAR");
        }


        // check if index exists
        try {
            Cursor cursor1 = database.rawQuery("SELECT count(hasindex) FROM TBL_members", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_members ADD COLUMN hasindex VARCHAR");
            database.execSQL("CREATE UNIQUE INDEX search_indexs  ON TBL_members (member_no, nat_id,full_name);");
        }

        //column category_separator in block_table
        try {
            Cursor cursor1 = database.rawQuery("SELECT category_separator FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN category_separator VARCHAR DEFAULT 'None' ");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(planted_year) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN planted_year VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN type_of_planting_id VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN spacing VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN rubber_clones_id VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN area_acre VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN year_of_opening VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN no_tapping_tasks VARCHAR ");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(tapping_frequency) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN tapping_frequency VARCHAR ");
            database.execSQL("ALTER TABLE block_table ADD COLUMN tapping_frequency_id VARCHAR ");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT tankId FROM TBL_sessions", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_sessions ADD COLUMN tankId VARCHAR ");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(unitCode) FROM item_products", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE item_products ADD COLUMN unitCode VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN rate_type_id VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN rate_type VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN category VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN main_category_id VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN is_compulsory VARCHAR");
            database.execSQL("ALTER TABLE item_products ADD COLUMN rate VARCHAR");


        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(transportcost) FROM centers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE centers ADD COLUMN transportcost VARCHAR");
            database.execSQL("ALTER TABLE centers ADD COLUMN more VARCHAR");
            database.execSQL("ALTER TABLE centers ADD COLUMN type VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(sync_status) FROM headman_report", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE headman_report ADD COLUMN sync_status VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(unit_price) FROM weighbridge_items", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE weighbridge_items ADD COLUMN unit_price VARCHAR");
            database.execSQL("ALTER TABLE weighbridge_items ADD COLUMN field_item_code VARCHAR");
            database.execSQL("ALTER TABLE weighbridge_items ADD COLUMN more VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(main_division_id) FROM block_table", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE block_table ADD COLUMN main_division_id VARCHAR");

        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_farmer_or_estate) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {

                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN is_farmer_or_estate VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(lr_number) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN lr_number VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN plot_number VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(datecomparer_all) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN datecomparer_all VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(plot_name) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN plot_name VARCHAR");
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN farm_name VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(nat_id) FROM TBL_divisions_polygon", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE TBL_divisions_polygon ADD COLUMN nat_id VARCHAR");
        }


        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_center_active) FROM centers", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE centers ADD COLUMN is_center_active VARCHAR");
        }

        try {
            Cursor cursor1 = database.rawQuery("SELECT count(is_tanktask_active) FROM tankTasks", null);
            cursor1.moveToFirst();
            if (!cursor1.isAfterLast()) {
                do {
                } while (cursor1.moveToNext());
            }
            cursor1.close();
        } catch (Exception e) {
            database.execSQL("ALTER TABLE tankTasks ADD COLUMN is_tanktask_active VARCHAR");
        }


    }

    public int userLoggedIn() {
        int p_id = 0;
        int refID = (int) getMaxId("logs");
        String[] columns = {"COALESCE(user_id, 0)"}; // columns to select
        String selection = "_id = ?";
        String[] args = {"" + refID}; // value added into where clause -->
        Cursor c = database.query("logs", columns, selection, args, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                do {
                    p_id = c.getInt(0);
                } while (c.moveToNext());
            }
            c.close();
        }
        return p_id;
    }

    public int count_Sessions() {
        int cnt = 0;
        String query = "SELECT Count(_id) FROM TBL_sessions WHERE status = ? AND vehicle_id <> 0 LIMIT 1";
        Cursor ct = database.rawQuery(query, new String[]{"new"});
        ct.moveToFirst();
        if (!ct.isAfterLast()) {
            do {
                cnt = ct.getInt(0);
            } while (ct.moveToNext());
        }
        ct.close();

        return cnt;
    }

    public void UpdateSessionSync(String session_id_id) {
        String whereClause = " _id = ? ";
        ContentValues cv = new ContentValues();
        cv.put("status", 1);
        Log.v("KKKKKKKKK", "KKKKKKK UPDATING ID " + session_id_id);
        database.update("TBL_sessions", cv, whereClause, new String[]{session_id_id});

    }


    public String getTestName(String test_id) {
        String test_name = "";

        return test_name;
    }

    public long getMaxIdFingerPrint(String TBL) {
        String query = "SELECT COALESCE(MAX(datecomparer), 0) AS maxTime FROM finger_prints ";
        Cursor cursor3 = database.rawQuery(query, null);
        long id = 0;
        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }

    public long getMaxIdDateComparer(String TBL) {
        String query = "SELECT MAX(datecomparer) AS maxTime FROM " + TBL + " ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }


    public long getMaxIdDateComparerSyncVar(String TBL) {
        String query = "SELECT MAX(sync_var) AS maxTime FROM " + TBL + " ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }


    public long getMaxIdDateComparerForFarmer(String TBL) {
        String query = "SELECT MAX(datecomparer_farmer) AS maxTime FROM " + TBL + " ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }

    public long getMaxIdDateComparerAll(String TBL) {
        String query = "SELECT MAX(datecomparer_all) AS maxTime FROM " + TBL + " ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }

    public long getMaxIdDateComparerFcDivPolygons(String TBL, String main_division_id) {
        String query = "SELECT MAX(datecomparer) AS maxTime FROM " + TBL + " WHERE main_division_id = '" + main_division_id + "' ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }

    public long getMaxIdDateComparerFcTreeCount(String TBL, String main_division_sid) {
        String query = "SELECT MAX(datecomparer) AS maxTime FROM " + TBL + " WHERE main_division_sid = '" + main_division_sid + "' ";
        Cursor cursor3 = database.rawQuery(query, null);

        long id = 0;

        if (cursor3.moveToFirst()) {
            do {
                id = cursor3.getLong(0);
            } while (cursor3.moveToNext());
        }
        cursor3.close();
        return id;
    }


    public long getMaxIdTBL(String TBL) {
        long id = 0;
        try {
            String query = "SELECT MAX(sync_datetime) AS maxTime FROM " + TBL + " ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    public long getMaxIdTBLAsInt(String TBL) {
        long id = 0;
        try {
            String query = "SELECT MAX(CAST(sync_datetime AS INTEGER)) AS maxTime FROM " + TBL + " ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    public long getMaxIdTBLDatecomarer(String TBL) {
        long id = 0;
        try {
            String query = "SELECT MAX(datecomparer) AS maxTime FROM " + TBL + " ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    public long getMaxIdTBLMember(String TBL, String main_category_code) {
        long id = 0;
        try {
            String query = "SELECT MAX(sync_datetime) AS maxTime FROM " + TBL;
//            String query = "SELECT MAX(sync_datetime) AS maxTime FROM " + TBL + " WHERE main_category_code = '"+main_category_code+"' ";

//            String query = "SELECT MAX(sync_datetime) AS maxTime FROM " + TBL + " WHERE membership_type_id = '"+member_type_id+"' ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    public String getCategoryId() {
        String id = "1";
        try {
            String query = " SELECT sid FROM member_category_table WHERE category_code = 'Emp' ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getString(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_CAT", "hh " + " DC " + id);
        return id;
    }

    public long getMaxIdQCTBL(String TBL) {
        long id = 0;
        try {
            String query = "SELECT MAX(sync_var) AS maxTime FROM " + TBL + " ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    public long getMaxIdTCTBL(String TBL) {
        long id = 0;
        try {
            String query = "SELECT MAX(sync_var) AS maxTime FROM " + TBL + " WHERE sync_var<>'' ";
            Cursor cursor3 = database.rawQuery(query, null);
            if (cursor3.moveToFirst()) {
                do {
                    id = cursor3.getLong(0);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.v("TBL_DC", "hh " + TBL + " DC " + id);
        return id;
    }

    private long getMaxId(String TBL) {
        int id = 0;
        String[] columns = {"MAX(_id)"}; // columns to select
        String selection = "_id = ?";
        Cursor cursor = database.query("logs", columns, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return id;
    }

    public String myPass() {
        String pass = "";
        SharedPreferences settings = act.getSharedPreferences("com.capturesolutions.weightcapture",
                Context.MODE_PRIVATE);
        pass = settings.getString("pass", "");
        return pass;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String encypt(String textRaw) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String text = textRaw;
        // Change this to UTF-16 if needed
        md.update(text.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        String hex = String.format("%064x", new BigInteger(1, digest));
        String str = new String(digest, StandardCharsets.UTF_8);
        String s = new String(digest);
        return hex;
    }

    private String getkey() {
        return act.getSharedPreferences("com.capturesolutions.weightcapture", Context.MODE_PRIVATE).getString("move",
                "");
    }

    public Boolean getUsers(String username, String passcode) {

        Boolean logged_in = false;

        try {
            Cursor UsersCursor = database
                    .query("users",
                            new String[]{"_id", "username", "password",
                                    "account_name", "branch", "rid"},
                            "(username = ? OR username = ? OR username = ? OR username = ? OR username = ?) AND password = ? ",
                            new String[]{"admin", "thikaadmin", "Admin", "HKOECH", "Developer", passcode.trim()},
                            null, null, "_id");

            UsersCursor.moveToFirst();

            if (!UsersCursor.isAfterLast()) {
                do {
                    String name = UsersCursor.getString(1);
                    logged_in = true;

                } while (UsersCursor.moveToNext());
            }
        } catch (Exception e) {
        }
        Log.v("hhhhhhhh", "bbbbb " + username);


        if (username.equalsIgnoreCase("User Must Login As Admin")) {
            return true;
        } else {
            return logged_in;
        }

    }

    public boolean loginUser(String username, String passcode) {
        boolean login_success = false;
        try {
            Cursor UsersCursor = database
                    .query("users",
                            new String[]{"_id", "username", "password",
                                    "account_name", "branch", "rid"},
                            "username = ? AND password = ? ",
                            new String[]{username.trim(), passcode.trim()},
                            null, null, "_id");
            // Toast.makeText(getApplicationContext(), passcode, Toast.LENGTH_LONG).show();
            UsersCursor.moveToFirst();

            if (!UsersCursor.isAfterLast()) {
                do {
                    String name = UsersCursor.getString(1);
                    login_success = true;
                    ContentValues logMap = new ContentValues();
                    logMap.put("user_id", UsersCursor.getInt(5));
                    InsertLog(logMap);
                    GlobalVariabless.userid = UsersCursor.getString(0);
                    GlobalVariabless.userrid_fc = UsersCursor.getString(5); //added by sakina
                } while (UsersCursor.moveToNext());
            } else {
                login_success = false;
            }
            UsersCursor.close();

            return login_success;
        } catch (Exception e) {
            e.printStackTrace();
            login_success = false;
        }

        return login_success;
    }

    private void InsertLog(ContentValues logMap) {

        try {
            database.insert("logs", null, logMap);
        } catch (Exception e) {

        }
    }

    public void updateUser(ContentValues memberMap, int user_id, ContentValues logMap) {
        int counter = 0;
        Cursor user = database.rawQuery("SELECT count(*) FROM users WHERE rid = ?", new String[]{"" + user_id});
        user.moveToFirst();
        if (!user.isAfterLast()) {
            do {
                counter = user.getInt(0);
            } while (user.moveToNext());
        }
        user.close();
        if (counter <= 0) {
            Log.v("LLLLLLLLLL", "KKKKKKKKKKKKKKKKK inserting");
            try {
                database.insert("users", null, memberMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String WhereClause = "rid = ?";
            String[] WhereArgs = {"" + user_id};
            try {
                Log.v("LLLLLLLLLL", "KKKKKKKKKKKKKKKKK updating");
                database.update("users", memberMap, WhereClause, WhereArgs);
            } catch (Exception ep) {
                ep.printStackTrace();
            }
        }
        // UPDATE LOG TBL
        try {
            database.insert("logs", null, logMap);
            // userModel.onCreate(null);
        } catch (Exception e) {
            Log.v("%%%%%%%%%%%% ", "" + e);
        }

    }

    public String getLOGGEDINUSER() {
        String query = "SELECT MAX(L._id), U.username FROM users U, logs L WHERE U.rid = L.user_id";
        String username = null;

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();

        if (!c.isAfterLast()) {

            do {
                username = c.getString(1);

            } while (c.moveToNext());
        }
        c.close();
        return username;
    }

    public void SaveSessionTasks(ContentValues val) {
        database.insertOrThrow("sessionTasks", null, val);
    }

    public void insertMemberPhotos(JSONArray data) throws JSONException {
    /*
    "$id": "2",
    "id": 4,
    "member_no": 1036,
    "biometric_type_details_id": 1,
    "biometric_type_id": 1,
    "biometric_string": "",
    "url_path": "",
    "biometric_type_name": "Finger Prints",
    "biometric_type_details_name": "Right Thumb",
    "isactive": false,
    "status": true,
    "datecomparer": 1
    */
        JSONArray photoArray = data;
        data = null;

        for (int i = 0; i < photoArray.length(); i++) {
            try {
//                String responseOBJ = photoArray.getJSONObject(i).toString();

                database.execSQL("DELETE FROM member_data_table WHERE sid = '" + photoArray.getJSONObject(i).getString("id") + "' ");

//               int deleted = database.delete("member_data_table", " WHERE sid = '"+photoArray.getJSONObject(i).getString("id")+"' ", null);
//
//               if (deleted == 0){
//
//               } else {
//                   //method to delete image from file also
//                   delete_file_photo(photoArray.getJSONObject(i).getString("member_no"));
//               }

                if (!photoArray.getJSONObject(i).getBoolean("status")) {
                    return;
                }

            } catch (Exception ex) {
            }

            try {
                String data_type = photoArray.getJSONObject(0).getString("biometric_type_id");

                ContentValues cv = new ContentValues();
                String imageName = data_type.equalsIgnoreCase("2") ? save_image(photoArray.getJSONObject(i).getString("biometric_string"), photoArray.getJSONObject(i).getString("member_no")) : photoArray.getJSONObject(i).getString("biometric_string");

                cv.put("sync_status", "i");
                cv.put("sid", photoArray.getJSONObject(i).getString("id"));
                cv.put("data", imageName);
                cv.put("data_index", photoArray.getJSONObject(i).getString("biometric_type_details_id"));
                cv.put("data_type", data_type);
                cv.put("member_no", photoArray.getJSONObject(i).getString("member_no"));
                cv.put("sync_var", photoArray.getJSONObject(i).getLong("datecomparer"));
                cv.put("data_storage_mode", data_type.equalsIgnoreCase("2") ? "path" : "db");

                cv.put("face_template", getMemberImageTemplate(photoArray.getJSONObject(i).getString("biometric_string")));

                String item_id = null;

                String[] WhereArgs1 = {"" + photoArray.getJSONObject(i).getInt("id")};
                String WhereClause = "sid = ?";
                //check database if data exists
                Cursor cursor1 = database.rawQuery("SELECT sid FROM member_data_table WHERE sid ='" + photoArray.getJSONObject(i).getString("id") + "'", null);
                cursor1.moveToFirst();
                if (!cursor1.isAfterLast()) {
                    do {
                        item_id = cursor1.getString(0);
                    } while (cursor1.moveToNext());
                }
                cursor1.close();
                if (item_id == null) {
                    Log.e("Inserting Photo =>", "" + database.insert("member_data_table", null, cv));
                } else {
                    Log.e("Inserting Photo =>", "" + database.update("member_data_table", cv, WhereClause, WhereArgs1));
                }

                if (data_type.equalsIgnoreCase("2")) {

                    System.gc();
                    //saveFaceToFacePass(cv);
                }

            } catch (Exception ex) {
                Log.e("error inserting photo=>", "" + ex.getMessage());
            }
        }
    }

    public Map<String, byte[]> getAllTemplate() {
        Map<String, byte[]> templateMap = new HashMap<>();
        String member_no;
        byte[] template;

        Cursor cursor1 = database.rawQuery("SELECT member_no, face_template FROM member_data_table WHERE face_template != null", null);

        if (cursor1.moveToFirst()) {
            do {
                member_no = cursor1.getString(0);
                template = cursor1.getBlob(1);
                templateMap.put(member_no, template);

            } while (cursor1.moveToNext());
        }


        return templateMap;
    }

    public byte[] getMemberImageTemplate(String base64) throws IOException {
        int count = 0;
        byte[] imgTemplate = new byte[0];
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedImg = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        if (decodedImg != null) {
            Log.e("DbHelper", "Get Template: ");

            byte[] imageBgr = ImageTransformUtils.bitmap2RGB(decodedImg);
            FacePPImage facePPImagex = new FacePPImage.Builder()
                    .setData(imageBgr)
                    .setWidth(decodedImg.getWidth())
                    .setHeight(decodedImg.getHeight())
                    .setMode(FacePPImage.IMAGE_MODE_BGR)
                    .setRotation(FacePPImage.FACE_UP).build();
            if (facePPImagex == null) {
                Log.e("DbHelper", "facePPImage IS NULL: ");
            }

            FaceDetectApi.Face[] faces_from_service = FaceDetectApi.getInstance().detectFace(facePPImagex);
            Log.e("DbHelper", "faces_from_service: " + faces_from_service.length);

            if (faces_from_service.length > 0) {
                FaceDetectApi.Face face2 = faces_from_service[0];
                FaceDetectApi.getInstance().getExtractFeature(face2);

                byte[] template = face2.feature;
                byte[] plain = template;

                count = count + 1;
                Global.faceTemplatMap.put(count + "", plain);

                imgTemplate = plain;

            } else {
                Log.v("DbHelper", "NO FACE FROM FILE xxxxxxxxxxxxxx");
            }
            return imgTemplate;
        } else {
            Log.v("DbHelper", "Bitmap is dead on arrival");
            return imgTemplate;
        }

    }

//    public static boolean saveFaceToFacePass(Bitmap image, String member_no) {
//
//        if (mFacePassHandler == null) {
//            //Toast.makeText(act, "FacePassHandle is null !", Toast.LENGTH_SHORT).show();
//            Log.e("dbhelper", "SaveFaceToFacePass : FacePassHandle is null !" );
//            return false;
//        }
////        String imagePath = cv.getAsString("data");
////        Log.v("Image path", "Image path = " + imagePath);
////        if (TextUtils.isEmpty(imagePath)) {
////            Log.e("dbhelper", "SaveFaceToFacePass : Please enter the correct image path!" );
////            //Toast.makeText(act, "Please enter the correct image path!", Toast.LENGTH_SHORT).show();
////            return false;
////        }
////
////        File imageFile = new File(imagePath);
////        if (!imageFile.exists()) {
////            //Toast.makeText(act, "Picture does not exist!", Toast.LENGTH_SHORT).show();
////            Log.e("dbhelper", "SaveFaceToFacePass : Picture does not exist!" );
////
////            return false;
////        }
////
////        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
//        try {
//            FacePassAddFaceResult result = mFacePassHandler.addFace(image);
//            if (result != null) {
//                if (result.result == 0) {
//                    //Toast.makeText(act, "add face successfully！", Toast.LENGTH_SHORT).show();
//                    Log.e("dbhelper", "SaveFaceToFacePass : Face Added Successfully!" );
//                    Log.e("dbhelper", new String(result.faceToken)+"SaveFaceToFacePass : Member_No : " + member_no + " Face_Tocken : " + Arrays.toString(result.faceToken));
//
//                    //faceTokenEt.setText(new String(result.faceToken));
//                    if (bindFaceToGroup(new String(result.faceToken))){
//                        database.execSQL("UPDATE member_data_table SET face_token = ? WHERE member_no = ?", new String[]{new String(result.faceToken),member_no});
//                    }else{
//                        database.execSQL("UPDATE member_data_table SET face_token = ? WHERE member_no = ?",new String[]{new String(result.faceToken),member_no});
//                        database.execSQL("UPDATE member_data_table SET data_source = 'Failed To Bind Face' WHERE member_no = ?",new String[]{member_no});
//
//                    }
//
//                    return true;
//
//                } else if (result.result == 1) {
//                    Log.e("dbhelper", "SaveFaceToFacePass : No Face!" );
//
//
//                    database.execSQL("UPDATE member_data_table SET data_source = 'no face' WHERE member_no = ?",new String[]{member_no});
//
//                    return false;
//
//                } else {
//
//                    Log.e("dbhelper", "SaveFaceToFacePass : quality problem!" );
//                    database.execSQL("UPDATE member_data_table SET data_source = 'quality problem' WHERE member_no = ?",new String[]{member_no});
//
//                    return false;
//                }
//            }
//        } catch (FacePassException e) {
//            e.printStackTrace();
//            Log.e("dbhelper", "SaveFaceToFacePass : " + e.getMessage());
//
//            return false;
//        }
//
//        return false;
//    }
//    public static boolean bindFaceToGroup(String face_token){
//        if (mFacePassHandler == null) {
//            Log.e("dbhelper", "SaveFaceToFacePass : FacePassHandle is null !" );
//
//            return false;
//        }
//
//        byte[] faceToken = face_token.getBytes();
//        String groupName = group_name;
//        if (faceToken == null || faceToken.length == 0 || TextUtils.isEmpty(groupName)) {
//
//            Log.e("dbhelper", "SaveFaceToFacePass : params error !" );
//
//            return false;
//        }
//        try {
//            Log.e("dbhelper", "BindFaceToGroup, Group Name: "+groupName +" FaceToken: " +faceToken );
//
//            boolean b = mFacePassHandler.bindGroup(groupName, faceToken);
//            String result = b ? "success" : "failed";
//
//            Log.e("dbhelper", "SaveFaceToFacePass : bind !" + result );
//
//            if (result.equalsIgnoreCase("success")){
//                Log.e("dbhelper", "SaveFaceToFacePass : Success Binding !" + result );
//                return true;
//            }else {
//                Log.e("dbhelper", "SaveFaceToFacePass : Failed Binding !" + result );
//                return false;
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("dbhelper", "SaveFaceToFacePass : " + e.getMessage());
//            return false;
//        }
//    }


    public static String save_image(String base64_bytes, String member_no) {
        Bitmap bmp = getImage(Base64.decode(base64_bytes, 0));

        String img_name = "TC_" + member_no + "_FS.JPG";

        OutputStream fOutputStream = null;

        File file = new File(GlobalVariabless.tc_employee_file_path);
        if (!file.exists()) {
            Log.e("Creating data dir=>", "" + String.valueOf(file.mkdirs()));
        }
        file = new File(GlobalVariabless.tc_employee_file_path, img_name);

        try {
            fOutputStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("Creating data dir=>", "------saving pic failed------");
            return "------saving pic failed------";
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Creating data dir=>", "------saving pic failed------");
            return "------saving pic failed------";
        }


        //Saving face to FacPass, Save face to Megvi
        //saveFaceToFacePass(bmp,member_no);

        return img_name;
    }

    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public int getUSERID() {

        String query = "SELECT MAX(_id), user_id FROM logs";
        String[] columns = {"MAX(_id)", "member_id"};
        String session_id = null;
        int res_id = 0;
        Cursor c = database.rawQuery(query, null);

        c.moveToFirst();
        if (!c.isAfterLast()) {
            do {
                res_id = c.getInt(1);

            } while (c.moveToNext());
        }

        c.close();
        return res_id;
    }


}