package com.megvii.demoface.skeleton;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.SkeletonImagePoint;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.SkeletonDetectApi;
import com.megvii.facepp.multi.sdk.skeleton.SkeletonInfo;

import butterknife.BindView;
import butterknife.OnClick;

public class SkeletonImageActivity extends BaseActivity {
    @BindView(R.id.mSkeletonImagePoint)
    SkeletonImagePoint mSkeletonImagePoint;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_skeleton_image;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String path = ConUtil.getRealPathFromURI(this, uri);
        Bitmap bitmap = ConUtil.getImage(path);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        mSkeletonImagePoint.refreshView((int) (imageWidth * scale), (int) (imageHeight * scale));
        mSkeletonImagePoint.setBitmapAndResult(bitmap, null);
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        long startTime = System.currentTimeMillis();
        SkeletonInfo[] result = SkeletonDetectApi.getInstance().detectSkeleton(facePPImage);
        long timeConst = System.currentTimeMillis()-startTime;
        mSkeletonImagePoint.setBitmapAndResult(bitmap, result);
        tvDetectTips.setText("Single frame time (ms):"+timeConst);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }
}
