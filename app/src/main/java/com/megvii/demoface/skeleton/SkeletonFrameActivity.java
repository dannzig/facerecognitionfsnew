package com.megvii.demoface.skeleton;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.CameraTextureView;
import com.megvii.demoface.view.SkeletonFramePoint;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.SkeletonDetectApi;
import com.megvii.facepp.multi.sdk.skeleton.SkeletonInfo;

import butterknife.BindView;
import butterknife.OnClick;

public class SkeletonFrameActivity extends BaseActivity implements CameraWrapper.CameraOpenCallback, CameraWrapper.ICameraCallback, TextureView.SurfaceTextureListener {

    @BindView(R.id.view_skeleton_point)
    SkeletonFramePoint mViewSkeletonPoint;
    @BindView(R.id.my_camera_textureview)
    CameraTextureView myCameraTextureview;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_flip_camera)
    ImageView ivFlipCamera;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    private CameraManager mCameraManager;
    private Handler mHandler;
    private HandlerThread mHandlerThread = new HandlerThread("facepp");

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_skeleton_frame;
    }

    @Override
    protected void initData() {
        mCameraManager = new CameraManager(this);
        mCameraManager.setmCameraOpenCallback(this);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    protected void initView() {
        myCameraTextureview.setSurfaceTextureListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraManager.openCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraManager.closeCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void adjustTextureViewSize() {
        float scale = Math.max(Screen.mHeight * 1.0f / CameraFactory.mWidth, Screen.mWidth * 1.0f / CameraFactory.mHeight);
        int layout_width = (int) (CameraFactory.mHeight * scale);
        int layout_height = (int) (CameraFactory.mWidth * scale);

        myCameraTextureview.refreshView(layout_width, layout_height);
        mViewSkeletonPoint.refreshView(layout_width, layout_height);

        RelativeLayout.LayoutParams surfaceParams = (RelativeLayout.LayoutParams) myCameraTextureview.getLayoutParams();
        int topMargin = Math.min((Screen.mHeight - layout_height) / 2, 0);
        int leftMargin = Math.min((Screen.mWidth - layout_width) / 2, 0);
        surfaceParams.topMargin = topMargin;
        surfaceParams.leftMargin = leftMargin;
        myCameraTextureview.setLayoutParams(surfaceParams);
        mViewSkeletonPoint.setLayoutParams(surfaceParams);
    }

    @Override
    public void onOpenSuccess() {
        Log.w("onOpenSuccess", "onOpenSuccess....");
        if (mCameraManager.isFrontCam()) {
            faceRotation = FacePPImage.FACE_RIGHT;
        } else {
            faceRotation = FacePPImage.FACE_LEFT;
        }

        mCameraManager.setDisplayOrientation();
        mCameraManager.startPreview(myCameraTextureview.getSurfaceTexture());
        mCameraManager.actionDetect(this);
        adjustTextureViewSize();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onOpenFailed() {

    }

    private int frame = 0;
    private long startTime = 0;
    private long totalTime = 0;

    private void calculateEfficiEency(long detectTime) {
        frame++;
        totalTime = totalTime + detectTime;
        if (frame == 30) {
            StringBuilder tipSb = new StringBuilder();
            tipSb.append("Single frame time (ms):");
            long timeconst = (long) (totalTime * 1.0f / frame);
            tipSb.append(timeconst);
            long fps = (long) (1000.0f/timeconst);
            tipSb.append("\n");
            tipSb.append("frame rate (fps):");
            tipSb.append(fps);
            frame = 0;
            totalTime = 0;
            tvDetectTips.setText(tipSb);
        }
    }

    boolean isFinish = true;
    private int faceRotation = FacePPImage.FACE_LEFT;

    @Override
    public void onPreviewFrame(final byte[] bytes, Camera camera) {
        if (!isFinish) {
            return;
        }
        isFinish = false;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.w("cameraAngle", (360 - mCameraManager.getAngleGoogle()) % 360 + "");
                FacePPImage facePPImage = new FacePPImage.Builder()
                        .setData(bytes)
                        .setWidth(CameraFactory.mWidth)
                        .setHeight(CameraFactory.mHeight)
                        .setMode(FacePPImage.IMAGE_MODE_NV21)
                        .setRotation(faceRotation).build();
                startTime = System.currentTimeMillis();
                final SkeletonInfo[] skeletonInfos = SkeletonDetectApi.getInstance().detectSkeletonFrame(facePPImage);
                final long timeConst = System.currentTimeMillis()-startTime;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mViewSkeletonPoint.setPointResult(skeletonInfos, !mCameraManager.isFrontCam());
                        calculateEfficiEency(timeConst);
                    }
                });
                isFinish = true;
            }
        });
    }


    @Override
    public void onCapturePicture(byte[] bytes) {

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }


    @OnClick({R.id.ll_go_back, R.id.iv_flip_camera, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_flip_camera:
                mCameraManager.switchCamera();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }
}
