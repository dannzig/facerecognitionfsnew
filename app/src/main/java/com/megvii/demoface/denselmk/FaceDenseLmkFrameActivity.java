package com.megvii.demoface.denselmk;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.opengl.CameraMatrix;
import com.megvii.demoface.opengl.ICameraMatrix;
import com.megvii.demoface.opengl.PointsMatrix;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.CameraGlSurfaceView;
import com.megvii.demoface.view.DenseLmkDetailTypeView;
import com.megvii.facepp.multi.sdk.DLmkDetectApi;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceDetail;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceLmks;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import butterknife.BindView;
import butterknife.OnClick;

import static com.megvii.facepp.multi.sdk.FaceDetectApi.FaceppConfig.DETECTION_MODE_TRACKING;

public class FaceDenseLmkFrameActivity extends BaseActivity implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener, CameraWrapper.CameraOpenCallback, CameraWrapper.ICameraCallback {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_flip_camera)
    ImageView ivFlipCamera;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.view_dlmk_hairline)
    DenseLmkDetailTypeView viewDlmkHairline;
    @BindView(R.id.view_dlmk_eyebrow)
    DenseLmkDetailTypeView viewDlmkEyebrow;
    @BindView(R.id.view_dlmk_eye)
    DenseLmkDetailTypeView viewDlmkEye;
    @BindView(R.id.view_dlmk_nose_midline)
    DenseLmkDetailTypeView viewDlmkNoseMidline;
    @BindView(R.id.view_dlmk_nose)
    DenseLmkDetailTypeView viewDlmkNose;
    @BindView(R.id.view_dlmk_mouth)
    DenseLmkDetailTypeView viewDlmkMouth;
    @BindView(R.id.view_dlmk_contour)
    DenseLmkDetailTypeView viewDlmkContour;
    @BindView(R.id.view_dlmk_face_midline)
    DenseLmkDetailTypeView viewDlmkFaceMidline;
    private CameraManager mCameraManager;
    private Handler mHandler;
    private HandlerThread mHandlerThread = new HandlerThread("facepp");
    private int detailType;
    @BindView(R.id.opengl_layout_surfaceview)
    CameraGlSurfaceView mGlSurfaceView;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_dense_lmk_frame;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("人脸稠密关键点");
        mGlSurfaceView.setEGLContextClientVersion(2);// 创建一个OpenGL ES 2.0
        mGlSurfaceView.setRenderer(this);
        // RENDERMODE_CONTINUOUSLY不停渲染
        // RENDERMODE_WHEN_DIRTY懒惰渲染，需要手动调用 glSurfaceView.requestRender() 才会进行更新
        mGlSurfaceView.setRenderMode(mGlSurfaceView.RENDERMODE_WHEN_DIRTY);// 设置渲染器模式
    }

    @Override
    protected void initData() {
        detailType = DLmkDetectApi.getInstance().getFullDetailType();
        FaceDetectApi.FaceppConfig config = FaceDetectApi.getInstance().getFaceppConfig();
        config.face_confidence_filter = 0.6f;
        config.detectionMode = DETECTION_MODE_TRACKING;
        int minface = 40; // 640*480;
        float scale = (CameraFactory.mWidth * CameraFactory.mHeight) * 1.0f / (640 * 480);
        config.minFaceSize = (int) (scale * minface);
        FaceDetectApi.getInstance().setFaceppConfig(config);
        mCameraManager = new CameraManager(this);
        mCameraManager.setmCameraOpenCallback(this);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraManager.openCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraManager.closeCamera();
    }

    private void adjustTextureViewSize() {
        float scale = Math.max(Screen.mHeight * 1.0f / CameraFactory.mWidth, Screen.mWidth * 1.0f / CameraFactory.mHeight);
        int layout_width = (int) (CameraFactory.mHeight * scale);
        int layout_height = (int) (CameraFactory.mWidth * scale);

        mGlSurfaceView.refreshView(layout_width, layout_height);

        RelativeLayout.LayoutParams surfaceParams = (RelativeLayout.LayoutParams) mGlSurfaceView.getLayoutParams();
        int topMargin = Math.min((Screen.mHeight - layout_height) / 2, 0);
        int leftMargin = Math.min((Screen.mWidth - layout_width) / 2, 0);
        surfaceParams.topMargin = topMargin;
        surfaceParams.leftMargin = leftMargin;
        mGlSurfaceView.setLayoutParams(surfaceParams);
    }

    boolean isFinish = true;
    private int faceRotation = FacePPImage.FACE_LEFT;
    private boolean isFliping = false; //相机切换中

    @Override
    public void onPreviewFrame(final byte[] bytes, Camera camera) {
        int width = CameraFactory.mWidth;
        int height = CameraFactory.mHeight;
        startTime = System.currentTimeMillis();
        final FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(imageBuilder.setData(bytes).build());
        final long detectConst = System.currentTimeMillis() - startTime;
        startTime = System.currentTimeMillis();

        if (faces != null) {
            ArrayList<ArrayList> pointsOpengl = new ArrayList<ArrayList>();
            for (int c = 0; c < faces.length; c++) {
                FaceDetectApi.getInstance().getLandmark(faces[c], FaceDetectApi.LMK_84, true);
                DlmkFaceDetail faceDetail = DLmkDetectApi.getInstance().detectDenseLmk(imageBuilder.setData(bytes).build(), faces[c].points, faces[c].rect, detailType);
                ArrayList<FloatBuffer> triangleVBList = new ArrayList<FloatBuffer>();
                int detailNum = faceDetail.detailNum;
                for (int i = 0; i < detailNum; i++) {
                    DlmkFaceLmks lmk = faceDetail.lmk[i];
                    int pointNum = lmk.pointNum;
                    for (int j = 0; j < pointNum; j++) {
                        float x = (lmk.data[j].x / width) * 2 - 1;
                        if (!mCameraManager.isFrontCam())
                            x = -x;
                        float y = (lmk.data[j].y / height) * 2 - 1;
                        float[] pointf = new float[]{y, x, 0.0f};

                        FloatBuffer fb = mCameraMatrix.floatBufferUtil(pointf);
                        triangleVBList.add(fb);
                    }
                }
                pointsOpengl.add(triangleVBList);
            }
            synchronized (mPointsMatrix) {
                mPointsMatrix.points = pointsOpengl;

            }
        }
        final long timeConst = System.currentTimeMillis() - startTime;
//                    mFaceImagePointView.setBitmapAndResult(bitmap, detailType, list);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isFliping) {
                    return;
                }
//                    viewDensePoint.setResult(list, !mCameraManager.isFrontCam());
                calculateEfficiEency(timeConst, detectConst);
            }
        });
        mGlSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                mGlSurfaceView.requestRender();
            }
        });
    }

//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                FacePPImage facePPImage = new FacePPImage.Builder()
//                        .setData(bytes)
//                        .setWidth(CameraFactory.mWidth)
//                        .setHeight(CameraFactory.mHeight)
//                        .setMode(FacePPImage.IMAGE_MODE_NV21)
//                        .setRotation(faceRotation).build();
//                startTime = System.currentTimeMillis();
//                final FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(facePPImage);
//                final long detectConst = System.currentTimeMillis() - startTime;
//                startTime = System.currentTimeMillis();
//                if (faces != null) {
//                    final ArrayList<DlmkFaceDetail> list = new ArrayList<>();
//                    for (FaceDetectApi.Face face : faces) {
//                        FaceDetectApi.getInstance().getLandmark(face, FaceDetectApi.LMK_84, true);
//                        DlmkFaceDetail faceDetail = DLmkDetectApi.getInstance().detectDenseLmk(facePPImage, face.points, face.rect, detailType);
//                        list.add(faceDetail);
//                    }
//                    final long timeConst = System.currentTimeMillis() - startTime;
////                    mFaceImagePointView.setBitmapAndResult(bitmap, detailType, list);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (isFliping) {
//                                return;
//                            }
//                            viewDensePoint.setResult(list, !mCameraManager.isFrontCam());
//                            calculateEfficiEency(timeConst, detectConst);
//                        }
//                    });
//                    isFinish = true;
//                }
//            }
//        });
//}

    private int frame = 0;
    private long startTime = 0;
    private long totalTime = 0;
    private long detectTotalTime = 0;

    private void calculateEfficiEency(long detectDenseTime, long detectFaceTime) {
        frame++;
        totalTime = totalTime + detectDenseTime;
        detectTotalTime = detectTotalTime + detectFaceTime;
        if (frame == 30) {
            StringBuilder tipSb = new StringBuilder();
            tipSb.append("人脸检测单帧耗时(ms):");
            long detectTimeCost = (long) (detectTotalTime * 1.0 / frame);
            tipSb.append(detectTimeCost).append("\n");
            tipSb.append("稠密点单帧耗时(ms):");
            long timeconst = (long) (totalTime * 1.0f / frame);
            tipSb.append(timeconst);
            long fps = (long) (1000.0f / (timeconst + detectTimeCost));
            tipSb.append("\n");
            tipSb.append("帧率(fps):");
            tipSb.append(fps);
            frame = 0;
            totalTime = 0;
            detectTotalTime = 0;
            tvDetectTips.setText(tipSb);
        }
    }

    @Override
    public void onCapturePicture(byte[] bytes) {

    }

    private FacePPImage.Builder imageBuilder;

    @Override
    public void onOpenSuccess() {
        isFliping = false;
        if (mCameraManager.isFrontCam()) {
            faceRotation = FacePPImage.FACE_RIGHT;
        } else {
            faceRotation = FacePPImage.FACE_LEFT;
        }

//        mCameraManager.setDisplayOrientation();
//        mCameraManager.startPreview(myCameraTextureview.getSurfaceTexture());
//        mCameraManager.actionDetect(this);
        imageBuilder = new FacePPImage.Builder().setWidth(CameraFactory.mWidth).setHeight(CameraFactory.mHeight).setMode(FacePPImage.IMAGE_MODE_NV21).setRotation(faceRotation);

        adjustTextureViewSize();
//        viewFacePoint.setVisibility(View.VISIBLE);
        mGlSurfaceView.setVisibility(View.GONE);
        mGlSurfaceView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onOpenFailed() {

    }

    @OnClick({R.id.ll_go_back, R.id.iv_flip_camera, R.id.iv_show_tips, R.id.view_dlmk_hairline, R.id.view_dlmk_eyebrow, R.id.view_dlmk_eye, R.id.view_dlmk_nose_midline, R.id.view_dlmk_nose, R.id.view_dlmk_mouth, R.id.view_dlmk_contour, R.id.view_dlmk_face_midline})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_flip_camera:
                isFliping = true;
                mCameraManager.switchCamera();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
            case R.id.view_dlmk_hairline:
                viewDlmkHairline.setChecked(!viewDlmkHairline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_HAIRLINE.toInt(), !viewDlmkHairline.isChecked());
                break;
            case R.id.view_dlmk_eyebrow:
                viewDlmkEyebrow.setChecked(!viewDlmkEyebrow.isChecked());
                int eyebrowValue = DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_LEFT_EYEBROW.toInt() | DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_RIGHT_EYEBROW.toInt();
                setDetailType(eyebrowValue, !viewDlmkEyebrow.isChecked());
                break;
            case R.id.view_dlmk_eye:
                viewDlmkEye.setChecked(!viewDlmkEye.isChecked());
                int eyeValue = DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_LEFT_EYE.toInt() | DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_RIGHT_EYE.toInt();
                setDetailType(eyeValue, !viewDlmkEye.isChecked());
                break;
            case R.id.view_dlmk_nose_midline:
                viewDlmkNoseMidline.setChecked(!viewDlmkNoseMidline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_NOSE_MIDLINE.toInt(), !viewDlmkNoseMidline.isChecked());
                break;
            case R.id.view_dlmk_nose:
                viewDlmkNose.setChecked(!viewDlmkNose.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_NOSE.toInt(), !viewDlmkNose.isChecked());
                break;
            case R.id.view_dlmk_mouth:
                viewDlmkMouth.setChecked(!viewDlmkMouth.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_MOUTH.toInt(), !viewDlmkMouth.isChecked());
                break;
            case R.id.view_dlmk_contour:
                viewDlmkContour.setChecked(!viewDlmkContour.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_CONTOUR.toInt(), !viewDlmkContour.isChecked());
                break;
            case R.id.view_dlmk_face_midline:
                viewDlmkFaceMidline.setChecked(!viewDlmkFaceMidline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_FACE_MIDLINE.toInt(), !viewDlmkFaceMidline.isChecked());
                break;
        }
    }

    private void setDetailType(int value, boolean isChecked) {
        if (isChecked) {
            detailType &= (~value);
        } else {
            detailType |= value;
        }
    }

    private int mTextureID = -1;
    private SurfaceTexture mSurface;
    private CameraMatrix mCameraMatrix;
    private PointsMatrix mPointsMatrix;

    private final float[] mProjMatrix = new float[16];
    private final float[] mVMatrix = new float[16];
    private final float[] mMVPMatrix = new float[16];

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        surfaceInit();
    }

    private void surfaceInit() {
        mTextureID = ICameraMatrix.getOESTexture();

        mSurface = new SurfaceTexture(mTextureID);
//        checkCamera();
        // 这个接口就干了这么一件事，当有数据上来后会进到onFrameAvailable方法
        mSurface.setOnFrameAvailableListener(this);// 设置照相机有数据时进入
        mCameraMatrix = new CameraMatrix(mTextureID);
        mPointsMatrix = new PointsMatrix(false);
//        mICamera.startPreview(mSurface);// 设置预览容器
//        mICamera.actionDetect(this);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mCameraManager.startPreview(mSurface);
        mCameraManager.actionDetect(this);

        // 设置画面的大小
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;
        ratio = 1; // 这样OpenGL就可以按照屏幕框来画了，不是一个正方形了

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        final long actionTime = System.currentTimeMillis();
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);// 清除屏幕和深度缓存
        float[] mtx = new float[16];
        mSurface.getTransformMatrix(mtx);
        mCameraMatrix.draw(mtx);
        // Set the camera position (View matrix)
        Matrix.setLookAtM(mVMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1f, 0f);
        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, mVMatrix, 0);
        mPointsMatrix.draw(mMVPMatrix);

        mSurface.updateTexImage();// 更新image，会调用onFrameAvailable方法
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//        mGlSurfaceView.requestRender();
    }
}
