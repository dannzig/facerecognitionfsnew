package com.megvii.demoface.denselmk;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.DenseLmkDetailTypeView;
import com.megvii.demoface.view.FaceImageDlmkView;
import com.megvii.facepp.multi.sdk.DLmkDetectApi;
import com.megvii.facepp.multi.sdk.FaceDetectApi;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.denselmk.DlmkFaceDetail;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.megvii.facepp.multi.sdk.FaceDetectApi.FaceppConfig.DETECTION_MODE_DETECT;

public class FaceDenseLmkImageActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.mFaceImagePointView)
    FaceImageDlmkView mFaceImagePointView;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.view_dlmk_hairline)
    DenseLmkDetailTypeView viewDlmkHairline;
    @BindView(R.id.view_dlmk_eyebrow)
    DenseLmkDetailTypeView viewDlmkEyebrow;
    @BindView(R.id.view_dlmk_eye)
    DenseLmkDetailTypeView viewDlmkEye;
    @BindView(R.id.view_dlmk_nose_midline)
    DenseLmkDetailTypeView viewDlmkNoseMidline;
    @BindView(R.id.view_dlmk_nose)
    DenseLmkDetailTypeView viewDlmkNose;
    @BindView(R.id.view_dlmk_mouth)
    DenseLmkDetailTypeView viewDlmkMouth;
    @BindView(R.id.view_dlmk_contour)
    DenseLmkDetailTypeView viewDlmkContour;
    @BindView(R.id.view_dlmk_face_midline)
    DenseLmkDetailTypeView viewDlmkFaceMidline;
    private Bitmap bitmap;

    private int detailType;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_dense_lmk_image;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("人脸稠密关键点");

    }

    @Override
    protected void initData() {
        detailType = DLmkDetectApi.getInstance().getFullDetailType();
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String path = ConUtil.getRealPathFromURI(this, uri);
        bitmap = ConUtil.getImage(path);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        mFaceImagePointView.refreshView((int) (imageWidth * scale), (int) (imageHeight * scale));
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        long startTime = System.currentTimeMillis();
        FaceDetectApi.FaceppConfig config = FaceDetectApi.getInstance().getFaceppConfig();
        config.face_confidence_filter = 0.6f;
        config.detectionMode = DETECTION_MODE_DETECT;
        FaceDetectApi.getInstance().setFaceppConfig(config);
        FaceDetectApi.Face[] faces = FaceDetectApi.getInstance().detectFace(facePPImage);

        StringBuilder sb = new StringBuilder();
        if (faces != null) {
            ArrayList<DlmkFaceDetail> list = new ArrayList<>();
            for (FaceDetectApi.Face face : faces) {
                DlmkFaceDetail faceDetail = DLmkDetectApi.getInstance().detectDenseLmk(facePPImage, face.points, face.rect, detailType);
                list.add(faceDetail);
            }
            mFaceImagePointView.setBitmapAndResult(bitmap, detailType, list);

        }
        long timeConst = System.currentTimeMillis() - startTime;

        sb.append("单帧耗时(ms):");
        sb.append(timeConst);
        tvDetectTips.setText(sb.toString());
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips, R.id.view_dlmk_hairline, R.id.view_dlmk_eyebrow, R.id.view_dlmk_eye, R.id.view_dlmk_nose_midline, R.id.view_dlmk_nose, R.id.view_dlmk_mouth, R.id.view_dlmk_contour, R.id.view_dlmk_face_midline})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
            case R.id.view_dlmk_hairline:
                viewDlmkHairline.setChecked(!viewDlmkHairline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_HAIRLINE.toInt(), !viewDlmkHairline.isChecked());
                break;
            case R.id.view_dlmk_eyebrow:
                viewDlmkEyebrow.setChecked(!viewDlmkEyebrow.isChecked());
                int eyebrowValue = DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_LEFT_EYEBROW.toInt() | DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_RIGHT_EYEBROW.toInt();
                setDetailType(eyebrowValue, !viewDlmkEyebrow.isChecked());
                break;
            case R.id.view_dlmk_eye:
                viewDlmkEye.setChecked(!viewDlmkEye.isChecked());
                int eyeValue = DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_LEFT_EYE.toInt() | DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_RIGHT_EYE.toInt();
                setDetailType(eyeValue, !viewDlmkEye.isChecked());
                break;
            case R.id.view_dlmk_nose_midline:
                viewDlmkNoseMidline.setChecked(!viewDlmkNoseMidline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_NOSE_MIDLINE.toInt(), !viewDlmkNoseMidline.isChecked());
                break;
            case R.id.view_dlmk_nose:
                viewDlmkNose.setChecked(!viewDlmkNose.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_NOSE.toInt(), !viewDlmkNose.isChecked());
                break;
            case R.id.view_dlmk_mouth:
                viewDlmkMouth.setChecked(!viewDlmkMouth.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_MOUTH.toInt(), !viewDlmkMouth.isChecked());
                break;
            case R.id.view_dlmk_contour:
                viewDlmkContour.setChecked(!viewDlmkContour.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_CONTOUR.toInt(), !viewDlmkContour.isChecked());
                break;
            case R.id.view_dlmk_face_midline:
                viewDlmkFaceMidline.setChecked(!viewDlmkFaceMidline.isChecked());
                setDetailType(DLmkDetectApi.FaceDetailType.MGF_FACE_DETAIL_FACE_MIDLINE.toInt(), !viewDlmkFaceMidline.isChecked());
                break;
        }
    }

    private void setDetailType(int value, boolean isChecked) {
        if (isChecked) {
            detailType &= (~value);
        } else {
            detailType |= value;
        }
        mFaceImagePointView.updateDetailType(detailType);
    }
}
