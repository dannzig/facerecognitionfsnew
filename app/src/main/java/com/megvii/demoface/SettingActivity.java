package com.megvii.demoface;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.rb_resolution_low)
    RadioButton rbResolutionLow;
    @BindView(R.id.rb_resolution_middle)
    RadioButton rbResolutionMiddle;
    @BindView(R.id.rb_resolution_high)
    RadioButton rbResolutionHigh;
    @BindView(R.id.rg_sresolution)
    RadioGroup rgSresolution;
    private int resolutionType = 1;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_set_resolution;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("human body cutout");
    }

    @Override
    protected void initData() {
        resolutionType = getIntent().getIntExtra("resolutionType", 1);
        if (resolutionType == 1) {
            rbResolutionLow.setChecked(true);
        } else if (resolutionType == 2) {
            rbResolutionMiddle.setChecked(true);
        } else if (resolutionType == 3) {
            rbResolutionHigh.setChecked(true);
        }
        setResult(resolutionType);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ll_go_back, R.id.rb_resolution_middle, R.id.rb_resolution_high, R.id.rb_resolution_low})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rb_resolution_middle:
                setResult(2);
                finish();
                break;
            case R.id.rb_resolution_high:
                setResult(3);
                finish();
                break;
            case R.id.rb_resolution_low:
                setResult(1);
                finish();
                break;
            case R.id.ll_go_back:
                finish();
                break;
        }
    }
}
