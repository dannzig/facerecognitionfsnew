package com.megvii.demoface.models;

public class faceModel {
    public static String id, name;
    public static byte[] template;

    public faceModel(String id, String name, byte[] template) {
        this.id = id;
        this.name = name;
        this.template = template;
    }

    public faceModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getTemplate() {
        return template;
    }

    public void setTemplate(byte[] template) {
        this.template = template;
    }
}
