package com.megvii.demoface.scene;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.utils.ConUtil;
import com.megvii.demoface.utils.ImageTransformUtils;
import com.megvii.demoface.utils.Screen;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.SceneDetectApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SceneImageDetectActivity extends BaseActivity {
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_scene_image)
    ImageView ivSceneImage;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;
    @BindView(R.id.tv_scene_name)
    TextView tvSceneName;
    @BindView(R.id.tv_scene_confidence)
    TextView tvSceneConfidence;
    @BindView(R.id.ll_scene_result)
    LinearLayout llSceneResult;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_scene_image;
    }

    @Override
    protected void initView() {
        tvTitleBar.setText("场景检测");
    }

    @Override
    protected void initData() {
        Uri uri = getIntent().getParcelableExtra("imgpath");
        String path = ConUtil.getRealPathFromURI(this, uri);
        Bitmap bitmap = ConUtil.getImage(path);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
        float scaleWidth = Screen.mWidth * 1.0f / imageWidth;
        float scaleHeight = Screen.mHeight * 1.0f / imageHeight;
        float scale = scaleWidth > scaleHeight ? scaleHeight : scaleWidth;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivSceneImage.getLayoutParams();
        layoutParams.width = (int) (scale * imageWidth);
        layoutParams.height = (int) (scale * imageHeight);
        ivSceneImage.setLayoutParams(layoutParams);
        ivSceneImage.setImageBitmap(bitmap);
        byte[] imageBgr = ImageTransformUtils.bitmap2BGR(bitmap);
        FacePPImage facePPImage = new FacePPImage.Builder()
                .setData(imageBgr)
                .setWidth(bitmap.getWidth())
                .setHeight(bitmap.getHeight())
                .setMode(FacePPImage.IMAGE_MODE_BGR)
                .setRotation(FacePPImage.FACE_UP).build();
        long startTime = System.currentTimeMillis();
        SceneDetectApi.SceneResult[] sceneResults = SceneDetectApi.getInstance().detectScene(facePPImage);
        long timeConst = System.currentTimeMillis() - startTime;
        tvDetectTips.setText("单帧耗时(ms):" + timeConst);
        float confidence = 0.0f;
        String name = "";
        for (SceneDetectApi.SceneResult result : sceneResults) {
            if (result.prop > confidence) {
                confidence = result.prop;
                name = result.name;
            }
        }
        if (!"".equals(name)){
            tvSceneName.setText(name);
            tvSceneConfidence.setText((int)(confidence*100)+"");
            llSceneResult.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ll_go_back, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }
}
