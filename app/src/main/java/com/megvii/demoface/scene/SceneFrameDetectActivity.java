package com.megvii.demoface.scene;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.megvii.demoface.BaseActivity;
import com.megvii.demoface.R;
import com.megvii.demoface.camera.CameraFactory;
import com.megvii.demoface.camera.CameraManager;
import com.megvii.demoface.camera.CameraWrapper;
import com.megvii.demoface.utils.Screen;
import com.megvii.demoface.view.CameraTextureView;
import com.megvii.facepp.multi.sdk.FacePPImage;
import com.megvii.facepp.multi.sdk.SceneDetectApi;

import butterknife.BindView;
import butterknife.OnClick;

public class SceneFrameDetectActivity extends BaseActivity implements CameraWrapper.CameraOpenCallback, CameraWrapper.ICameraCallback, TextureView.SurfaceTextureListener {
    @BindView(R.id.my_camera_textureview)
    CameraTextureView myCameraTextureview;
    @BindView(R.id.tv_scene_name)
    TextView tvSceneName;
    @BindView(R.id.tv_scene_confidence)
    TextView tvSceneConfidence;
    @BindView(R.id.ll_scene_result)
    LinearLayout llSceneResult;
    @BindView(R.id.tv_title_bar)
    TextView tvTitleBar;
    @BindView(R.id.ll_go_back)
    LinearLayout llGoBack;
    @BindView(R.id.iv_flip_camera)
    ImageView ivFlipCamera;
    @BindView(R.id.iv_show_tips)
    ImageView ivShowTips;
    @BindView(R.id.tv_detect_tips)
    TextView tvDetectTips;
    @BindView(R.id.ll_detect_tips)
    LinearLayout llDetectTips;

    private CameraManager mCameraManager;
    private Handler mHandler;
    private HandlerThread mHandlerThread = new HandlerThread("facepp");

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_scene_frame;
    }

    @Override
    protected void initView() {
        myCameraTextureview.setSurfaceTextureListener(this);
        tvTitleBar.setText("场景识别");
    }

    @Override
    protected void initData() {
        mCameraManager = new CameraManager(this);
        mCameraManager.setmCameraOpenCallback(this);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraManager.openCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraManager.closeCamera();
    }

    @OnClick({R.id.ll_go_back, R.id.iv_flip_camera, R.id.iv_show_tips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_go_back:
                finish();
                break;
            case R.id.iv_flip_camera:
                mCameraManager.switchCamera();
                break;
            case R.id.iv_show_tips:
                if (llDetectTips.getVisibility() == View.VISIBLE)
                    llDetectTips.setVisibility(View.GONE);
                else
                    llDetectTips.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void adjustTextureViewSize() {
        float scale = Math.max(Screen.mHeight * 1.0f / CameraFactory.mWidth, Screen.mWidth * 1.0f / CameraFactory.mHeight);
        int layout_width = (int) (CameraFactory.mHeight * scale);
        int layout_height = (int) (CameraFactory.mWidth * scale);

        myCameraTextureview.refreshView(layout_width, layout_height);

        RelativeLayout.LayoutParams surfaceParams = (RelativeLayout.LayoutParams) myCameraTextureview.getLayoutParams();
        int topMargin = Math.min((Screen.mHeight - layout_height) / 2, 0);
        int leftMargin = Math.min((Screen.mWidth - layout_width) / 2, 0);
        surfaceParams.topMargin = topMargin;
        surfaceParams.leftMargin = leftMargin;
        myCameraTextureview.setLayoutParams(surfaceParams);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    boolean isFinish = true;
    private int faceRotation = FacePPImage.FACE_LEFT;

    @Override
    public void onPreviewFrame(final byte[] bytes, Camera camera) {
        if (!isFinish) {
            return;
        }
        isFinish = false;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                FacePPImage facePPImage = new FacePPImage.Builder()
                        .setData(bytes)
                        .setWidth(CameraFactory.mWidth)
                        .setHeight(CameraFactory.mHeight)
                        .setMode(FacePPImage.IMAGE_MODE_NV21)
                        .setRotation(faceRotation).build();
                startTime = System.currentTimeMillis();
                SceneDetectApi.SceneResult[] sceneResults = SceneDetectApi.getInstance().detectScene(facePPImage);
                final long timeConst = System.currentTimeMillis() - startTime;
                float confidence = 0.0f;
                String name = "";
                for (SceneDetectApi.SceneResult result : sceneResults) {
                    if (result.prop > confidence) {
                        confidence = result.prop;
                        name = result.name;
                    }
                }
                final String finalName = name;
                final float finalConfidence = confidence;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!"".equals(finalName)) {
                            tvSceneName.setText(finalName);
                            tvSceneConfidence.setText((int) (finalConfidence * 100) + "");
                            llSceneResult.setVisibility(View.VISIBLE);
                        } else {
                            llSceneResult.setVisibility(View.GONE);
                        }
                        calculateEfficiEency(timeConst);
                    }
                });
                isFinish = true;
            }
        });
    }

    @Override
    public void onCapturePicture(byte[] bytes) {

    }

    private int frame = 0;
    private long startTime = 0;
    private long totalTime = 0;

    private void calculateEfficiEency(long detectTime) {
        frame++;
        totalTime = totalTime + detectTime;
        if (frame == 30) {
            StringBuilder tipSb = new StringBuilder();
            tipSb.append("单帧耗时(ms):");
            long timeconst = (long) (totalTime * 1.0f / frame);
            tipSb.append(timeconst);
            long fps = (long) (1000.0f / timeconst);
            tipSb.append("\n");
            tipSb.append("帧率(fps):");
            tipSb.append(fps);
            frame = 0;
            totalTime = 0;
            tvDetectTips.setText(tipSb);
        }
    }

    @Override
    public void onOpenSuccess() {
        if (mCameraManager.isFrontCam()) {
            faceRotation = FacePPImage.FACE_RIGHT;
        } else {
            faceRotation = FacePPImage.FACE_LEFT;
        }

        mCameraManager.setDisplayOrientation();
        mCameraManager.startPreview(myCameraTextureview.getSurfaceTexture());
        mCameraManager.actionDetect(this);
        adjustTextureViewSize();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onOpenFailed() {

    }
}
